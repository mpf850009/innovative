<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;

class bikerentalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($locale)
    {
        $active   = "index";
        $link     = "https: //www.twtainan.net/";
        $linkname = "台南市政府觀光旅遊局";
        App::setLocale($locale);
        return view('bikerental.index', compact('active', 'link', 'linkname', 'locale'));
    }
    public function company($locale)
    {
        $active   = "company";
        $link     = " ";
        $linkname = " ";
        App::setLocale($locale);

        return view('bikerental.company', compact('active', 'link', 'linkname', 'locale'));
    }
    public function onsale($locale)
    {
        $active   = "onsale";
        $link     = " ";
        $linkname = " ";
        App::setLocale($locale);

        return view('bikerental.onsale', compact('active', 'link', 'linkname', 'locale'));
    }
    public function ebike($locale)
    {
        $active   = "ebike";
        $link     = " ";
        $linkname = " ";
        App::setLocale($locale);

        return view('bikerental.ebike', compact('active', 'link', 'linkname', 'locale'));
    }
    public function contact($locale)
    {
        $active   = "contact";
        $link     = " ";
        $linkname = " ";
        App::setLocale($locale);

        return view('bikerental.contact', compact('active', 'link', 'linkname', 'locale'));
    }
    public function fulong($locale)
    {
        $active   = "fulong";
        $link     = "http: //www.ifulong.com.tw";
        $linkname = "福隆驛站";
        App::setLocale($locale);

        return view('bikerental.fulong', compact('active', 'link', 'linkname', 'locale'));
    }
    public function jiaoxi($locale)
    {
        $active   = "jiaoxi";
        $link     = "http: //www.natural-hotel.com.tw";
        $linkname = "自然風溫泉會館";
        App::setLocale($locale);

        return view('bikerental.jiaoxi', compact('active', 'link', 'linkname', 'locale'));
    }
    public function winhappy($locale)
    {
        $active   = "winhappy";
        $link     = "http://0425132875.tw.tranews.com/";
        $linkname = "東豐鐵馬道-勝開心租車";
        App::setLocale($locale);

        return view('bikerental.winhappy', compact('active', 'link', 'linkname', 'locale'));
    }
    public function dragon($locale)
    {
        $active   = "dragon";
        $link     = " ";
        $linkname = " ";
        App::setLocale($locale);

        return view('bikerental.dragon', compact('active', 'link', 'linkname', 'locale'));
    }
    public function asheng_h($locale)
    {
        $active   = "asheng_h";
        $link     = "http: //bike.e089.com.tw/";
        $linkname = "阿勝單車";
        App::setLocale($locale);

        return view('bikerental.asheng_h', compact('active', 'link', 'linkname', 'locale'));
    }
    public function asheng_t($locale)
    {
        $active   = "asheng_t";
        $link     = 'http: //bike.e089.com.tw/';
        $linkname = "阿勝單車";
        App::setLocale($locale);

        return view('bikerental.asheng_t', compact('active', 'link', 'linkname', 'locale'));
    }
    public function guanshan($locale)
    {
        $active   = "guanshan";
        $link     = "https: //tour.taitung.gov.tw/zh-tw/attraction/details/265";
        $linkname = "台東縣觀光旅遊網";
        App::setLocale($locale);

        return view('bikerental.guanshan', compact('active', 'link', 'linkname', 'locale'));
    }
    public function brown($locale)
    {
        $active   = "brown";
        $link     = "https: //tour.taitung.gov.tw/zh-tw/attraction/details/265";
        $linkname = "台東縣觀光旅遊網";
        App::setLocale($locale);

        return view('bikerental.brown', compact('active', 'link', 'linkname', 'locale'));
    }
    public function chishang($locale)
    {
        $active   = "chishang";
        $link     = "https: //tour.taitung.gov.tw/zh-tw/attraction/details/265";
        $linkname = "台東縣觀光旅遊網";
        App::setLocale($locale);

        return view('bikerental.chishang', compact('active', 'link', 'linkname', 'locale'));
    }
    public function park_water($locale)
    {
        $active   = "park_water";
        $link     = "https: //tour.taitung.gov.tw/zh-tw/attraction/details/265";
        $linkname = "台東縣觀光旅遊網";
        App::setLocale($locale);

        return view('bikerental.park_water', compact('active', 'link', 'linkname', 'locale'));
    }
    public function dongshan($locale)
    {
        $active   = "dongshan";
        $link     = "https: //tour.taitung.gov.tw/zh-tw/attraction/details/265";
        $linkname = "台東縣觀光旅遊網";
        App::setLocale($locale);

        return view('bikerental.dongshan', compact('active', 'link', 'linkname', 'locale'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
