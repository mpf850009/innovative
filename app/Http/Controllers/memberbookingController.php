<?php

namespace App\Http\Controllers;

use App\Area;
use App\Bike;
use App\Booking;
use App\Payment;
use App\User;
use Auth;
use Illuminate\Http\Request;

class memberbookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function booking()
    {
        if (Auth::check()) {
            $active   = "booking";
            $link     = " ";
            $linkname = " ";
            $bike     = Bike::all();
            $area     = Area::all();
            return view('bikerental.member.booking', compact('active', 'link', 'linkname', 'bike', 'area'));
        } else {
            return redirect()->route('bikerental.login');
        }
    }
    public function findProductName(Request $request)
    {
        $data = Payment::select('payment', 'time')->where('type', $request->id)->take(100)->get();
        return response()->json($data);
    }
    public function findQuantity(Request $request)
    {
        $data = Area::select('area', 'quantity', 'id')->where('id', $request->id)->take(100)->get();
        return response()->json($data);
    }
    public function confirmdate()
    {
        if (Auth::check()) {

            return view('bikerental.member.confirmdate');
        } else {
            return redirect()->route('bikerental.login');
        }
    }

    public function orderdata()
    {
        if (Auth::check()) {
            $active     = "orderdata";
            $link       = " ";
            $linkname   = " ";
            $booking    = Booking::where('member_id', Auth::id())->Paginate(4);
            $bookingall = Booking::Paginate(4);
            $user       = User::all();
            $bikes      = Bike::all();
            $payments   = Payment::all();
            $areas      = Area::all();

            return view('bikerental.member.orderdata', compact('active', 'link', 'linkname', 'booking', 'bookingall', 'user', 'bikes', 'payments', 'areas'));
        } else {
            return redirect()->route('bikerental.login');
        }
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'datetime' => 'required',
            'type'     => 'required',
            'time'     => 'required',
            'area'     => 'required',
            'quantity' => 'required',
            'howtopay' => 'required',
            'total'    => 'required',
        ]);
        $user_id = Auth::id();
        $booking = Booking::create([
            'member_id'   => $user_id,
            'datetime'    => $request->datetime,
            'type'        => $request->type,
            'time'        => $request->time,
            'area'        => $request->area,
            'quantity'    => $request->quantity,
            'howtopay'    => $request->howtopay,
            'total'       => $request->total,
            'status'      => "1",
            'alreadypaid' => "0",
        ]);
        return redirect()->route('bikerental.booking');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $active   = "orderdataedit";
        $link     = " ";
        $linkname = " ";
        $booking  = Booking::find($id);
        $bikes    = Bike::all();
        $areas    = Area::all();
        $payments = Payment::all();
        $users    = User::all();

        return view('bikerental.member.edit', compact('booking', 'active', 'link', 'linkname', 'bikes', 'areas', 'payments', 'users'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_id = Auth::id();
        $booking = Booking::find($id);
        $this->validate($request, [
            'datetime' => 'required',
            'type'     => 'required',
            'time'     => 'required',
            'area'     => 'required',
            'quantity' => 'required',
            'howtopay' => 'required',
            'total'    => 'required',
        ]);
        $booking->update($request->all());
        return redirect()->route('bikerental.orderdata');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
