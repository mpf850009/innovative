<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;

class InnovativeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($locale)
    {
        $active = "index";
        $title  = "Innovative 創星有限公司台灣分公司";
        App::setLocale($locale);
        return view('innovative.index', compact('active', 'title', 'locale'));
    }
    public function about($locale)
    {
        $active     = "about us";
        $title      = "About | Innovative 創星有限公司台灣分公司";
        $background = "background:url('images/innovative/logoback1.png') no-repeat 0px 0px;
					background-size: cover;
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					-ms-background-size: cover;
					background-position: center;
                    min-height: 500px;";
        App::setLocale($locale);

        return view('innovative.about', compact('active', 'background', 'title', 'locale'));
    }
    public function news($locale)
    {
        $active     = "news";
        $title      = "News | Innovative 創星有限公司台灣分公司";
         //Edited on 2020/04/23 change photo background
        $background = "background:url('images/innovative/bg34.jpg') no-repeat 0px 0px;
					background-size: cover;
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					-ms-background-size: cover;
					background-position: center left;
                    min-height: 500px;";
        App::setLocale($locale);

        return view('innovative.news', compact('active', 'background', 'title', 'locale'));
    }
    public function nikimotion($locale)
    {
        App::setLocale($locale);

        $active     = "nikimotion";
        $title      = "Nikimotion | Innovative 創星有限公司台灣分公司";
        //Edited on 2020/04/23 change photo background
        $background = "background:url('images/innovative/bg35.jpg') no-repeat 0px 0px;
					background-size: cover;
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					-ms-background-size: cover;
					background-position: center;
					min-height: 500px;";

        return view('innovative.nikimotion', compact('active', 'background', 'title', 'locale'));
    }
    public function autofold($locale)
    {
        App::setLocale($locale);

        $active     = "autofold";
        $title      = "Nikimotion Autofold | Innovative 創星有限公司台灣分公司";
        //Edited on 2020/04/23 change photo background
        $background = "background:url('images/innovative/bg35.jpg') no-repeat 0px 0px;
					background-size: cover;
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					-ms-background-size: cover;
					background-position: center;
					min-height: 500px;";

        return view('innovative.autofold', compact('active', 'background', 'title', 'locale'));
    }
    public function autofoldlite($locale)
    {
        App::setLocale($locale);

        $active     = "autofold lite";
        $title      = "Nikimotion Autofold Lite | Innovative 創星有限公司台灣分公司";
        //Edited on 2020/04/23 change photo background
        $background = "background:url('images/innovative/bg35.jpg') no-repeat 0px 0px;
					background-size: cover;
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					-ms-background-size: cover;
					background-position: center;
					min-height: 500px;";

        return view('innovative.autofoldlite', compact('active', 'background', 'title', 'locale'));
    }
    public function blade($locale)
    {
        App::setLocale($locale);

        $active     = "blade";
        $title      = "Nikimotion Blade | Innovative 創星有限公司台灣分公司";
        //Edited on 2020/04/23 change photo background
        $background = "background:url('images/innovative/bg35.jpg') no-repeat 0px 0px;
					background-size: cover;
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					-ms-background-size: cover;
					background-position: center;
					min-height: 500px;";

        return view('innovative.blade', compact('active', 'background', 'title', 'locale'));
    }
    public function nkac($locale)
    {
        App::setLocale($locale);

        $active     = "accessories";
        $title      = "Nikimotion Accessories | Innovative 創星有限公司台灣分公司";
        //Edited on 2020/04/23 change photo background
        $background = "background:url('images/innovative/bg35.jpg') no-repeat 0px 0px;
					background-size: cover;
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					-ms-background-size: cover;
					background-position: center;
					min-height: 500px;";

        return view('innovative.nkac', compact('active', 'background', 'title', 'locale'));
    }
    public function veer($locale)
    {
        App::setLocale($locale);

        $active     = "veer";
        $title      = "Veer | Innovative 創星有限公司台灣分公司";
        $background = "background:url('images/innovative/bg16.jpg') no-repeat 0px 0px;
					background-size: cover;
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					-ms-background-size: cover;
					background-position: center;
					min-height: 500px;";

        return view('innovative.veer', compact('active', 'background', 'title', 'locale'));
    }
    public function veeracc($locale, $type)
    {
        App::setLocale($locale);
        $active     = "veer acc";
        $title      = "Veer Accessories | Innovative 創星有限公司台灣分公司";
        $background = "background:url('../images/innovative/bg16.jpg') no-repeat 0px 0px;
					background-size: cover;
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					-ms-background-size: cover;
					background-position: center;
					min-height: 500px;";

        return view('innovative.veeracc', compact('active', 'type', 'background', 'title', 'locale'));
    }
    public function ebike($locale)
    {
        App::setLocale($locale);

        $active     = "e-bike";
        $title      = "E-Bike | Innovative 創星有限公司台灣分公司";
        $background = "background:url('images/innovative/bg14.jpg') no-repeat 0px 0px;
					background-size: cover;
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					-ms-background-size: cover;
                    background-position: center;
					min-height: 500px;";

        return view('innovative.ebike', compact('active', 'background', 'title', 'locale'));
    }
    public function ebikeintro($locale, $bike)
    {
        App::setLocale($locale);
        $active     = "e-bike intro";
        $title      = "E-Bike Introduction | Innovative 創星有限公司台灣分公司";
        $background = "background:url('../images/innovative/bg14.jpg') no-repeat 0px 0px;
					background-size: cover;
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					-ms-background-size: cover;
                    background-position: center;
					min-height: 500px;";

        return view('innovative.ebikeintro', compact('active', 'background', 'title', 'locale', 'bike'));
    }
    public function flagship($locale)
    {
        App::setLocale($locale);

        $active     = "store";
        $title      = "Store | Innovative 創星有限公司台灣分公司";
        $background = "background:url('images/innovative/store.jpg') no-repeat 0px 0px;
					background-size: cover;
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					-ms-background-size: cover;
					background-position: center;
					min-height: 500px;";

        return view('innovative.store', compact('active', 'background', 'title', 'locale'));
    }
    public function rentmap($locale)
    {
        App::setLocale($locale);

        $active     = "rentmap";
        $title      = "E-bike全省租車點 | Innovative 創星有限公司台灣分公司";
        $background = "background:url('images/innovative/bg14.jpg') no-repeat 0px 0px;
					background-size: cover;
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					-ms-background-size: cover;
					background-position: center;
					min-height: 500px;";

        return view('innovative.rentmap', compact('active', 'background', 'title', 'locale'));
    }
    public function rentplace($locale, $place)
    {
        App::setLocale($locale);
        $active     = "rentplace";
        $title      = "E-bike全省租車點 | Innovative 創星有限公司台灣分公司";
        $background = "background:url('images/innovative/bg14.jpg') no-repeat 0px 0px;
					background-size: cover;
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					-ms-background-size: cover;
					background-position: center;
					min-height: 500px;";

        return view('innovative.rentplace', compact('active', 'background', 'title', 'locale', 'place'));
    }
    public function brand($locale)
    {
        App::setLocale($locale);

        $active     = "brand";
        $title      = "E-Bike品牌夥伴 | Innovative 創星有限公司台灣分公司";
        $background = "background:url('images/innovative/bg14.jpg') no-repeat 0px 0px;
					background-size: cover;
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
                    -ms-background-size: cover;
                    background-position: center;
                    min-height: 500px;";

        return view('innovative.brand', compact('active', 'background', 'title', 'locale'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
