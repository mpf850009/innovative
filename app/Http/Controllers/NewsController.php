<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function news_detail2018($locale, $date)
    {
        App::setLocale($locale);
        $active = "news1";
        if ($date == '0427') {
            $title      = "News 新北市福隆驛站開幕 | Innovative 創星有限公司台灣分公司";
            $tag        = "新北市福隆驛站開幕";
            $background = "background:url('../images/innovative/bg22.jpg') no-repeat 0px 0px;
                background-size: cover;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                -ms-background-size: cover;
                background-position: center;
                min-height: 500px;";
        } else if ($date == '0601') {
            $title      = "News 環鎮租車池上站開幕 | Innovative 創星有限公司台灣分公司";
            $tag        = "環鎮租車池上站開幕";
            $background = "background:url('../images/innovative/bg24.jpg') no-repeat 0px 0px;
                background-size: cover;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                -ms-background-size: cover;
                background-position: center top;
                min-height: 500px;";
        } else if ($date == '0617') {
            $title      = "News 樂享學永康站開幕 | Innovative 創星有限公司台灣分公司";
            $tag        = "樂享學永康站開幕";
            $background = "background:url('../images/innovative/store.jpg') no-repeat 0px 0px;
                background-size: cover;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                -ms-background-size: cover;
                background-position: center;
                min-height: 500px;";
        } else if ($date == '0911') {
            $title      = "News 花蓮玉里站開幕 | Innovative 創星有限公司台灣分公司";
            $tag        = "花蓮玉里站開幕";
            $background = "background:url('../images/innovative/news_yuli1.jpg') no-repeat 0px 0px;
                background-size: cover;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                -ms-background-size: cover;
                background-position: center;
                min-height: 500px;";
        } else if ($date == '1030') {
            $title      = "News 台北國際自行車展覽會 Taipei Cycle | Innovative 創星有限公司台灣分公司";
            $tag        = "台北國際自行車展覽會";
            $background = "background:url('../images/innovative/tclogo2.png') no-repeat 0px 0px;
                background-size: cover;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                -ms-background-size: cover;
                background-position: center;
                min-height: 500px;";
        } else if ($date == '1120') {
            $title      = "News 阿勝單車站開幕 | Innovative 創星有限公司台灣分公司";
            $tag        = "阿勝單車站開幕";
            $background = "background:url('../images/innovative/bg29.jpg') no-repeat 0px 0px;
                background-size: cover;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                -ms-background-size: cover;
                background-position: center top;
                min-height: 500px;";

        } else if ($date == '1127') {
            $title      = "News 豐原起站開幕 | Innovative 創星有限公司台灣分公司";
            $tag        = "豐原起站開幕";
            $background = "background:url('../images/innovative/bg30.jpg') no-repeat 0px 0px;
                background-size: cover;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                -ms-background-size: cover;
                background-position: center top;
                min-height: 500px;";

        } else if ($date == '1214') {
            $title      = "News 高雄嬰兒與孕媽咪用品展 | Innovative 創星有限公司台灣分公司";
            $tag        = "高雄嬰兒與孕媽咪用品展";
            $background = "background:url('../images/innovative/ksstrollerback.jpg') no-repeat 0px 0px;
                background-size: cover;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                -ms-background-size: cover;
                background-position: center;
                min-height: 500px;";

        }else if ($date == '190327') {
            $title      = "News Taipei Cycle 台北國際自行車展覽會 | Innovative 創星有限公司台灣分公司";
            $tag        = "Taipei Cycle 台北國際自行車展覽會";
            $background = "background:url('../images/innovative/news1.jpg') no-repeat 0px 0px;
                background-size: cover;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                -ms-background-size: cover;
                background-position: bottom;
                min-height: 500px;";

        }else if ($date == '190404') {
            $title      = "News 台北嬰兒與孕媽咪用品展 | Innovative 創星有限公司台灣分公司";
            $tag        = "台北嬰兒與孕媽咪用品展";
            //edited on 2020/04/23 change news8.jpg to mombaby.jpg
            $background = "background:url('../images/innovative/mombaby.jpg') no-repeat 0px 0px;
                background-size: cover;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                -ms-background-size: cover;
                background-position: center;
                min-height: 500px;";

        }else if ($date == '200304') {
            $title      = "News Taipei Cycle 台北國際自行車展覽會 | Innovative 創星有限公司台灣分公司";
            $tag        = "Taipei Cycle 台北國際自行車展覽會";
            $background = "background:url('../images/innovative/news10.jpg') no-repeat 0px 0px;
                background-size: cover;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                -ms-background-size: cover;
                background-position: bottom;
                min-height: 500px;";

        } else {
            $title      = "News | Innovative 創星有限公司台灣分公司";
            $tag        = "News";
            //edited on 2020/04/23 change bg.21jpg to bg34.jpg
            $background = "background:url('../images/innovative/bg34.jpg') no-repeat 0px 0px;
					background-size: cover;
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					-ms-background-size: cover;
					background-position: center left;
					min-height: 500px;";
        }
        return view('innovative.news_detail', compact('tag', 'active', 'background', 'title', 'locale', 'date'));
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

