<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;

class RentplaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function rentplace($locale, $place)
    {
        App::setLocale($locale);
        $active = "rentplace";
        if ($place == 'fulong') {
            $title      = "E-bike全省租車點 | Innovative 創星有限公司台灣分公司";
            $background = "background:url('../images/innovative/bg22.jpg') no-repeat 0px 0px;
					background-size: cover;
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					-ms-background-size: cover;
					background-position: center top;
					min-height: 400px;";
        } else if ($place == 'fengyuan') {
            $title      = "E-bike全省租車點 | Innovative 創星有限公司台灣分公司";
            $background = "background:url('../images/innovative/bg30.jpg') no-repeat 0px 0px;
					background-size: cover;
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					-ms-background-size: cover;
					background-position: center top;
					min-height: 400px;";
        } else if ($place == 'yongkang') {
            $title      = "E-bike全省租車點 | Innovative 創星有限公司台灣分公司";
            $background = "background:url('../images/innovative/store.jpg') no-repeat 0px 0px;
					background-size: cover;
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					-ms-background-size: cover;
					background-position: center;
					min-height: 400px;";
        } else if ($place == 'asheng') {
            $title      = "E-bike全省租車點 | Innovative 創星有限公司台灣分公司";
            $background = "background:url('../images/innovative/bg29.jpg') no-repeat 0px 0px;
					background-size: cover;
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					-ms-background-size: cover;
					background-position: center top;
					min-height: 400px;";
        } else if ($place == 'chishang') {
            $title      = "E-bike全省租車點 | Innovative 創星有限公司台灣分公司";
            $background = "background:url('../images/innovative/bg24.jpg') no-repeat 0px 0px;
					background-size: cover;
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					-ms-background-size: cover;
					background-position: center top;
					min-height: 400px;";
        } else if ($place == 'yuli') {
            $title      = "E-bike全省租車點 | Innovative 創星有限公司台灣分公司";
            $background = "background:url('../images/innovative/news_yuli1.jpg') no-repeat 0px 0px;
					background-size: cover;
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					-ms-background-size: cover;
					background-position: center;
					min-height: 400px;";
        } else {
            $title      = "E-bike全省租車點 | Innovative 創星有限公司台灣分公司";
            $background = "background:url('../images/innovative/bg14.jpg') no-repeat 0px 0px;
					background-size: cover;
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					-ms-background-size: cover;
					background-position: center;
					min-height: 500px;";
        }
        return view('innovative.rentplace', compact('active', 'background', 'title', 'locale', 'place'));
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
