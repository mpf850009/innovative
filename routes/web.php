<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

Route::get('/', function () {
return view('welcome');
});
 */

//unionsports
Route::get('/unionsports', 'UnionsportsController@index');

//innovative
//Route::get('/', 'innovativeController@index')->name('index');
Route::get('/', function () {
    return redirect('/home_zh-TW');
});

Route::get('/innovative', function () {
    return redirect('/home_zh-TW');
})->name('index');
Route::get('/home_{locale}', 'innovativeController@index')->name('innovative.index');
Route::get('/about_{locale}', 'innovativeController@about')->name('innovative.about');
Route::get('/news_{locale}', 'innovativeController@news')->name('innovative.news');
Route::get('/news_{locale}/{date}', 'NewsController@news_detail2018')->name('innovative.news_detail');

Route::get('/nikimotion_{locale}', 'innovativeController@nikimotion')->name('innovative.nikimotion');
Route::get('/autofold_{locale}', 'innovativeController@autofold')->name('innovative.autofold');
Route::get('/autofoldlite_{locale}', 'innovativeController@autofoldlite')->name('innovative.autofoldlite');
Route::get('/blade_{locale}', 'innovativeController@blade')->name('innovative.blade');
Route::get('/nk_accessories_{locale}', 'innovativeController@nkac')->name('innovative.nkac');
Route::get('/veer_{locale}', 'innovativeController@veer')->name('innovative.veer');
Route::get('/veeracc_{locale}/{type}', 'innovativeController@veeracc')->name('innovative.veeracc');
Route::get('/ebike_{locale}', 'innovativeController@ebike')->name('innovative.ebike');
Route::get('/ebikeintro_{locale}/{bike}', 'innovativeController@ebikeintro')->name('innovative.ebikeintro');
Route::get('/store_{locale}', 'innovativeController@flagship')->name('innovative.flagship');
Route::get('/rentmap_{locale}', 'innovativeController@rentmap')->name('innovative.rentmap');
Route::get('/rentmap_{locale}/{place}', 'RentplaceController@rentplace')->name('innovative.rentplace');

Route::get('/brand_{locale}', 'innovativeController@brand')->name('innovative.brand');

//innovative_news
Route::get('/20181030_{locale}', 'NewsController@news20181030')->name('innovative.news20181030');
Route::get('/20180911_{locale}', 'NewsController@news20180911')->name('innovative.news20180911');
Route::get('/20180629_{locale}', 'NewsController@news20180629')->name('innovative.news20180629');
Route::get('/20180617_{locale}', 'NewsController@news20180617')->name('innovative.news20180617');
Route::get('/20180614_{locale}', 'NewsController@news20180614')->name('innovative.news20180614');
Route::get('/20180621_{locale}', 'NewsController@news20180621')->name('innovative.news20180621');
Route::get('/20180601_{locale}', 'NewsController@news20180601')->name('innovative.news20180601');
Route::get('/20180521_{locale}', 'NewsController@news20180521')->name('innovative.news20180521');
Route::get('/20180427_{locale}', 'NewsController@news20180427')->name('innovative.news20180427');

//bikerental
Route::get('/bikerental', function () {
    return redirect('/bikerental/zh-TW');
})->name("trans");
Route::get('/bikerental/{locale}', 'bikerentalController@index')->name('bikerental.index');
Route::get('/bikerental_company/{locale}', 'bikerentalController@company')->name('bikerental.company');
Route::get('/bikerental_onsale/{locale}', 'bikerentalController@onsale')->name('bikerental.onsale');
Route::get('/bikerental_ebike/{locale}', 'bikerentalController@ebike')->name('bikerental.ebike');
Route::get('/bikerental_contact/{locale}', 'bikerentalController@contact')->name('bikerental.contact');
Route::get('/bikerental_fulong/{locale}', 'bikerentalController@fulong')->name('bikerental.fulong');
Route::get('/bikerental_jiaoxi/{locale}', 'bikerentalController@jiaoxi')->name('bikerental.jiaoxi');
Route::get('/bikerental_winhappy/{locale}', 'bikerentalController@winhappy')->name('bikerental.winhappy');
Route::get('/bikerental_dragon/{locale}', 'bikerentalController@dragon')->name('bikerental.dragon');
Route::get('/bikerental_asheng_h/{locale}', 'bikerentalController@asheng_h')->name('bikerental.asheng_h');
Route::get('/bikerental_asheng_t/{locale}', 'bikerentalController@asheng_t')->name('bikerental.asheng_t');
Route::get('/bikerental_guanshan/{locale}', 'bikerentalController@guanshan')->name('bikerental.guanshan');
Route::get('/bikerental_brown/{locale}', 'bikerentalController@brown')->name('bikerental.brown');
Route::get('/bikerental_chishang/{locale}', 'bikerentalController@chishang')->name('bikerental.chishang');
Route::get('/bikerental_dongshan/{locale}', 'bikerentalController@dongshan')->name('bikerental.dongshan');

Route::get('/bikerental_park_water/{locale}', 'bikerentalController@park_water')->name('bikerental.park_water');

Route::get('/bikerental_register', 'rentalmemberController@register')->name('bikerental.register');
Route::get('/bikerental_login', 'rentalmemberController@login')->name('bikerental.login');
Route::get('/bikerental_center', 'rentalmemberController@center')->name('bikerental.center');

Route::get('/bikerental_booking', 'memberbookingController@booking')->name('bikerental.booking');
Route::get('/bikerental_confirmdate', 'memberbookingController@confirmdate')->name('bikerental.confirmdate');
Route::post('/bikerental_booking', 'memberbookingController@store')->name('bikerental.store');
Route::get('/bikerental_orderdata', 'memberbookingController@orderdata')->name('bikerental.orderdata');
Route::get('/bikerental_orderdata/{id}/edit', 'memberbookingController@edit')->name('bikerental.orderdataedit');
Route::patch('/bikerental_orderdata/{id}', 'memberbookingController@update')->name('bikerental.orderdataupdate');

//mpfdrive
Route::get('/mpfdrive', 'MpfdriveController@index')->name('mpfdrive.index');
//mpfcomtw
Route::get('/mpfcomtw', 'MpfcomtwController@index')->name('mpfcomtw.index');

//auth login
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

//ajax
Route::get('/findProductName', 'testController@findProductName');
Route::get('/findQuantity', 'memberbookingController@findQuantity');
