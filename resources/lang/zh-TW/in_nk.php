<?php

return [
    'p'       => '產品',
    'fc'      => '顏色款式',
    'dc1'     => '<strong class="hideme">產品介紹</strong><br>
            <ul class="hideme">
                <li class="hideme">單手收折</li>
                <li class="hideme">可調節式背靠及腳踏</li>
                <li class="hideme">收折後可自行站立</li>
                <li class="hideme">四輪避震設計</li>
                <li class="hideme">快拆式前後輪</li>
                <li class="hideme">可拆卸式幼兒手扶把</li>
                <li class="hideme">可拆卸式清洗布料</li>
                <li class="hideme">大空間置物籃</li>
                <li class="hideme">開折式遮陽篷(共四折)</li>
                <li class="hideme">五點式安全帶</li>
                <li class="hideme">防潑水及抗UV布料</li>
                <li class="hideme">15種顏色可搭配選擇</li>
            </ul><br>
            <strong class="hideme">規格</strong><br>
            <ul class="hideme">
                <li class="hideme">適用年齡 新生兒-15KG</li>
                <li class="hideme">推車重量 8.6 KG</li>
                <li class="hideme">展車尺寸 91.5 57.5 105 cm</li>
                <li class="hideme">收車尺寸 68 57.5 29 cm</li>
                <li class="hideme">推車輪胎 四輪PU</li>
                <li class="hideme">測試認證 EN1888-2012/CNS12940</li>
            </ul><br>
            <strong class="hideme">配件</strong><br>
            <ul class="hideme">
                <li class="hideme">MAXI-COSI 汽座轉接座</li>
                <li class="hideme">水杯架</li>
                <li class="hideme">雨罩</li>
                <li class="hideme">蚊帳</li>
                <li class="hideme">手把置物包</li>
            </ul><br>',
    'ac'      => '配件',
    'dc2'     => '<strong class="hideme">產品介紹</strong><br>
            <ul class="hideme">
                <li class="hideme">單手收折</li>
                <li class="hideme">可調節式背靠</li>
                <li class="hideme">收折後可自行站立</li>
                <li class="hideme">大空間置物籃</li>
                <li class="hideme">五點式安全扣具</li>
                <li class="hideme">可搭配配件睡箱</li>
            </ul><br>
            <strong class="hideme">規格</strong><br>
            <ul class="hideme">
                <li class="hideme">重量 7.6 kg</li>
                <li class="hideme">收折尺寸 68 x 55 x 30 cm</li>
                <li class="hideme">車身寬度 > 60cm</li>
            </ul><br>
            <strong class="hideme">配件</strong><br>
            <ul class="hideme">
                <li class="hideme">MAXI-COSI汽座轉接座</li>
                <li class="hideme">水杯架</li>
                <li class="hideme">蚊帳</li>
                <li class="hideme">雨罩</li>
            </ul><br>',
    'dc3'     => '<strong class="hideme">產品介紹</strong><br>
            <ul class="hideme">
                <li class="hideme">雙向使用座椅</li>
                <li class="hideme">雙向座椅皆可直接收展車</li>
                <li class="hideme">收車後可自行站立</li>
                <li class="hideme">可調節式背靠</li>
                <li class="hideme">可拆卸式手把</li>
                <li class="hideme">天窗式遮陽篷</li>
                <li class="hideme">五點式安全扣具</li>
                <li class="hideme">搭配配件睡箱</li>
            </ul><br>
            <strong class="hideme">規格</strong><br>
            <ul class="hideme">
                <li class="hideme">重量	14.5 kg</li>
                <li class="hideme">尺寸	90 x 54 x 29 cm</li>
            </ul><br>
            <strong class="hideme">配件</strong><br>
            <ul class="hideme">
                <li class="hideme">MAXI-COSI氣座轉接座</li>
                <li class="hideme">水杯架</li>
                <li class="hideme">蚊帳</li>
                <li class="hideme">雨罩</li>
            </ul><br>',
    'nkac'    => '嬰兒車配件',
    'acintro' => '配件介紹',
    'back'    => ' BACK ',
    'afac'    => 'Autofold專用配件',
    'aflac'   => 'Autofold Lite專用配件',
    'blac'    => 'Blade專用配件',
    'acc1t'   => '杯架',
    'acc8t'   => 'AUTOFOLD<br>汽座轉接座',
    'acc9t'   => '蚊帳',
    'acc10t'  => '雨罩',
    'acc12t'  => 'AUTOFOLD LITE 手把',
    'acc13t'  => 'BLADE<br>汽座轉接座',
    'acc16t'  => '手扶把',
    'acc17t'  => '手把置物包',
    'acc1'    => '<h3>杯架</h3><br><p>杯架可快速安裝在嬰兒車把手側邊的卡榫上，放置爸爸媽媽的茶水、飲料。</p><br>',
    'acc2'    => '<h3>睡袋</h3><br><p><strong>nikimotion睡袋可以保護您的寶寶免於受凍、風吹和雨淋。</strong><br><br>頂部帶有抽繩，可與嬰兒車的皮帶系統連接。</p><br>',
    'acc3'    => '<h3>媽媽包</h3><br><p><strong> nikimotion媽媽包為您的寶寶需要的所有東西提供了寬敞的空間，可以放置所有重要的東西。</strong><br><br><li>可作為背包</li><li>適合所有嬰兒車</li><li>襯墊尿布</li><li>尿布袋</li><li>嬰兒奶瓶的熱袋</li><li>手機袋</li><li>額外的背袋</li></p><br>',
    'acc4'    => '<h3>ORGANIZER</h3><br><p><strong>With the nikimotion Organizer you have all important things always within reach.</strong><br><br><li>suitable as a mobile phone & cup holder or as a spacious pocket</li><li>with Velcro straps attached to the stroller</li></p><br>',
    'acc5'    => '<h3>PARASOL</h3><br><p><strong>The parasol offers pleasant shadow and protect against direct sun.</strong><br><br><li>UV protection 50+</li><li>Can be applied to round and oval frame tubes using a universal clamp</li></p><br>',
    'acc6'    => '<h3>蚊帳</h3><br><p>使用配件蚊帳可有效將蚊蟲隔絕在車身外，蚊帳材質為網狀具有良好的通風效果。</p><br>',
    'acc7'    => '<h3>雨罩</h3><br><p>雨罩可在下雨天和風大的天氣狀況下使用，雨罩採用PVC材質，容易清潔、收納</p><br>',
    'acc8'    => '<h3>AUTOFOLD 汽座轉接座</h3><br><p>安裝汽座轉接座可將MAXI-COSI的安全座椅放置在AUTOFOLD車上使用。<br>適用MAXI-COSI汽座型號 : Cabrio Fix, Pebble, Pebble Plus, Citi</p><br>',
    'acc9'    => '<h3>蚊帳</h3><br><p>使用配件蚊帳可有效將蚊蟲隔絕在車身外，蚊帳材質為具有良好的通風效果的網狀布料。</p><br>',
    'acc10'   => '<h3>雨罩</h3><br><p>雨罩可在下雨天和風大的天氣狀況下使用，雨罩採用PVC材質，容易清潔、收納。</p><br>',
    'acc11'   => '<h3>CARRYCOT</h3><br><p>The<strong> AUTOFOLD pram </strong>provides an additional extension to the buggy, allowing a child up to six months old to sleep and lie down.<br><br><li>complete with upholstery and mattress</li><li>textile parts remov- and washable</li><li>protected against UV rays</li><li>water repellent impregnation</li></p><br>',
    'acc12'   => '<h3>AUTOFOLD LITE 手把</h3><br><p>AUTOFOLD LITE的手扶把。</p><br>',
    'acc13'   => '<h3>BLADE 汽座轉接座</h3><br><p>裝汽座轉接座可將MAXI-COSI的安全座椅放置在BLADE車上使用。<br>適用MAXI-COSI汽座型號 : Cabrio Fix, Pebble, Pebble Plus, Citi</p><br>',
    'acc14'   => '<h3>MOSQUITO NET</h3><br><p><strong>Summer nights without insect bites – the BLADE mosquito net makes it possible!</strong><br><br><li>fine mesh material for optimum ventilation</li><li>reclosable storage bag</li></p><br>',
    'acc15'   => '<h3>RAINCOVER</h3><br><p><strong>The BLADE raincover protects your darling against rain, wind and snow. It fits perfectly on the nikimotion BLADE sport seat.</strong><br><br><li>PVC-free</li><li>great windowed with vendilation</li><li>easy to clean</li><li>protection against rain, snow and wind</li><li>age recommendation: from birth</li><li>color: transparent</li></p><br>',
    'acc16'   => '<h3>手扶把</h3><br><p>手扶把，為小朋友安全提供多一道防護。</p><br>',
    'acc17'   => '<h3>手把置物包</h3><br><p>手把置物包方便讓媽媽放置手機、錢包、鑰匙、飲料；防波水材質布料，讓媽媽們在下雨天時也能安心使用。</p><br>',
    'colorred'=>'珊瑚紅'
];
