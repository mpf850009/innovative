<?php

return [
    'home'          => '首頁',
    'aboutus'       => '公司簡介',
    'news'          => '歡迎光臨',
    'nikimotion'    => 'Nikimotion',
    'autofold'      => 'Autofold',
    'autofoldlite'  => 'Autofold Lite',
    'blade'         => 'Blade',
    'accessories'   => '配件',
    'veer'          => 'VEER',
    'ebike'         => '電動輔助自行車',
    'rentplace'     => '全省租車點',
    'brand'         => '品牌夥伴',
    'store'         => '旗艦店',
    'welcome'       => 'WELCOME',
    'welcome1'      => '<h2 align="center";>Innovative創星有限公司</h2><br><p>創星有限公司，代理國內外優質品牌，結合嬰幼兒產品以及戶外運動用品，提供給您最優質的保障。如對相關產品有任何問題，歡迎隨時與我們聯繫。</p>',
    'wel1'          => '品牌起源於奧地利，是給予家庭希望的送子鳥。單手即可完成快速秒收的技術，使得爸媽使用嬰兒車更為方便。',
    'wel2'          => '品牌起源於美國，全地形戰艦級嬰兒車，既是嬰兒車也是拖車，一台顛覆您想像中的嬰兒車。',
    'wel3'          => '採用MPF Drive 中置馬達電機系統的各式品牌電動輔助自行車！',
    'service'       => '服務項目',
    'service1'      => '<h5>體驗產品</h5><p>可點選「E-Bike」至全省租車點現場租借體驗...</p>',
    'service2'      => '<h5>技術支援</h5><p>技術支援...</p>',
    'service3'      => '<h5>產品保固</h5><p>各式產品保固...</p>',
    'exhibitor'     => '參展訊息',
    'exh1'          => '<div class="w3-adv-right2-text">
                            <h3>第18屆CBME中國孕嬰童展、童裝展</h3>
                            <p>2018.7.25-27, 國家會展中心(上海)</p>
                        </div>
                        <div class="w3-adv-right2-text-para">
                            <p>
                                ✔ 266,548平米展示空間，匯聚全品類孕嬰童產品<br>
                                ✔ 2,900+家展商在2018 CBME中國現場展示4,300+個品牌<br>
                                ✔ 全球知名品牌齊聚CBME中國<br>
                                ✔ 與90,000+位優質買家建立業務聯繫，拓展商機 <br>
                                ✔ 整合式營銷，提升品牌曝光度和影響力
                            </p>
                            <small style="float:right;">We will be there soon!</small>
                        </div>',
    'exh2'          => '<div class="w3-adv-lef1-text">
                            <h3>2018 台中自行車展</h3>
                            <p>9/26-9/28, 2018</p>
                        </div>
                        <div class="w3-adv-lef1-text2">
                            <p>
                                <span>"</span>By the bike industry for the bike industry.<span>"</span>
                            </p>
                            <small style="float:right;">We will be there soon!</small>
                        </div>',
    'exh3'          => '<div class="w3-adv-right2-text">
                            <h3>2018台北婦幼展覽</h3>
                            <p>Oct 10 - 14, 2018, Taipei</p>
                        </div>
                        <div class="w3-adv-right2-text-para">
                            <h4>亞洲首選 台灣唯一國際級 媽咪寶貝採購盛會</h4><br>
                            <p>　　參展廠商包含孕嬰用品國際品牌原廠、月子餐/中心、保養品、彌月蛋糕禮盒、寶貝育教娛樂商品、臍帶血生物科技等各大母嬰產業龍頭，推出2018年度新品，聯手規劃最優惠經濟的孕嬰商品，打造最大規模且完整的婦幼採購一站式平台。</p>
                            <small style="float:right;">We will be there soon!</small>
                        </div>',
    'exh4'          => '<div class="w3-adv-right2-text">
                            <h3>台北自行車展 2019</h3>
                            <p>03/27 - 03/30, 2019, Taipei</p>
                        </div>
                        <div class="w3-adv-right2-text-para">
                            <h4>Events</h4><br>
                            <p>
                                ✔  Mar 27 to 28: only open to professional buyers. Children under 12 are not allowed to enter on these days.<br>
                                ✔  Mar 29 to 30: Open to buyers and the public. Members of the general public and children under 12 must buy tickets to enter the show ground. (The ticket office will close 2 hours before the show closes.)
                            </p>
                            <small style="float:right;">We will be there soon!</small>
                        </div>',
    'exh5'          => '<div class="w3-adv-right2-text">
                            <h3>台北世貿嬰兒與孕媽咪用品展 2020</h3>
                            <p>06/25 - 06/28, 2020, Kaohsiung</p>
                        </div>
                        <div class="w3-adv-right2-text-para">
                            <h4>高雄嬰兒與孕媽咪用品展 (夏季展)</h4><br>
                            <p>
                                ✔ 展出日期：2020年6月25日(星期四)－6月28日(星期日)<br>
                                ✔ 展出時間：10:00-18:00<br>
                                ✔ 展出地點：高雄市前鎮區成功二路39號<br>
                            </p>
                            <small style="float:right;">We will be there soon!</small>
                        </div>',
    'tcbw2'          => '<div class="w3-adv-right2-text">
                            <h3>台中週自行車 2019</h3>
                            <p>10/16 - 10/18, 2019, 台中</p>
                        </div>
                        <div class="w3-adv-right2-text-para">
                            <h4>Events</h4><br>
                            <p>
                                ✔  TBW is the most important trade show in the bicycle industry.<br>
                                ✔  Its unique format allows quality face to face time between buyers and suppliers.<br>
                                ✔  All this takes place inside five star hotels so you can work, sleep and play all in one location without the hassle of trekking in and out of an exhibition center. TBW is run by industry and is completely non-profit.
                            </p>
                            <small style="float:right;">We will be there soon!</small>
                        </div>',
    'info1'         => '<p style="float:right">
                            資料來源:
                            <!--「<a target="_blank" href="http://www.cbmexpo.com/">中國孕嬰童展、童裝展</a>」
                            「<a target="_blank" href="http://www.taichungbikeweek.com/">Taichung Bike Week</a>」-->
                            「<a target="_blank" href="https://ksmombaby-fair.top-link.com.tw/">2020 高雄嬰兒與孕媽咪用品展</a>」
                            「<a target="_blank" href="https://www.taipeicycle.com.tw/">2019 台北國際自行車展</a>」
                            <!--「<a target="_blank" href="https://ksmombaby-fair.top-link.com.tw/home">2018 高雄嬰兒與孕媽咪用品展</a>」-->
                        </p>',
    'contactus'     => '聯絡我們',
    'contactdetail' => '<h4>聯絡方式</h4>
                        <p style="font-size:25px;" class="cnt-p">創星有限公司台灣分公司</p>
                        <div class="agileits-social-address">
                            <p>710台南市永康區中山路296號</p>
                            <p>電話 : 06-2028038</p>
                            <p>傳真 : 06-2028038</p>
                            <p>電子信箱 :
                                <a href="mailto:sales@innovative.tw">sales@innovative.tw</a>
                            </p>
                        </div>',
    'open1'         => '台南小遊龍安平站開幕',
    'open2'         => '宜蘭關山汽車TR9-02冬山站開幕',
    'open3'         => '台南樂享學永康站開幕',
    'open4'         => '台東關山汽車TR9-10關山站開幕',
    'open5'         => '台東環鎮租車池上站開幕',
    'open6'         => '新北市福隆驛站開幕',
    'open7'         => 'Taipei Cycle 台北國際自行車展覽會',
    'open8'         => '花蓮關山汽車TR9-09玉里站開幕',
    'open9'         => '台東阿勝租車站開幕',
    'open10'        => '台中起站租車豐原站開幕',
    'open11'        => '高雄嬰兒與孕媽咪用品展',
    'open12'        => '台北世貿嬰兒與孕媽咪用品展',
    'open13'        => '台中週自行車',
    'about1'        => '<p class="hideme">Innovative 安圭拉商創星有限公司 台灣分公司創立於2016年，身為品牌代理商，我們秉持著尋找創新且設計新穎的嬰幼兒戶外產品，訴求提供安全又有保障的商品給全家人使用。對小孩及家長而言，全家人共同進行戶外活動，是小孩成長階段中重要的一個環節。基於這個原則，創星代理了奧地利品牌-nikimotion、美國品牌-veer、台灣品牌-ikin，希望能提供高品質的產品，讓您全家享受戶外出遊的美好時光。<br></p><br>',
    'imglink'       => '圖片連結',
    'ngeh'          => '台北世貿中心南港展覽館 4樓 N1230攤位',
    'ngeh2'         => '台北世貿中心南港展覽館 4樓 N0718攤位',
    'ngeh3'         => '台北世貿中心南港展覽館2館 4樓 R0520攤位',
    'tbme1'         => '台北世貿一館#C450及#C452攤位',
    'kse'           => '高雄展覽館 Veer & Nikimotion 547-549攤位',
    'tcbw1'         => '@Booth#1363&1364攤位 13樓',
];

