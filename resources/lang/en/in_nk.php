<?php

return [
    'p'       => 'Product',
    'fc'      => 'Fabric color',
    'dc1'     => '<strong class="hideme">Description</strong><br>
            <ul class="hideme">
                <li class="hideme">Easy folding system with “central folding belt”</li>
                <li class="hideme">Continuously adjustable backrest (50cm long)</li>
                <li class="hideme">Stands when folded</li>
                <li class="hideme">Removable and washable fabric parts</li>
                <li class="hideme">Front bar detachable and can be opened from both sides</li>
                <li class="hideme">Large shopping basket below the backrest and underneath the seat</li>
                <li class="hideme">Viewing window on the roof-back</li>
                <li class="hideme">5-point belt system</li>
                <li class="hideme">Pram optional available</li>
            </ul><br>
            <strong class="hideme">Product Spec</strong><br>
            <ul class="hideme">
                <li class="hideme">Weight 8.6 kg</li>
                <li class="hideme">Dimensions 68 x 59 x 30 cm</li>
                <li class="hideme">Width > 60cm</li>
            </ul><br>
            <strong class="hideme">Accessory</strong><br>
            <ul class="hideme">
                <li class="hideme">MAXI-COSI Car Seat Adapter</li>
                <li class="hideme">Cup Holder</li>
                <li class="hideme">Bug Net</li>
                <li class="hideme">Rain Cover</li>
            </ul><br>',
    'ac'      => 'Accessories',
    'dc2'     => '<strong class="hideme">Description</strong><br>
            <ul class="hideme">
                <li class="hideme">Easy folding system with “central folding belt”</li>
                <li class="hideme">Continuously adjustable backrest (50cm long)</li>
                <li class="hideme">Stands when folded</li>
                <li class="hideme">Large shopping basket below the backrest and underneath the seat</li>
                <li class="hideme">5-point belt system</li>
                <li class="hideme">Pram optional available</li>
            </ul><br>
            <strong class="hideme">Product Spec</strong><br>
            <ul class="hideme">
                <li class="hideme">Weight 7.6 kg</li>
                <li class="hideme">Dimensions 68 x 55 x 30 cm</li>
                <li class="hideme">Width > 60cm</li>
            </ul><br>
            <strong class="hideme">Accessory</strong><br>
            <ul class="hideme">
                <li class="hideme">MAXI-COSI Car Seat Adapter</li>
                <li class="hideme">Cup Holder</li>
                <li class="hideme">Bug Net</li>
                <li class="hideme">Rain Cover</li>
            </ul><br>',
    'dc3'     => '<strong class="hideme">Description</strong><br>
            <ul class="hideme">
                <li class="hideme">Reversible seat</li>
                <li class="hideme">Stroller can be folded with seat in both directions</li>
                <li class="hideme">Stands when folded</li>
                <li class="hideme">One-handed back rest adjustment</li>
                <li class="hideme">Front bar detachable and can be opened from both sides</li>
                <li class="hideme">Viewing window on the roof-back</li>
                <li class="hideme">5-point belt system</li>
                <li class="hideme">Pram optional available</li>
            </ul><br>
            <strong class="hideme">Product spec</strong><br>
            <ul class="hideme">
                <li class="hideme">Weight	14.5 kg</li>
                <li class="hideme">Dimention	90 x 54 x 29 cm</li>
            </ul><br>
            <strong class="hideme">Accessory</strong><br>
            <ul class="hideme">
                <li class="hideme">MAXI-COSI Car Seat Adapter</li>
                <li class="hideme">Cup Holder</li>
                <li class="hideme">Bug Net</li>
                <li class="hideme">Rain Cover</li>
            </ul><br>',
    'nkac'    => 'Nikimotion Accessories',
    'acintro' => 'Accessories Introduction',
    'afac'    => 'Autofold Accessories',
    'aflac'   => 'Autofold Lite Accessories',
    'blac'    => 'Blade Accessories',
    'back'    => ' BACK ',
    'acc1t'   => 'CUP HOLDER',
    'acc8t'   => 'AUTOFOLD ADAPTER',
    'acc9t'   => 'MOSQUITO NET',
    'acc10t'  => 'RAINCOVER',
    'acc12t'  => 'AUTOFOLD LITE FRONT BAR',
    'acc13t'  => 'BLADE ADAPTER',
    'acc16t'  => '手扶把',
    'acc17t'  => '手扶把置物包',
    'acc1'    => '<h3>CUP HOLDER</h3><br><p><strong>The Cup Holder from nikimotion is ideal for your and your darlings drinks!</strong><br><br>To be attached to the side of the pushchair by means of the fastening hook on the handle</p><br>',
    'acc2'    => '<h3>FOOTMUFF</h3><br><p><strong>The nikimotion footmuff protects your darling against freeze, wind and rain.</strong><br><br>headpiece with drawstring attachable with the belt system of the baby carriage.</p><br>',
    'acc3'    => '<h3>NAPPY BAG</h3><br><p><strong>The nikimotion nappy bag offers a lot of space for all the things your darling need. Spacious like a sportsbag for all the important things to stow.</strong><br><br><li>can be used as a backpack</li><li>suitable for all nikimotion baby carriages</li><li>padded changing mat</li><li>diaper bag</li><li>thermo pocket for the baby bottle</li><li>mobile phone pocket</li><li>additional carrying strap</li></p><br>',
    'acc4'    => '<h3>ORGANIZER</h3><br><p><strong>With the nikimotion Organizer you have all important things always within reach.</strong><br><br><li>suitable as a mobile phone & cup holder or as a spacious pocket</li><li>with Velcro straps attached to the stroller</li></p><br>',
    'acc5'    => '<h3>PARASOL</h3><br><p><strong>The parasol offers pleasant shadow and protect against direct sun.</strong><br><br><li>UV protection 50+</li><li>Can be applied to round and oval frame tubes using a universal clamp</li></p><br>',
    'acc6'    => '<h3>MOSQUITO NET FOR PRAM</h3><br><p><strong>The moskito net is a reliable protection against insects.</strong><br><br> <li>fine mesh material for optimum ventilation</li><li>reclosable storage case</li></p><br>',
    'acc7'    => '<h3>RAIN COVER FOR PRAM</h3><br><p><strong>The rain cover fits perfectly onto the carrycot of your nikimotion AUTOFOLD & BLADE.</strong><br><br><li>PVC-free</li><li>great windowed with vendilation</li><li>easy to clean</li><li>protection of rain, snow and wind</li><li>age recommendation: from birth</li><li>color: Transparent</li></p><br>',
    'acc8'    => '<h3>ADAPTER</h3><br><p><strong>The Adapter for your AUTOFOLD stroller for setting the Baby Pram or Maxi Cosi.</strong><br><br>* The adapter allows the Maxi Cosi baby carrier (Cabrio Fix, Pebble, Pebble Plus, Citi)</p><br>',
    'acc9'    => '<h3>MOSQUITO NET</h3><br><p><strong>The mosquito net for the nikimotion AUTOFOLD is a reliable protection against insects.</strong><br><br><li>fine mesh material for optimum ventilation</li><li>reclosable storage bag</li></p><br>',
    'acc10'   => '<h3>RAINCOVER</h3><br><p><strong>The AUTOFOLD raincover protects your darling against rain, wind and snow. It fits perfectly on the nikimotion AUTOFOLD and AUTOFOLD LITE stroller.</strong><br><br><li>PVC-free</li><li>great windowed with vendilation</li><li>easy to clean</li><li>protection of rain, snow and wind</li><li>age recommendation: from birth</li><li>color: Transparent</li><li>also suitable for AUTOFOLD LITE</li></p><br>',
    'acc11'   => '<h3>CARRYCOT</h3><br><p>The<strong> AUTOFOLD pram </strong>provides an additional extension to the buggy, allowing a child up to six months old to sleep and lie down.<br><br><li>complete with upholstery and mattress</li><li>textile parts remov- and washable</li><li>protected against UV rays</li><li>water repellent impregnation</li></p><br>',
    'acc12'   => '<h3>FRONT BAR</h3><br><p><strong>Front bar for Autofold Lite black</strong></p><br>',
    'acc13'   => '<h3>ADAPTER</h3><br><p><strong>The Adapter for your nikimotion BLADE stroller for setting the Baby Pram or Maxi Cosi</strong><br><br>* The adapter allows the Maxi Cosi baby carrier (Cabrio Fix, Pebble, Pebble Plus, Citi)</p><br>',
    'acc14'   => '<h3>MOSQUITO NET</h3><br><p><strong>Summer nights without insect bites – the BLADE mosquito net makes it possible!</strong><br><br><li>fine mesh material for optimum ventilation</li><li>reclosable storage bag</li></p><br>',
    'acc15'   => '<h3>RAINCOVER</h3><br><p><strong>The BLADE raincover protects your darling against rain, wind and snow. It fits perfectly on the nikimotion BLADE sport seat.</strong><br><br><li>PVC-free</li><li>great windowed with vendilation</li><li>easy to clean</li><li>protection against rain, snow and wind</li><li>age recommendation: from birth</li><li>color: transparent</li></p><br>',
    // Edited on 2020/04/15
    'acc16'   => '<h3>手扶把</h3><br>手扶把，為小朋友安全提供多一道防護。<p>',
    'acc17'   => '<h3>手扶把置物包</h3><br>手把置物包方便讓媽媽放置手機、錢包、鑰匙、飲料；防波水材質布料，讓媽媽們在下雨天時也能安心使用。<p>',
];
