<?php

return [
    'home'          => 'Home',
    'aboutus'       => 'About Us',
    'news'          => 'News',
    'nikimotion'    => 'Nikimotion',
    'autofold'      => 'Autofold',
    'autofoldlite'  => 'Autofold Lite',
    'blade'         => 'Blade',
    'accessories'   => 'Accessories',
    'veer'          => 'Veer',
    'ebike'         => 'E-Bike',
    'rentplace'     => 'Rentplace',
    'brand'         => 'Brand',
    'store'         => 'Store',
    'welcome'       => 'Welcome',
    'welcome1'      => '<h3 align="center">Welcome to Innovative</h3><br><p>　　The products introduced on this website are the best quality brands we have at home and abroad. Chuangxing Innovative website is a life website that combines baby products and outdoor activities. If you have any questions about related products, please feel free to contact us.</p>',
    'wel1'          => 'The task for the product designers was to develop a special product design created out of the unique folding mechanism. The first prototype was created in 2015 and was ultimately the birthdays of the nikimotion models Blade and Autofold.',
    'wel2'          => 'PUSH, PULL, PUSH-ALONG, STOW & GO Push it like a stroller, pull it like a wagon or push it along like modern luggage. Proper design makes it suprisingly easy to maneuver.Fast and compact folding for easy transport. Sits upright on end for compact storage.',
    'wel3'          => 'E-Bikes brands and models powered by MPF Drive Mid-Motor System.',
    'service'       => 'service',
    'service1'      => '<h5>Experience product</h5><p>Click &quot;E-Bike&quot; to go to the province&prime;s car rental point on-site rental experience...</p>',
    'service2'      => '<h5>technical support</h5><p>technical support...</p>',
    'service3'      => '<h5>Product warranty</h5><p>Product warranty...</p>',
    'exhibitor'     => 'Exhibition message',
    'exh1'          => '<div class="w3-adv-right2-text">
                            <h3>第18屆CBME中國孕嬰童展、童裝展</h3>
                            <p>2018.7.25-27, 國家會展中心(上海)</p>
                        </div>
                        <div class="w3-adv-right2-text-para">
                            <p>
                                ✔ 266,548平米展示空間，匯聚全品類孕嬰童產品<br>
                                ✔ 2,900+家展商在2018 CBME中國現場展示4,300+個品牌<br>
                                ✔ 全球知名品牌齊聚CBME中國<br>
                                ✔ 與90,000+位優質買家建立業務聯繫，拓展商機 <br>
                                ✔ 整合式營銷，提升品牌曝光度和影響力
                            </p>
                            <small style="float:right;">We will be there soon!</small>
                        </div>',
    'exh2'          => '<div class="w3-adv-lef1-text">
                            <h3>2018 Taichung Bike Week</h3>
                            <p>September 26-28, 2018, Taichung</p>
                        </div>
                        <div class="w3-adv-lef1-text2">
                            <p>
                                <span>"</span>By the bike industry for the bike industry.<span>"</span>
                            </p>
                            <small style="float:right;">We will be there soon!</small>
                        </div>',
    'exh3'          => '<div class="w3-adv-right2-text">
                            <h3>2018台北婦幼展覽</h3>
                            <p>Oct 10 - 14, 2018, Taipei</p>
                        </div>
                        <div class="w3-adv-right2-text-para">
                            <h4>亞洲首選 台灣唯一國際級 媽咪寶貝採購盛會</h4><br>
                            <p>　　參展廠商包含孕嬰用品國際品牌原廠、月子餐/中心、保養品、彌月蛋糕禮盒、寶貝育教娛樂商品、臍帶血生物科技等各大母嬰產業龍頭，推出2018年度新品，聯手規劃最優惠經濟的孕嬰商品，打造最大規模且完整的婦幼採購一站式平台。</p>
                            <small style="float:right;">We will be there soon!</small>
                        </div>',
    'exh4'          => '<div class="w3-adv-right2-text">
                            <h3>Taipei Cycle 2019</h3>
                            <p>Mar 27-Mar 30, 2019, Taipei</p>
                        </div>
                        <div class="w3-adv-right2-text-para">
                            <h4>Events</h4><br>
                            <p>
                                ✔  Mar 27 to 28: only open to professional buyers. Children under 12 are not allowed to enter on these days.<br>
                                ✔  Mar 29 to 30: Open to buyers and the public. Members of the general public and children under 12 must buy tickets to enter the show ground. (The ticket office will close 2 hours before the show closes.)
                            </p>
                            <small style="float:right;">We will be there soon!</small><br>
                        </div>',
    'exh5'          => '<div class="w3-adv-right2-text">
                            <h3>台北世貿嬰兒與孕媽咪用品展 2020</h3>
                            <p>06/25 - 06/28, 2020, Kaohsiung</p>
                        </div>
                        <div class="w3-adv-right2-text-para">
                            <h4>高雄嬰兒與孕媽咪用品展 (夏季展)</h4><br>
                            <p>
                                ✔ 展出日期：2020年6月25日(星期四)－6月28日(星期日)<br>
                                ✔ 展出時間：10:00-18:00<br>
                                ✔ 展出地點：高雄市前鎮區成功二路39號<br>
                            </p>
                            <small style="float:right;">We will be there soon!</small>
                        </div>',
    'tcbw2'          => '<div class="w3-adv-right2-text">
                            <h3>台中週自行車 2019</h3>
                            <p>10/16 - 10/18, 2019, 台中</p>
                        </div>
                        <div class="w3-adv-right2-text-para">
                            <h4>Events</h4><br>
                            <p>
                                ✔  TBW is the most important trade show in the bicycle industry.<br>
                                ✔  Its unique format allows quality face to face time between buyers and suppliers.<br>
                                ✔  All this takes place inside five star hotels so you can work, sleep and play all in one location without the hassle of trekking in and out of an exhibition center. TBW is run by industry and is completely non-profit.
                            </p>
                            <small style="float:right;">We will be there soon!</small>
                        </div>',
    'info1'         => '<p style="float:right">
                            Source:
                            「<a target="_blank" href="https://ksmombaby-fair.top-link.com.tw/">2020 高雄嬰兒與孕媽咪用品展</a>」
                            「<a target="_blank" href="https://www.taipeicycle.com.tw/">2019 台北國際自行車展</a>」
                        </p>',
    'contactus'     => 'Contact Us',
    'contactdetail' => '<h4>Get in touch with us</h4>
                        <p style="font-size:25px;" class="cnt-p">Innovative Taiwan Branch</p>
                        <div class="agileits-social-address">
                            <p>No.296, Zhong Shan Road, Yong Kang District, Tainan, Taiwan</p>
                            <p>Telephone : 06-2028038</p>
                            <p>FAX : 06-2028038</p>
                            <p>Email :
                                <a href="mailto:sales@innovative.tw">sales@innovative.tw</a>
                            </p>
                        </div>',
    'open1'         => '「Tainan」 Anping Xiaoyou Long Station Opening',
    'open2'         => '「Ilan」 Dongshan TR9-02 Station Opening',
    'open3'         => '「Tainan」 Yongkang O-Fami Station Opening',
    'open4'         => '「Taitung」 Guanshang TR9-10 Station Opening',
    'open5'         => '「Taitung」 Chishang Station Opening',
    'open6'         => '「New Taipei」 Fulong Station Opening',
    'open7'         => '「Taipei International Cycle Show」',
    'open8'         => '「Hualien」 Yuli Station Opening',
    'open9'         => '「Taitung」 A-Sheng Station Opening',
    'open10'        => '「Taichung」 Fengyuan Station Opening',
    'open11'        => '「Kaohsiung」Baby and Pregnant Mommy Supplies Exhibition',
    'open12'        => '「Taipei」Baby and Pregnant Mommy Supplies Exhibition',
    'open13'        => '「TaiChung」Bike Week',
    'about1'        => '<p class="hideme">Innovative was established in 2016 in Tainan, Taiwan. We are the distributor who is focusing on searching the innovative and creative outdoor baby related products. Outdoor activities with family is always one of the most important part in children’s life and we believe our children deserve qualified and high quality products to explorer the world with their family. Currently, we are the exclusive distributor of brand NIKIMOTION / VEER / IKIN for Asia market.<br></p><br>',
    'imglink'       => 'Image Link',
    'ngeh'          => 'TAIPEI NANGANG EXHIBITION CENTER STATION, 4F-N1230',
    'ngeh2'         => 'TAIPEI NANGANG EXHIBITION CENTER STATION, 4F-N0718',
    'ngeh3'         => 'TAIPEI NANGANG EXHIBITION CENTER STATION, HALL 2, 4F-R0520',
    'tbme1'         => 'Taipei world Trade Center Exhibition Hall-1, #C450 & #C452',
    'kse'           => 'Kaohsiung Exhibition Center - Veer & Nikimotion 547-549',
    'tcbw1'         =>'@Booth#1363&1364, 13F',
];

