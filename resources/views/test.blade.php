<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <span>Product Category: </span>
    <select style="width: 200px" class="productcategory" id="prod_cat_id">

        <option value="0" disabled="true" selected="true">-Select-</option>
        @foreach($prod as $cat)
            <option value="{{$cat->id}}">{{$cat->type}}</option>
        @endforeach
    </select>
    <span>Product Name: </span>
    <select style="width: 200px" class="productname" id="productname">
        <option value="0" disabled="true" selected="true">Product Name</option>
    </select>

    <span>Product Price: </span>
    <input type="text" class="prod_price">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        $(document).on('change','#prod_cat_id',function(){
            var cat_id=$(this).val();
            var div=$(this).parent();
            var op=" ";
            $.ajax({
                type:'get',
                url:'{!!URL::to('findProductName')!!}',
                data:{'id':cat_id},
                success:function(data){
                    op+='<option value="0" selected disabled>chose product</option>';
                    for(var i=0;i<data.length;i++){
                        op+='<option value="'+data[i].payment+'">'+data[i].time+'</option>';
                    }
                    div.find('#productname').html(" ");
                    div.find('#productname').append(op);
                },
                error:function(){
                }
            });
        });
    });
</script>



</body>
</html>