@extends('bikerental.main')
@section('content')
<div id="main" class="wrapper style1">
    <div class="container">
        <header class="major">
            <h2>關山親水公園</h2>
            <p>Guanshan Water Park</p>
        </header>

        <!-- Content -->
        <section id="content">
            <a  class="image fit"><img src="{{asset('images/bikerental/taitung3.jpg')}}" alt="關山親水公園" /></a>
            <h3>{{trans('bikerental_index.wf')}}</h3>
            <h4>{{trans('bikerental_location.intro')}}</h4>
            <p>{!!trans('bikerental_location.wfintro')!!}</p>
            <h4>{{trans('bikerental_location.p')}}</h4>
            <p>{{trans('bikerental_location.wfp')}}</p>
            <h4>{{trans('bikerental_location.oh')}}</h4>
            <p>{!!trans('bikerental_location.wfoh')!!}</p>
            <h4>{{trans('bikerental_location.ad')}}</h4>
            <p>{{trans('bikerental_location.wfad')}}</p>
        </section>
        <section>
            <div>
                <h4>{{trans('bikerental_location.gm')}}</h4>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3671.5514783984477!2d121.16988761434469!3d23.0402350214508!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x346f0a02b149e503%3A0xdd7682ab2be46e3f!2zOTU25Y-w5p2x57ij6Zec5bGx6Y6u6ZqG55ub6LevMeiZnw!5e0!3m2!1szh-TW!2stw!4v1522638387083"
                width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </section>
    </div>
</div>
@endsection