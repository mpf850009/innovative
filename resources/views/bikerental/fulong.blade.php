@extends('bikerental.main')
@section('content')
<div id="main" class="wrapper style1">
    <div class="container">
        <header class="major">
            <h2>福隆驛站</h2>
            <p>Fulong Station</p>
        </header>

        <!-- Content -->
        <section id="content">
            <a  class="image fit"><img src="{{asset('images/bikerental/fulong1.jpg')}}" alt="福隆驛站" /></a>
            <h3>{{trans('bikerental_index.fl')}}</h3>
            {!!trans('bikerental_index.fldt1')!!}
            {!!trans('bikerental_index.fldt2')!!}
            <h4>{{trans('bikerental_location.ow')}}</h4>
            <p>
                <a href="http://www.ifulong.com.tw/">{{trans('bikerental_index.fl')}}</a>
            </p>                
        </section>
        <section>
            <div>
                <h4>{{trans('bikerental_location.gm')}}</h4>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3615.5356613734557!2d121.94298461500594!3d25.01588938398087!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x345d5c5484b55c5b%3A0x50fe76487401c49f!2zTVBGIERyaXZlIOemj-mahuermQ!5e0!3m2!1szh-TW!2stw!4v1515648782393"
                    width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </section>
    </div>
</div>
@endsection