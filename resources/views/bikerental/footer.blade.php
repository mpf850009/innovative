<footer id="footer">
    <!--
    <ul class="icons">
        <li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
        <li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
        <li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
        <li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
        <li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
        <li><a href="#" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
    </ul>-->
    <ul class="copyright">
        <li>&copy; Copyright (C) 2017 Unique Product & Design Co., Ltd. All Rights Reserved.</li>
        <li><a target="_blank" href="http://www.mpfdrive.tw/">MPF Drive 穩正企業股份有限公司</a></li>
        <li>Design: <a target="_blank" href="http://html5up.net">HTML5 UP</a></li>
        @if($active=='index')
            <li>
                {{trans('bikerental_index.ref')}}：
                <a target="_blank" href="https://tour.taitung.gov.tw">{!!trans('bikerental_index.ttgt')!!}</a>
            </li>
        @elseif($active=='fulong')
            <li>
                {{trans('bikerental_index.ref')}}：<a target="_blank" href="http://www.ifulong.com.tw">{{trans('bikerental_index.fl')}}</a>
            </li>
        @elseif($active=='jiaoxi')
            <li>
                {{trans('bikerental_index.ref')}}：<a target="_blank" href="http://www.natural-hotel.com.tw">自然風溫泉會館</a>
            </li>
        @elseif($active=='dongshan')
            <li>
                {{trans('bikerental_index.ref')}}：<a target="_blank" href="https://travel.yilan.tw/zh-tw/attraction/details/345">{{trans('bikerental_location.dsh4')}}</a>
            </li>
        @elseif($active=='asheng_h' || $active=='asheng_t')
            <li>
                {{trans('bikerental_index.ref')}}：<a target="_blank" href="http://bike.e089.com.tw/">{{trans('bikerental_index.as')}}</a>
            </li>
        @elseif($active=='park_water')
            <li>
                {{trans('bikerental_index.ref')}}：<a target="_blank" href="https://tour.taitung.gov.tw/zh-tw/attraction/details/265">{{trans('bikerental_index.ttgt')}}</a>
            </li>
        @elseif($active=='winhappy')
            <li>
                {{trans('bikerental_index.ref')}}：<a target="_blank" href="http://0425132875.tw.tranews.com/">{{trans('bikerental_index.df1')}}</a>
            </li>
        @elseif($active=='dragon')
            <li>
                {{trans('bikerental_index.ref')}}：<a target="_blank" href="http://www.bubucars.com.tw/store/index.php?useno=R010801">{{trans('bikerental_index.ld')}}</a>
            </li>
        @endif

    </ul>
</footer>