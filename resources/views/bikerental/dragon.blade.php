@extends('bikerental.main')
@section('content')
<div id="main" class="wrapper style1">
    <div class="container">
        <header class="major">
            <h2>台南安平小遊龍自行車</h2>
            <p>Tainan Anping Little Dragon Bike Station</p>
        </header>

        <!-- Content -->
        <section id="content">
            <a  class="image fit"><img src="{{asset('images/bikerental/tainan1.jpg')}}" alt="小遊龍自行車" /></a>
            <h3>{{trans('bikerental_index.ld')}}</h3>
            <h4>{{trans('bikerental_location.intro')}}</h4>
            <p>{!!trans('bikerental_location.lddt')!!}</p>
            <h4>{{trans('bikerental_location.oh')}}</h4>
            <p>{!!trans('bikerental_location.ldoh')!!}</p>
            <h4>臉書Facebook</h4>
            <p>
                <a target="_blank" href="https://goo.gl/T38PWs">{{trans('bikerental_index.ld')}}</a>
            </p>
            <h4>{{trans('bikerental_location.p')}}</h4>
            <p>
                062285472<br>
                0936-909988
            </p>
            <h4>{{trans('bikerental_location.ad')}}</h4>
            <p>{{trans('bikerental_location.ldad')}}</p>
        </section>
        <section>
            <div>
                <h4>{{trans('bikerental_location.gm')}}</h4>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2672.9952184589843!2d120.1644439252058!3d23.00277736544061!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x346e7602d4a79685%3A0x226cbc17d86c3793!2z5bCP6YGK6b6N6Ieq6KGM6LuK!5e0!3m2!1szh-TW!2stw!4v1527556525027" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </section>
    </div>
</div>
@endsection