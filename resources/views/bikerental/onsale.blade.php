@extends('bikerental.main')
@section('content')
<style>
    p{
        font-size:2.5em;
    }
    p b{
        color:white;
        font-family:Courier;
        font-size:1.4cm;
    }
</style>
<div id="main" class="wrapper style1">
    <div class="container">
        <header class="major">
            <h2>{{ trans('bikerental_bike.si') }}</h2>
            <p>{{ trans('bikerental_bike.sid') }}</p>
        </header>

    <!-- 小折小徑車 oA -->
        <section>
            <h3>{{ trans('bikerental_bike.type1') }}</h3>
            <div class="row">
                <div class="6u 12u(xsmall)">
                    <a class="image fit"><img src="{{asset('images/bikerental/ebike1.jpg')}}" alt="" /></a>
                    <p>
                        {{ trans('bikerental_bike.p') }}：<b>NT$60,800</b>
                    </p>
                </div>
                <div class="6u 12u(xsmall)">
                    <div class="table-wrapper">
                        <table>
                            <tbody>
                                <tr>
                                    <td>{{ trans('bikerental_bike.frame') }}</td>
                                    <td>{{ trans('bikerental_bike.al') }}#6061</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.groupest') }}</td>
                                    <td>{{ trans('bikerental_bike.groupest1') }}</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.crankset') }}</td>
                                    <td>{{ trans('bikerental_bike.crankset1') }}</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.cassette') }}</td>
                                    <td>11/32T</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.handlebar') }}</td>
                                    <td>{{trans('bikerental_bike.al')}}31.8(620W,640W,660W)ISO4210M</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.seatpost') }}</td>
                                    <td>{{ trans('bikerental_bike.al') }}31.8 EX:105 L:41 ISO4210M</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.tires') }}</td>
                                    <td>700C*35C</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.wheelset') }}</td>
                                    <td>700C*32H {{ trans('bikerental_bike.dc') }} BK</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.weight') }}</td>
                                    <td>21.5KG</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.color') }}</td>
                                    <td>{{ trans('bikerental_bike.color1') }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <ul>{!! trans('bikerental_bike.rules') !!}</ul>
                </div>
            </div>
        </section>
        <section><p></p>
            <h3>{{ trans('bikerental_bike.type2') }}</h3>
            <div class="row">
                <div class="6u 12u(xsmall)">
                    <a class="image fit"><img src="{{asset('images/bikerental/ebike2.jpg')}}" alt="" /></a>
                    <p>
                        {{ trans('bikerental_bike.p') }}：<b>NT$69,800</b>
                    </p>
                </div>
                <div class="6u 12u(xsmall)">
                    <div class="table-wrapper">
                        <table>
                            <tbody>
                                <tr>
                                    <td>{{ trans('bikerental_bike.frame') }}</td>
                                    <td>{{ trans('bikerental_bike.al') }}#6061</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.groupest') }}</td>
                                    <td>{{ trans('bikerental_bike.groupest1') }}</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.crankset') }}</td>
                                    <td>{{ trans('bikerental_bike.crankset2') }}</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.cassette') }}</td>
                                    <td>11/32T</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.handlebar') }}</td>
                                    <td>{{ trans('bikerental_bike.al') }}31.8(620W,640W,660W)ISO4210M</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.seatpost') }}</td>
                                    <td>{{ trans('bikerental_bike.al') }}31.8 EX:105 L:41 ISO4210M</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.tires') }}</td>
                                    <td>700C*35C</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.wheelset') }}</td>
                                    <td>700C*32H {{ trans('bikerental_bike.dc') }} BK</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.weight') }}</td>
                                    <td>19.5KG</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.color') }}</td>
                                    <td>{{ trans('bikerental_bike.color2') }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <ul>{!! trans('bikerental_bike.rules') !!}</ul>
                </div>
            </div>
        </section>
        <section>
            <h3>{{trans('bikerental_bike.type3')}}</h3>
            <div class="row">
                <div class="6u 12u(xsmall)">
                    <a class="image fit"><img src="{{asset('images/bikerental/ebike3.jpg')}}" alt="" /></a>
                    <p>
                        {{ trans('bikerental_bike.p') }}：<b>NT$79,800</b>
                    </p>
                </div>
                <div class="6u 12u(xsmall)">
                    <div class="table-wrapper">
                        <table>
                            <tbody>
                                <tr>
                                    <td>{{ trans('bikerental_bike.frame') }}</td>
                                    <td>{{ trans('bikerental_bike.al') }}#6061</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.groupest') }}</td>
                                    <td>{{ trans('bikerental_bike.groupest1') }}</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.crankset') }}</td>
                                    <td>1/2*11/128&quot;*36T</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.cassette') }}</td>
                                    <td>11/32T</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.handlebar') }}</td>
                                    <td>{{ trans('bikerental_bike.al') }}31.8 700W ISO4210-M</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.seatpost') }}</td>
                                    <td>{{ trans('bikerental_bike.al') }}31.8 EX:80*-7&deg; 90*-7&deg; L:38 ISO4210-M</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.tires') }}</td>
                                    <td>26*4&quot;</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.wheelset') }}</td>
                                    <td>26*32H {{ trans('bikerental_bike.sc') }} {{ trans('bikerental_bike.slh') }} BK</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.weight') }}</td>
                                    <td>23.3KG</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('bikerental_bike.color') }}</td>
                                    <td>{{ trans('bikerental_bike.color3') }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <ul>{!!trans('bikerental_bike.rules')!!}</ul>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection