@extends('bikerental.main')
@section('content')
<div id="main" class="wrapper style1">
    <div class="container">
        <header class="major">
            <h2>台東池上日光車行</h2>
            <p>Chishan Sunny Bike Station</p>
        </header>

        <!-- Content -->
        <section id="content">
            <a  class="image fit"><img src="{{asset('images/bikerental/chishang.jpg')}}" alt="關山TR9" /></a>
            <h3>{{trans('bikerental_location.csh3')}}</h3>
            <h4>{{trans('bikerental_location.intro')}}</h4>
            <p>　　{!!trans('bikerental_location.csintro')!!}</p>
            <h4>{{trans('bikerental_location.p')}}</h4>
            <p>{{trans('bikerental_location.csp')}}</p>
            <h4>{{trans('bikerental_location.oh')}}</h4>
            <p>{{trans('bikerental_location.csoh')}}</p>
            <h4>{{trans('bikerental_location.ad')}}</h4>
            <p>{{trans('bikerental_location.csad')}}</p>
            <h4>{{trans('bikerental_location.ow')}}</h4>
            <p><a href="http://0918883692.strikingly.com/">{{trans('bikerental_location.csh3')}}</a></p>
        </section>
        <section>
            <div>
                <h4>{{trans('bikerental_location.gm')}}</h4>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3669.2421679335966!2d121.2185603143362!3d23.124821918303162!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x346f0c8b3dd31d7b%3A0x6264bf4d8dd4320e!2z5pel5YWJ6LuK6KGM!5e0!3m2!1szh-TW!2stw!4v1528875660043"
                width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </section>
    </div>
</div>
@endsection