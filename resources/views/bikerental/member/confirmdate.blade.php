<!DOCTYPE HTML>
<html>
	<head>
		<title>MPF租車系統-選擇時間</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="{{asset('css/bikerental/main.css')}}" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--Requirement jQuery-->
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<!--Load Script and Stylesheet -->
		<script type="text/javascript" src="{{asset('dtpicker/jquery.simple-dtpicker.js')}}"></script>
		<link type="text/css" href="{{asset('dtpicker/jquery.simple-dtpicker.css')}}" rel="stylesheet" />
		<!---->

		<style type="text/css">
			body { padding-left: 2%; padding-bottom: 100px; color: #101010; }
			footer{ font-size:small;position:fixed;right:5px;bottom:5px; }
			a:link, a:visited  { color: #0000ee; }
			pre{ background-color: #eeeeee; margin-left: 1%; margin-right: 2%; padding: 2% 2% 2% 5%; }
			p { font-size: 0.9rem; }
			ul { font-size: 0.9rem; }
			hr { border: 2px solid #eeeeee; margin: 2% 0% 2% -3%; }
			h3 { border-bottom: 2px solid #eeeeee; margin: 2rem 0 2rem -1%; padding-left: 1%; padding-bottom: 0.1em; }
			h4 { border-bottom: 1px solid #eeeeee; margin-top: 2rem; margin-left: -1%; padding-left: 1%; padding-bottom: 0.1em; }
		</style>
		<script language="javascript">
			function passBackToParent(){
			  opener.document.formAdd.datetime.value = document.childForm.date17.value;
			  window.close();
			}
		</script>
	</head>
	<body>
		<h3>營業時間為 07:00 - 18:00</h3>
		<form class="" name="childForm" action="{{route('bikerental.booking')}}" method="post">
			<div class="4u 12u$(medium)">
				<input type="text" name="date17" value="" style="color:#eeeeee;">
				<script type="text/javascript">
					$(function(){
						$('*[name=date17]').appendDtpicker({
							"inline": true,
							"futureOnly": true,
							"minTime":"07:00",
							"maxTime":"18:10"
						});
					});
				</script>
			</div>
			<div class="12u$">
				<ul class="actions">
					<li><input type="button" class="button special" value="確認日期" onclick="javascript: passBackToParent()" readonly ></li>
				</ul>
			</div>
		</form>
	</body>
</html>
