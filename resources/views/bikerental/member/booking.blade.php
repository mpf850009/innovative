@extends('bikerental.main')
@section('content')
<div id="main" class="wrapper style1">
    <div class="container">
        <header class="major">
            <h2>歡迎光臨MPF會員中心<br/>Welcome to MPF Member Center</h2>
            <p>請點選導覽列「我要租車」開始租車流程<br/>Please click Navigation bar ＂Rent a Bike＂ to start renting process</p>
        </header>
        <div class="row 150%">
            <div class="4u 12u$(medium)">

            <!-- Sidebar -->
                <section id="sidebar">
                    @include('bikerental.member.info')

                    <hr />
                    @include('bikerental.member.contact')
                </section>

            </div>
            <div class="8u$ 12u$(medium) important(medium)">

            <!-- Content -->
                <section id="content">
                    
                    <h3>我要租車<br/>I want to rent a bike</h3>
                    @if (count($errors) > 0)
                        @foreach ($errors->all() as $error)
                            <span style="color:#FFFFFF; background-color:#FF0000">{{ $error }}</span>　
                        @endforeach
                    @endif
                    <form method="post" action="{{route('bikerental.booking')}}" name="formAdd" id="formAdd" onsubmit="return check_filed(this)">
                        {{ csrf_field() }}

                        <div class="row uniform 50%">
                            <strong>姓　　名：<br/>Name</strong>
                            <div class="6u$ 12u$(xsmall)">
                                <label for = "example">{{ Auth::user()->name }}</label>
                            </div>
                            <strong>電　　話：<br/>Phone</strong>
                            <div class="6u$ 12u$(xsmall)">
                                <label for = "example">{{ Auth::user()->phone }}</label>
                            </div>

                            <div class="12u$">
                                <ul class="actions">
                                    <li><strong>租車日期：<br/>Date</strong></li>
                                    <li><input type="text"  name="datetime" id="datetime" value="<?php $_POST["date17"]="";echo $_POST["date17"];?>" readonly></li>
                                    <li><input type="button" value="請選擇租車日期Choose Date" onclick="JavaScript:window.open('{{route('bikerental.confirmdate')}}','mywindow','width=450,height=630')"  class="form-control" placeholder=""></li>
                                </ul>
                            </div><br>

                            <strong>租用車種：　<br>RentType</strong><br>
                            <select style="width: 280px;" class="biketype" id="biketype" name="type">
                                <option value="0" disabled="true" selected="true">請選擇租用車種Choose Type</option>
                                @foreach($bike as $b)
                                    <option value="{{$b->id}}">{{$b->type}}</option>
                                @endforeach
                            </select><br><br>
                            
                            <strong>租用時間：　<br>RentTime</strong><br>
                            <select style="width: 280px;" class="biketime" name="time" id="biketime" onblur="calculate();">
                                <option value="0" disabled="true" selected="true">請先行選擇車種Choose Type</option>
                            </select><br><br>

                            <strong>地　　區：　<br/>Area</strong><br>
                            <select style="width: 280px;" name="area" id="area">
                                <option value="0" disabled="true" selected="true">請選擇租借地點Choose Area</option>
                                @foreach($area as $a)
                                    <option value="{{$a->id}}">{{$a->area}}</option>
                                @endforeach
                            </select><br><br>

                            <strong>租用數量：　<br/>Quantity</strong><br>
                            <select style="width: 280px;" name="quantity" id="quantity" onblur="calculate();">
                                <option value="0" disabled="true" selected="true">請先行選擇地點Choose Area</option>
                            </select><br><br>

                            

                            <strong>付款方式：<br/>Payment</strong>
                            <div class="3u 12u$(medium)">
                                <input type="radio" id="priority-low" name="howtopay" value="1" checked>
                                <label for="priority-low">現場付款<br/>Prompt Cash</label>
                            </div>
                            <div class="3u$ 12u$(medium)">
                                <input type="radio" id="priority-normal" name="howtopay" value="2" >
                                <label for="priority-normal">自行匯款<br/>Transaction</label>
                            </div>
                            <div class="12u$">
                                <b><font color="yellow">備註：若未先行付款者將不保證現場有車<br/>PS. If you do not pay the advance will not guarantee bikes at the scene.</font></b>
                            </div>

                            <strong>小計/NT$：<br/>Total</strong>
                            <div class="3u 12u(xsmall)">
                                <input name="total" type="text" readonly/>
                            </div>

                            <div class="12u$">
                                <ul class="actions">
                                    <li><input type="submit" value="確認送出Confirm" class="special" name="button" id="button" /></li>
                                    <!--
                                    <li><input type = "button" name="submit1" id="submit1" value="回上一頁Previous Page" onclick = "window.history.back(); "></li>-->
                                    <li><input name="action" type="hidden" value="add"></li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</div>
<script>
    function calculate(){//檢查 數量 欄位 有輸入資料,否則在做計算時,就會發生錯誤了
		if(formAdd.biketype.value != 0){
			formAdd.total.value = formAdd.biketime.value * formAdd.quantity.value;
		}else{
				formAdd.total.value = '0' ;
		}
    }
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('change','#biketype',function(){
            //console.log("hmm its change");
            var type_id=$(this).val();
            //console.log(type_id);
            var div=$(this).parent();
            var op="";
            $.ajax({
                type:'get',
                url:'{!!URL::to('findProductName')!!}',
                data:{'id':type_id},
                success:function(data){
                    //console.log('success');
                    //console.log(data);
                    //console.log(data.length);
                    op+='<option value="0" selected disabled>請選擇租用時間Choose Time</option>';
                    for(var i=0;i<data.length;i++){
                    op+='<option value="'+data[i].payment+'">'+data[i].time+'</option>';
                    }
                    div.find('#biketime').html(" ");
                    div.find('#biketime').append(op);
                },
                error:function(){
                }
            });
        });
        $(document).on('change','#area',function(){
            //console.log("hmm its change");
            var area_id=$(this).val();
            //console.log(type_id);
            var div=$(this).parent();
            var op="";
            $.ajax({
                type:'get',
                url:'{!!URL::to('findQuantity')!!}',
                data:{'id':area_id},
                success:function(data){
                    console.log('success');
                    console.log(data);
                    console.log(data.length);
                    op+='<option value="0" selected disabled>請選擇租借數量Choose Quantity</option>';
                    for(var i=1;i<=data[0].quantity;i++){
                    op+='<option value="'+i+'">'+i+'</option>';
                    }
                    div.find('#quantity').html(" ");
                    div.find('#quantity').append(op);
                },
                error:function(){
                }
            });
        });
    });
</script>
@endsection