@extends('bikerental.main')
@section('content')
<div id="main" class="wrapper style1">
    <div class="container">
        <header class="major">
            <h2>歡迎光臨MPF會員中心<br/>Welcome to MPF Member Center</h2>
            <p>請點選導覽列「我要租車」開始租車流程<br/>Please click Navigation bar ＂Rent a Bike＂ to start renting process</p>
        </header>
        <div class="row 150%">
            <div class="3u 12u$(medium)">

            <!-- Sidebar -->
                <section id="sidebar">
                    @include('bikerental.member.info')

                    <hr />
                    @include('bikerental.member.contact')
                </section>

            </div>
            <div class="9u$ 12u$(medium) important(medium)">

            <!-- Content -->
                <section id="content">
                    <a class="image fit"><img src="{{asset('images/bikerental/index2.png')}}" alt="" /></a>
                    <h3>注意事項：</h3>
                    <ul>
                        <li>本公司會使用您的資料進行市場調查、客戶服務等使用。</li>
                        <li>您同意本公司以您所提供的個人資料與您進行聯絡及提供您本公司之活動訊息、服務及相關事項聯繫。</li>
                        <li>您的個人資料會被我們蒐集並受到安全的保護，本公司將嚴謹的保管您的個人資料，不會任意揭露或出售、交換、或轉讓您的任何非公開性資料予第三人。</li>
                    </ul>
                    <h3>會員功能：</h3>
                    <ul>
                        <li>免費加入會員。</li>
                        <li>每個會員可修改本身資料。</li>
                        <li>若是遺忘密碼，會員可由系統發出電子信函通知。</li>
                        <li>管理者可以修改、刪除會員的資料。</li>
                    </ul>
                </section>  
            </div>
        </div>
    </div>
</div>

@endsection