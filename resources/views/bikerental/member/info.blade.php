<section>
    <h3>會員登入成功！<br/>Sign In Suceesfully！</h3>
    <td>
        <div>

            <p>
                <strong>NO. {{ Auth::user()->id }} </strong><br>
                <strong>{{ Auth::user()->name }}</strong> 您好！ <br> <br>

                <a href="{{route('bikerental.orderdata')}}">查詢訂單Checking Orders</a> <br>
                <a href="admin/edit-account-info">帳戶管理Account Management</a>

            </p>                                

            <div class="12u$">
                <ul class="actions vertical">
                    <li><a href="{{route('bikerental.booking')}}" class="button special fit">我要租車Rent a Bike</a></li>
                </ul>
            </div>
            
            <div class="12u$">
                <ul class="actions vertical">
                    <li><a href="{{ route('logout') }}" class="button fit" onclick="event.preventDefault();document.getElementById('logout-form').submit();">登出系統Logout</a></li>
                </ul>
            </div>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </div>
    </td>
</section>