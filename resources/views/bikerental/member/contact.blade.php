<section>
    <a class="image fit"><img src="{{asset('images/bikerental/label.png')}}" alt="" /></a>
    <h3>服務專線</h3>
    <p> TEL:(06)253-8390<br> FAX:(06)254-1507<br> Email:services@mpf.com.tw<br> 統一編號:23536785<br> 地址:71042台南市永康區民東路5號</p>
    <footer>
        <ul class="actions">
            <li><a href="{{route('bikerental.contact')}}" class="button">聯絡我們Contact</a></li>
        </ul>
    </footer>
</section>