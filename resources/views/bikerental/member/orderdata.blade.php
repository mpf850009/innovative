@extends('bikerental.main')
@section('content')
<div id="main" class="wrapper style1">
    <div class="container">
        <header class="major">
            <h2>會員租車查詢系統<br/>Inquiry System</h2>
            <p>供會員查詢訂單狀態及修改訂單功能<br/>For members to check the status of orders and modify order functions.</p>
        </header>
        <div class="row 150%">
            <!-- Content -->
            <section id="content">
                <div class="table-wrapper">
                    <table>
                        <thead>
                            <tr>
                                <th>訂單編號<br/>No.</th>
                                <th>姓名<br/>Name</th>
                                <th>電話<br/>Phone</th>
                                <th>租車日期<br/>Date</th>
                                <th>租用車種<br/>Type</th>
                                <th>租用時間<br/>Time</th>
                                <th>地區<br/>Area</th>
                                <th>租用數量<br/>Quantity</th>
                                <th>付款方式<br/>Payment</th>
                                <th>總金額<br/>Total</th>
                                <th>訂單狀態<br/>Status</th>
                                <th>已付金額<br/>AlreadyPaid</th>
                                <th>功能<br/>Features</th>
                            </tr>
                        </thead>
                        @role('一般')
                            @foreach($booking as $b)
                                <tr>
                                    <td>{{$b->id}}</td>
                                    <td>{{Auth::user()->name}}</td>
                                    <td>{{Auth::user()->phone}}</td>
                                    <td>{{$b->datetime}}</td>
                                    @foreach($bikes as $bike)
                                        @if($b->type == $bike->id)
                                            <td>{{$bike->type}}</td>
                                        @endif
                                    @endforeach
                                    @foreach($payments as $p)
                                        @if($b->type == $p->type && $p->payment == $b->time)
                                            <td>{{$p->time}}</td>
                                        @endif
                                    @endforeach
                                    @foreach($areas as $a)
                                        @if($a->id == $b->area)
                                            <td>{{$a->area}}</td>
                                        @endif
                                    @endforeach
                                    <td>{{$b->quantity}}</td>
                                    @if($b->howtopay=='1')<td>現場付款</td>
                                    @else <td>自行匯款</td>
                                    @endif
                                    <td>{{$b->total}}元</td>
                                    @if($b->status=='1')<td>處理中</td>
                                    @else <td>已確認</td>
                                    @endif
                                    <td>{{$b->alreadypaid}}元</td>
                                    @if($b->status=='1')<td><a href="{{route('bikerental.orderdataedit',$b->id)}}">修改</a></td>
                                    @else <td>訂單確認</td>
                                    @endif
                                </tr>
                            @endforeach
                        @endrole
                        @role('管理')
                            @foreach($bookingall as $ba)
                                <tr>
                                    <td>{{$ba->id}}</td>
                                    @foreach($user as $u)
                                        @if($u->id==$ba->member_id)
                                            <td>{{$u->name}}</td>
                                            <td>{{$u->phone}}</td>
                                        @endif
                                    @endforeach
                                    <td>{{$ba->datetime}}</td>
                                    @foreach($bikes as $bike)
                                        @if($ba->type == $bike->id)
                                            <td>{{$bike->type}}</td>
                                        @endif
                                    @endforeach
                                    @foreach($payments as $p)
                                        @if($ba->type == $p->type && $p->payment == $ba->time)
                                            <td>{{$p->time}}</td>
                                        @endif
                                    @endforeach
                                    @foreach($areas as $a)
                                        @if($a->id == $ba->area)
                                            <td>{{$a->area}}</td>
                                        @endif
                                    @endforeach
                                    <td>{{$ba->quantity}}</td>
                                    @if($ba->howtopay=='1')<td>現場付款</td>
                                    @else <td>自行匯款</td>
                                    @endif
                                    <td>{{$ba->total}}元</td>
                                    @if($ba->status=='1')<td>處理中</td>
                                    @else <td>已確認</td>
                                    @endif
                                    <td>{{$ba->alreadypaid}}元</td>
                                    <td><a href="{{route('bikerental.orderdataedit',$ba->id)}}">編輯</a></td>
                                </tr>
                            @endforeach
                        @endrole
                    </table>
                </div>
                <tfoot>
                    @role('一般')
                        <a href="{{$booking->previousPageUrl()}}">上一頁</a>　
                        <a href="{{$booking->nextPageUrl()}}">下一頁</a><br><br>
                    @endrole
                    @role('管理')
                        <a href="{{$bookingall->previousPageUrl()}}">上一頁</a>　
                        <a href="{{$bookingall->nextPageUrl()}}">下一頁</a><br><br>
                    @endrole
                    <a href="{{route('bikerental.center')}}" class="button special">會員中心Member Center</a>
                </tfoot>
            </section>
        </div>
    </div>
</div>
@endsection