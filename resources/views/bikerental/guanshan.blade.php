@extends('bikerental.main')
@section('content')
<div id="main" class="wrapper style1">
    <div class="container">
        <header class="major">
            <h2>台東關山火車站</h2>
            <p>Guanshan Station</p>
        </header>

        <!-- Content -->
        <section id="content">
            <a  class="image fit"><img src="{{asset('images/bikerental/guanshan.jpg')}}" alt="關山TR9" /></a>
            <h3>{{trans('bikerental_location.gsh3')}}</h3>
            <h4>{{trans('bikerental_location.intro')}}</h4>
            <p>　　{{trans('bikerental_location.gsintro')}}</p>
            <h4>{{trans('bikerental_location.p')}}</h4>
            <p>{{trans('bikerental_location.gsp')}}</p>
            <h4>{{trans('bikerental_location.oh')}}</h4>
            <p>{{trans('bikerental_location.gsoh')}}</p>
            <h4>{{trans('bikerental_location.ad')}}</h4>
            <p>{{trans('bikerental_location.gsad')}}</p>
        </section>
        <section>
            <div>
                <h4>{{trans('bikerental_location.gm')}}</h4>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d917.8501494521257!2d121.16377862916667!3d23.0457705331335!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x346f09f13b6ace07%3A0xb7afc48902196549!2zVFI56Zec5bGx56ef6LuK56uZ44CQ5Y-w5p2x6Zec5bGx54Gr6LuK56uZ56uZ5YWn44CR!5e0!3m2!1szh-TW!2stw!4v1528860745362"
                width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </section>
    </div>
</div>
@endsection