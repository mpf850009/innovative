@extends('bikerental.main')
@section('content')
<div id="main" class="wrapper style1">
    <div class="container">
        <header class="major">
            <h2>台中東豐鐵馬道勝開心租車</h2>
            <p>Taichung Dongfong Bike Trail</p>
        </header>

        <!-- Content -->
        <section id="content">
            <a  class="image fit"><img src="{{asset('images/bikerental/taichung2.jpg')}}" alt="勝開心租車" /></a>
            <h3>{{trans('bikerental_index.df1')}}</h3>
            <h4>{{trans('bikerental_location.dfintro')}}</h4>
            <p>{!!trans('bikerental_index.dfdt')!!}</p>
            <h4>{{trans('bikerental_location.oh')}}</h4>
            <p>{!!trans('bikerental_location.dfoh')!!}</p>
            <h4>{{trans('bikerental_location.v')}}</h4>
            <p>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/jT9dToc6OGU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </p>
            <h4>臉書Facebook</h4>
            <p>
                <a target="_blank" href="https://goo.gl/unZH5o">{{trans('bikerental_index.df1')}}</a>
            </p>
            <h4>{{trans('bikerental_location.p')}}</h4>
            <p>{!!trans('bikerental_location.dfpdt')!!}</p>
            <h4>{{trans('bikerental_location.ad')}}</h4>
            <p>{!!trans('bikerental_location.dfad')!!}</p>
            <h4>{{trans('bikerental_location.ow')}}</h4>
            <p><a href="http://0425132875.tw.tranews.com/">{{trans('bikerental_index.df1')}}</a></p>
        </section>
        <section>
            <div>
                <h4>{{trans('bikerental_location.gm')}}</h4>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3432.97131787381!2d120.74827127031492!3d24.273012508782653!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x34691a571a7bdd57%3A0x63b7e4f23f6bd9e0!2z5Yud6ZaL5b-D!5e0!3m2!1szh-TW!2stw!4v1527553372296" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </section>
    </div>
</div>
@endsection