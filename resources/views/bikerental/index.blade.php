@extends('bikerental.main')
@section('content')
<style>

#banner div header h2 {
}
</style>
<section id="banner">
    <div class="content">
        <header>
            <h2>
                <b>MPF Drive</b>
            </h2>
            <p><br>More Power Feelings!</p>
        </header>
        <span class="image">
            <img src="{{asset('images/bikerental/mpflogo.jpg')}}" alt="" />
        </span>
    </div>
    <a href="#one" class="goto-next scrolly">Next</a>
</section>

<!-- One -->
<section id="one" class="spotlight style1 bottom">
    <span class="image fit main">
        <img src="{{asset('images/bikerental/index_taipei.jpg')}}" alt="" />
    </span>
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="4u 12u$(medium)">
                    <header>
                        <h2>{{trans('bikerental_index.fl')}}</h2>
                        <p>{!!trans('bikerental_index.fldc')!!}</p>
                    </header>
                    <ul class="actions">
                        <li>
                            <a target="_blank" href="http://www.ifulong.com.tw/" class="button">{{trans('bikerental_index.fl')}}</a>
                        </li>
                    </ul>
                </div>
                <div class="4u 12u$(medium)">{!!trans('bikerental_index.fldt1')!!}</div>
                <div class="4u$ 12u$(medium)">{!!trans('bikerental_index.fldt2')!!}</div>
            </div>
        </div>
    </div>
    <a href="#two" class="goto-next scrolly">Next</a>
</section>

<!-- Two -->
<section id="two" class="spotlight style2 right">
    <span class="image fit main">
        <img src="{{asset('images/bikerental/index_taichung1.jpg')}}" alt="" />
    </span>
    <div class="content">
        <header>
            <h2>{{trans('bikerental_index.df1')}}</h2>
        </header>
        <p>{!!trans('bikerental_index.dfdt')!!}</p>
        <ul class="actions">
            <li>
                <a target="_blank" href="http://0425132875.tw.tranews.com/" class="button">{{trans('bikerental_index.df1')}}</a>
            </li>
        </ul>
    </div>
    <a href="#three" class="goto-next scrolly">Next</a>
</section>

<!-- Three -->
<section id="three" class="spotlight style3 left">
    <span class="image fit main bottom">
        <img src="{{asset('images/bikerental/index_tainan2.jpg')}}" alt="" />
    </span>
    <div class="content">
        <header>
            <h2>{{trans('bikerental_index.ld')}}</h2>
        </header>
        <p>{!!trans('bikerental_index.lddt')!!}</p>
        <ul class="actions">
            <li>
                <a target="_blank" href="https://goo.gl/T38PWs" class="button">{{trans('bikerental_index.ld')}}</a>
            </li>
        </ul>
    </div>
    <a href="#four" class="goto-next scrolly">Next</a>
</section>

<!-- four -->
<section id="four" class="spotlight style1 bottom">
    <span class="image fit main">
        <img src="{{asset('images/bikerental/taitung3.jpg')}}" alt="" />
    </span>
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="4u 12u$(medium)">
                    <header>
                        <h2>{{trans('bikerental_index.wf')}}</h2>
                    </header>
                    <ul class="actions">
                        <li>
                            <a target="_blank" href="https://tour.taitung.gov.tw/zh-tw/attraction/details/265" class="button">{{trans('bikerental_index.ttgt')}}</a>
                        </li>
                    </ul>
                </div>
                <div class="4u 12u$(medium)">{!!trans('bikerental_index.wfdt1')!!}</div>
                <div class="4u$ 12u$(medium)">{!!trans('bikerental_index.wfdt2')!!}</div>
            </div>
        </div>
    </div>
    <a href="#five" class="goto-next scrolly">Next</a>
</section>

<!-- five -->
<section id="five" class="spotlight style2 right">
    <span class="image fit main">
        <img src="{{asset('images/bikerental/bg02.jpg')}}" alt="" />
    </span>
    <div class="content">
        <header>
            <h2>{{trans('bikerental_index.tc')}}</h2>
        </header>
        <p>{!!trans('bikerental_index.tcdt')!!}</p>
        <ul class="actions">
            <li>
                <a target="_blank" href="https://tour.taitung.gov.tw/zh-tw/shop/detail/406" class="button">{!!trans('bikerental_index.ttgt')!!}</a>
            </li>
        </ul>
    </div>
    <a href="#six" class="goto-next scrolly">Next</a>
</section>

<!-- six -->
<section id="six" class="spotlight style3 left">
    <span class="image fit main bottom">
        <img src="{{asset('images/bikerental/bg01.jpg')}}" alt="" />
    </span>
    <div class="content">
        <header>
            <h2>{{trans('bikerental_index.as')}}</h2>
        </header>
        <p>{!!trans('bikerental_index.asdt')!!}</p>
        <ul class="actions">
            <li>
                <a target="_blank" href="https://tour.taitung.gov.tw/zh-tw/shop/detail/3518" class="button">{{trans('bikerental_index.ttgt')}}</a>
            </li>
        </ul>
    </div>
    <a href="#service" class="goto-next scrolly">Next</a>
</section>

<!-- Service -->
<section id="service" class="wrapper style1 special fade-up">
    <div class="container">
        <header class="major">
            <h2>{{trans('bikerental_index.service')}}</h2>
            <p>{{trans('bikerental_index.df')}}</p>
        </header>
        <div class="box alt">
            <div class="row uniform">
                <section class="4u 6u(medium) 12u$(xsmall)">
                    <span class="icon alt major fa-area-chart"></span>
                    <h3>{{trans('bikerental_index.bo')}}</h3>
                    <p>{{trans('bikerental_index.bodt')}}</p>
                </section>
                <section class="4u 6u$(medium) 12u$(xsmall)">
                    <span class="icon alt major fa-comment"></span>
                    <h3>{{trans('bikerental_index.rm')}}</h3>
                    <p>{{trans('bikerental_index.rmdt')}}</p>

                </section>
                <section class="4u$ 6u(medium) 12u$(xsmall)">
                    <span class="icon alt major fa-flask">
                        <a href="#"></a>
                    </span>
                    <h3>{{trans('bikerental_index.rfrs')}}</h3>
                    <p>{{trans('bikerental_index.rfrsdt')}}</p>
                </section>
                <!--<section class="4u 6u$(medium) 12u$(xsmall)">
                            <span class="icon alt major fa-paper-plane"></span>
                            <h3>付款還車</h3>
                            <p>說點什麼...說點什麼...</p>
                        </section>
                        <section class="4u 6u(medium) 12u$(xsmall)">
                            <span class="icon alt major fa-file"></span>
                            <h3>費用說明</h3>
                            <p>說點什麼...說點什麼...</p>
                        </section>
                        <section class="4u$ 6u$(medium) 12u$(xsmall)">
                            <span class="icon alt major fa-lock"></span>
                            <h3>保險及相關規範</h3>
                            <p>說點什麼...說點什麼...</p>
                        </section>-->
            </div>
        </div>
        <footer class="major">
            <ul class="actions">
                <li>
                    <a href="" class="button">{{trans('bikerental_index.lm')}}</a>
                </li>
            </ul>
        </footer>
    </div>
</section>

<!-- Rent -->
<section id="rent" class="wrapper style2 special fade">
    <div>
        <header>
            <h2>{{trans('bikerental_index.bi')}}</h2>
            <p>
                <!--開始體驗Get Started 登入/加入會員Login/Join Member 我要租車Rent Bikes  填寫表單Fill The Form  確認訂單Check Orders-->
                {!!trans('bikerental_index.bidt')!!}
            </p>
        </header>
        <!--<form method="post" action="memberindex.php" >
            <div align = "center" >
                <div class="2u$ 12u$(xsmall)"><input type="submit" value="開始體驗Get Started" class="fit special" /></div>
            </div>
        </form>-->
    </div>
</section>
<section id="six" class="wrapper style1 special fade-up">
    <div class="container">
        <header class="major">
            <h2>{{trans('bikerental_index.sq')}}</h2>
        </header>
        <div class="box alt">
            <div class="row uniform">
                <section class="6u 6u(medium) 12u$(xsmall)">
                    <h3>Android APP</h3>
                    <a href="https://play.google.com/store/apps/details?id=tw.mpf.a860014.btlescan&hl=zh-TW">
                        <img src="{{asset('images/bikerental/androiddownload.jpg')}}" alt="" width="60%" />
                    </a>
                </section>
                <section class="6u 6u$(medium) 12u$(xsmall)">
                    <h3>iOS APP</h3>
                    <a href="https://itunes.apple.com/tw/app/mpf%E7%A7%9F%E8%BB%8A%E7%B3%BB%E7%B5%B1/id1316030260?mt=8">
                        <img src="{{asset('images/bikerental/iosdownload.jpg')}}" width="60%" alt="" />
                    </a>
                </section>
            </div>
        </div>
    </div>
</section>

<section>
    <div>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3671.1951500813566!2d120.25734731454644!3d23.053305984937477!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x346e70a9ce73a439%3A0x4c4704ff65de7975!2z56mp5q2jKOS8gSk!5e0!3m2!1szh-TW!2stw!4v1510646685104"
        width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
</section>
@endsection
