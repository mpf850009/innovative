@extends('bikerental.main')
@section('content')
<div id="main" class="wrapper style1">
    <div class="container">
        <header class="major">
            <h2>宜蘭冬山火車站</h2>
            <p>Ilan Dongshan Station</p>
        </header>

        <!-- Content -->
        <section id="content">
            <a  class="image fit"><img src="{{asset('images/bikerental/dongshan.jpg')}}" alt="關山TR9" /></a>
            <h3>{{trans('bikerental_location.dsh4')}}</h3>
            <h4>{{trans('bikerental_location.intro')}}</h4>
            <p>　　{!!trans('bikerental_location.dsintro')!!}</p>
        </section>
        <section>
            <div>
                <h4>{{trans('bikerental_location.gm')}}</h4>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3625.7402167615733!2d121.80763601418413!3d24.667068784145506!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3467e601fb31883b%3A0x62dea980e39701b5!2z5Yas5bGx5rKz6Ieq6KGM6LuK6YGT!5e0!3m2!1szh-TW!2stw!4v1528878723858"
                width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </section>
    </div>
</div>
@endsection