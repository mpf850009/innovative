

<header id="header">
    <h1 id="logo">
        <a href="index.html"></a>
    </h1>
    <nav id="nav">
        <ul>
            @if($locale=="zh-TW")
                <li>
                    <a href="{{route("trans")}}/en">
                        <b>English</b>
                    </a>
                </li>
            @else
                <li>
                    <a href="{{route("trans")}}/zh-TW">
                        <b>繁體中文</b>
                    </a>
                </li>
            @endif
            <li>
                <a href="{{route('bikerental.index',$locale)}}">
                    <b>{{ trans('bikerental_nav.home') }}</b>
                </a>
            </li>
            <li>
                <a href="{{route('bikerental.company',$locale)}}">
                    <b>{{ trans('bikerental_nav.company') }}</b>
                </a>
            </li>
            <li>
                <a>
                    <b>{{ trans('bikerental_nav.service') }}</b>
                </a>
                <ul>
                    <li>
                        <a>{{ trans('bikerental_nav.taipei') }}</a>
                        <ul>
                            <li>
                                <a href="{{route('bikerental.fulong',$locale)}}">{{ trans('bikerental_nav.fl') }}</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a>{{ trans('bikerental_nav.yilan') }}</a>
                        <ul>
                            <li>
                                <a href="{{route('bikerental.dongshan',$locale)}}">{{ trans('bikerental_nav.ds') }}</a>
                            </li>
                        </ul>
                    <li>
                        <a>{{ trans('bikerental_nav.taichung') }}</a>
                        <ul>
                            <li>
                                <a href="{{route('bikerental.winhappy',$locale)}}">{{ trans('bikerental_nav.wh') }}</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a>{{ trans('bikerental_nav.tainan') }}</a>
                        <ul>
                            <li>
                                <a href="{{route('bikerental.dragon',$locale)}}">{{ trans('bikerental_nav.ld') }}</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a>{{ trans('bikerental_nav.hualien') }}</a>
                        <ul>
                            <li>
                                <a href="{{route('bikerental.asheng_h',$locale)}}">{{ trans('bikerental_nav.as') }}</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a>{{ trans('bikerental_nav.taitung') }}</a>
                        <ul>
                            <li>
                                <a href="{{route('bikerental.park_water',$locale)}}">{{ trans('bikerental_nav.wf') }}</a>
                            </li>
                            <li>
                                <a href="{{route('bikerental.guanshan',$locale)}}">{{ trans('bikerental_nav.gs') }}</a>
                            </li>
                            <li>
                                <a href="{{route('bikerental.chishang',$locale)}}">{{ trans('bikerental_nav.cs') }}</a>
                            </li>
                            <li>
                                <a href="{{route('bikerental.asheng_t',$locale)}}">{{ trans('bikerental_nav.as') }}</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{route('bikerental.onsale',$locale)}}">
                    <b>{{ trans('bikerental_nav.sale') }}</b>
                </a>
            </li>
            <li>
                <a href="{{route('bikerental.ebike',$locale)}}">
                    <b>{{ trans('bikerental_nav.rent') }}</b>
                </a>
            </li>
            <li>
                <a href="{{route('bikerental.contact',$locale)}}">
                    <b>{{ trans('bikerental_nav.contact') }}</b>
                </a>
            </li>
            
            @if($active!='register'&&$active!='center'&&$active!='login'&&$active!='center')
                <li>
                    <a href="{{route('bikerental.index',$locale)}}#rent" class="button special">{{ trans('bikerental_nav.startrent') }}</a>
                </li>
            @else
                <li>
                    <a href="{{route('bikerental.center',$locale)}}" class="button special">{{ trans('bikerental_nav.rentbike') }}</a>
                </li>
            @endif
            
        </ul>
    </nav>
</header>