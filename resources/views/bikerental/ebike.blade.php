@extends('bikerental.main')
@section('content')
<style>
    table thead tr th{
        font-size: 25px;
        padding: 15px 15px;
    }
    table tbody tr td{
        padding: 8px 15px;
    }
</style>
<div id="main" class="wrapper style1">
    <div class="container">
        <header class="major">
            <h2>{{trans('bikerental_bike.eab')}}</h2>
            <p>{{trans('bikerental_bike.eabd')}}</p>
        </header>

    <!-- E-Bike -->
        <section>
            <h3>{{trans('bikerental_bike.eb20')}}</h3>
            <div class="row">
                <div class="6u 12u(xsmall)">
                    <h4>{{trans('bikerental_bike.pic')}}</h4>
                    <a class="image fit"><img src="{{asset('images/bikerental/ebike4.jpg')}}" alt="" /></a>
                </div>
                <div class="6u 12u(xsmall)">
                    <h4>{{trans('bikerental_bike.intro')}}</h4>
                    <ul>{!!trans('bikerental_bike.eb20intro')!!}</ul>
                </div>
            </div>
        </section>
        <section>
            <h3>{{trans('bikerental_bike.eb26')}}</h3>
            <div class="row">
                <div class="6u 12u(xsmall)">
                    <h4>{{trans('bikerental_bike.pic')}}</h4>
                    <a class="image fit"><img src="{{asset('images/bikerental/ebike5.jpg')}}" alt="" /></a>
                </div>
                <div class="6u 12u(xsmall)">
                    <h4>{{trans('bikerental_bike.intro')}}</h4>
                    <ul>{!!trans('bikerental_bike.eb26intro')!!}</ul>
                </div>
            </div>
        </section>

        <!--<section>
            <h3>MPF203</h3>
            <div class="row">
                <div class="6u 12u(xsmall)">
                    <h4>車體圖片</h4>
                    <a href="#" class="image fit"><img src="{{asset('images/bikerental/ebike7.jpg')}}" alt="" /></a>
                </div>
                <div class="6u 12u(xsmall)">
                    <h4>詳細介紹</h4>
                    <ul>
                        <li>36V無刷直流高效率馬達</li>
                        <li>最大400W輸出</li>
                        <li>最高速度為25km/hr</li>
                        <li>行駛里程約30~80km</li>
                        <li>20吋低跨度車架</li>
                        <li>6段外變速</li>
                        <li>前輪雙叉避震</li>
                        <li>租車即附前燈和大鎖</li>
                        <li>APP藍芽控制</li>
                        <li>APP緊急救援功能</li>
                    </ul>
                </div>
            </div>
        </section>-->

        <section>
            <h3>{{trans('bikerental_bike.type3')}}</h3>
            <div class="box alt">
                <div class="row 50% uniform">
                    <div class="6u  12u$(xsmall)">
                        <span class="image fit">
                            <img src="{{asset('images/bikerental/ebike3.jpg')}}" alt="" />
                        </span>
                        <ol>{!!trans('bikerental_bike.rules')!!}</ol>
                    </div>
                    
                    <div class="6u$  12u(xsmall)">
                        <table class="table table-striped ">
                            <thead>
                                <tr>
                                    <th>{{trans('bikerental_bike.ac')}}</th>
                                    <th>{{trans('bikerental_bike.spec')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{trans('bikerental_bike.frame')}}</td>
                                    <td>{{trans('bikerental_bike.al')}}#6061</td>
                                </tr>
                                <tr>
                                    <td>{{trans('bikerental_bike.ff')}}</td>
                                    <td>Renegade Air TRL 行程100mm 附線控</td>
                                </tr>
                                <tr>
                                    <td>{{trans('bikerental_bike.cb')}}</td>
                                    <td>SHIMANO 拉柄BLM365剎車器BRM36</td>
                                </tr>
                                <tr>
                                    <td>{{trans('bikerental_bike.groupest')}}</td>
                                    <td>{{trans('bikerental_bike.groupest1')}}</td>
                                </tr>
                                <tr>
                                    <td>{{trans('bikerental_bike.crankset')}}</td>
                                    <td>1/2*11/128&quot;*36T</td>
                                </tr>
                                <tr>
                                    <td>{{trans('bikerental_bike.ch')}}</td>
                                    <td>KMC X9e</td>
                                </tr>
                                <tr>
                                    <td>{{trans('bikerental_bike.sp')}}</td>
                                    <td>ETENi L400mm27.2 線控式可調座管</td>
                                </tr>
                                <tr>
                                    <td>{{trans('bikerental_bike.cassette')}}</td>
                                    <td>11/34T</td>
                                </tr>
                                <tr>
                                    <td>{{trans('bikerental_bike.handlebar')}}</td>
                                    <td>{{trans('bikerental_bike.al')}} 31.8 700W ISO4210-M</td>
                                </tr>
                                <tr>
                                    <td>{{trans('bikerental_bike.seatpost')}}</td>
                                    <td>{{trans('bikerental_bike.al')}}31.8 ISO4210-M</td>
                                </tr>
                                <tr>
                                    <td>{{trans('bikerental_bike.tires')}}</td>
                                    <td>26*4&quot;</td>
                                </tr>
                                <tr>
                                    <td>{{trans('bikerental_bike.wheelset')}}</td>
                                    <td>26*32H {{trans('bikerental_bike.sc')}} {{trans('bikerental_bike.slh')}} BK</td>
                                </tr>
                                <tr>
                                    <td>{{trans('bikerental_bike.spoke')}}</td>
                                    <td>{{trans('bikerental_bike.ss')}} BK</td>
                                </tr>
                                <tr>
                                    <td>{{trans('bikerental_bike.pedal')}}</td>
                                    <td>∮916 CRMO {{trans('bikerental_bike.al')}} BK ISO4210</td>
                                </tr>
                                <tr>
                                    <td>{{trans('bikerental_bike.sk')}}</td>
                                    <td>側立 可調式 鋁 B</td>
                                </tr>
                                <tr>
                                    <td>{{trans('bikerental_bike.hl')}}</td>
                                    <td>SSL127WH</td>
                                </tr>
                                <tr>
                                    <td>{{trans('bikerental_bike.lwh')}}</td>
                                    <td>1850*650*1100 MM</td>
                                </tr>
                                <tr>
                                    <td>{{trans('bikerental_bike.weight')}}</td>
                                    <td>23.3KG</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clearfix "> </div>
        </section>

        <section>
            <h3>平把電動輔助自行車</h3>
            <div class="box alt">
                <div class="row 50% uniform">
                    <div class="6u  12u$(xsmall)">
                        <span class="image fit">
                            <img src="{{asset('images/bikerental/ebike1.jpg')}}" alt="" />
                        </span>
                        <ol>
                            <li>符合台灣法規</li>
                            <li>提供動力輔助最高25公里後停止補助動力</li>
                            <li>最大輸出功率400W以下</li>
                            <li>充電後最大輔助可行走約50公里</li>
                            <li>最低補助可行走約140公里</li>
                            <li>超速斷電：行駛速率超過25KM/H時電動輔助自行車之電動機電源應能於三秒內自動暫停供電</li>
                        </ol>
                    </div>
                    <div class="6u$  12u(xsmall)">
                        <table class="table table-striped ">
                            <thead>
                                <tr>
                                    <th>配件</th>
                                    <th>規格</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>車架Frame</td>
                                    <td>鋁合金#6061</td>
                                </tr>
                                <tr>
                                    <td>前叉Front Fork</td>
                                    <td>Renegade Air TRL 行程100mm 附線控</td>
                                </tr>
                                <tr>
                                    <td>煞車組</td>
                                    <td>SHIMANO 拉柄BLM365剎車器BRM36</td>
                                </tr>
                                <tr>
                                    <td>變速Groupset</td>
                                    <td>11速SHIMANO 105</td>
                                </tr>
                                <tr>
                                    <td>齒盤Crankset</td>
                                    <td>1/2*11/128吋*34T</td>
                                </tr>
                                <tr>
                                    <td>鍊條Chain</td>
                                    <td>KMC X11e</td>
                                </tr>
                                <tr>
                                    <td>坐墊柱</td>
                                    <td>ETENi L400mm27.2 線控式可調座管</td>
                                </tr>
                                <tr>
                                    <td>飛輪Cassette</td>
                                    <td>11/34T</td>
                                </tr>
                                <tr>
                                    <td>車手Handlebar</td>
                                    <td>鋁合金 31.8 620W,640W,660W ISO4210-M</td>
                                </tr>
                                <tr>
                                    <td>座管Seatpost</td>
                                    <td>鋁合金31.8 EX105 L41 ISO4210-M</td>
                                </tr>
                                <tr>
                                    <td>輪胎Tires</td>
                                    <td>700C*35C</td>
                                </tr>
                                <tr>
                                    <td>輪圈Wheelset</td>
                                    <td>700C*32H 雙層圈 方形減重孔 BK</td>
                                </tr>
                                <tr>
                                    <td>鋼絲</td>
                                    <td>白鐵 BK</td>
                                </tr>
                                <tr>
                                    <td>腳踏</td>
                                    <td>∮916 CRMO軸心 鋁 BK ISO4210</td>
                                </tr>
                                <tr>
                                    <td>停車架</td>
                                    <td>側立 可調式 鋁 B</td>
                                </tr>
                                <tr>
                                    <td>前燈</td>
                                    <td>SSL127WH</td>
                                </tr>
                                <tr>
                                    <td>長寬高</td>
                                    <td>1760*670*1050 MM</td>
                                </tr>
                                <tr>
                                    <td>重量Weight</td>
                                    <td>21.5KG</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clearfix "> </div>
        </section>

        <section>
            <h3>彎把電動輔助自行車</h3>
            <div class="box alt">
                <div class="row 50% uniform">
                    <div class="6u  12u$(xsmall)">
                        <span class="image fit">
                            <img src="{{asset('images/bikerental/ebike2.jpg')}}" alt="" />
                        </span>
                        <ol>
                            <li>符合台灣法規</li>
                            <li>提供動力輔助最高25公里後停止補助動力</li>
                            <li>最大輸出功率400W以下</li>
                            <li>充電後最大輔助可行走約50公里</li>
                            <li>最低補助可行走約140公里</li>
                            <li>超速斷電：行駛速率超過25KM/H時電動輔助自行車之電動機電源應能於三秒內自動暫停供電</li>
                        </ol>
                    </div>
                    <div class="6u$  12u(xsmall)">
                        <table class="table table-striped ">
                            <thead>
                                <tr>
                                    <th>配件</th>
                                    <th>規格</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>車架Frame</td>
                                    <td>鋁合金#6061</td>
                                </tr>
                                <tr>
                                    <td>前叉Front Fork</td>
                                    <td>CARBON鋁豎</td>
                                </tr>
                                <tr>
                                    <td>煞車組</td>
                                    <td>BB5</td>
                                </tr>
                                <tr>
                                    <td>變速Groupset</td>
                                    <td>11速SHIMANO 105</td>
                                </tr>
                                <tr>
                                    <td>齒盤Crankset</td>
                                    <td>1/2*11/128吋*44/30T</td>
                                </tr>
                                <tr>
                                    <td>鍊條Chain</td>
                                    <td>KMC X11e</td>
                                </tr>
                                <tr>
                                    <td>坐墊柱</td>
                                    <td>SPSL282 L350mm27.2 鋁</td>
                                </tr>
                                <tr>
                                    <td>飛輪Cassette</td>
                                    <td>11/32T</td>
                                </tr>
                                <tr>
                                    <td>車手Handlebar</td>
                                    <td>鋁合金 31.8 420W , 440W ISO421</td>
                                </tr>
                                <tr>
                                    <td>座管Seatpost</td>
                                    <td>鋁合金 31.8 EX807度 , 90 7度 L38 ISO4210M</td>
                                </tr>
                                <tr>
                                    <td>輪胎Tires</td>
                                    <td>700C*32C</td>
                                </tr>
                                <tr>
                                    <td>輪圈Wheelset</td>
                                    <td>700C*28H 雙層圈 BK</td>
                                </tr>
                                <tr>
                                    <td>鋼絲</td>
                                    <td>白鐵 BK</td>
                                </tr>
                                <tr>
                                    <td>腳踏</td>
                                    <td>∮916 CRMO軸心 鋁 BK ISO4210</td>
                                </tr>
                                <tr>
                                    <td>停車架</td>
                                    <td>側立 可調式 鋁 B</td>
                                </tr>
                                <tr>
                                    <td>前燈</td>
                                    <td>SSL127WH</td>
                                </tr>
                                <tr>
                                    <td>長寬高</td>
                                    <td>1750*480*960 MM</td>
                                </tr>
                                <tr>
                                    <td>重量Weight</td>
                                    <td>19.5KG</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clearfix "> </div>
        </section>

        <section>
            <h3>K&K胖胎車</h3>
            <div class="box alt">
                <div class="row 50% uniform">
                    <div class="6u  12u$(xsmall)">
                        <span class="image fit">
                            <img src="{{asset('images/bikerental/ebike6.jpg')}}" alt="" />
                        </span>
                        <ol>
                            <li>符合台灣法規</li>
                            <li>提供動力輔助最高25公里後停止補助動力</li>
                            <li>最大輸出功率400W以下</li>
                            <li>充電後最大輔助可行走約50公里</li>
                            <li>最低補助可行走約140公里</li>
                            <li>超速斷電：行駛速率超過25KM/H時電動輔助自行車之電動機電源應能於三秒內自動暫停供電</li>
                        </ol>
                    </div>
                    <div class="6u$  12u(xsmall)">
                        <table class="table table-striped ">
                            <thead>
                                <tr>
                                    <th>配件</th>
                                    <th>規格</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>車架Frame</td>
                                    <td>鋁合金#6061</td>
                                </tr>
                                <tr>
                                    <td>前叉Front Fork</td>
                                    <td>Renegade Air TRL 行程100mm 附線控</td>
                                </tr>
                                <tr>
                                    <td>煞車組</td>
                                    <td>SHIMANO 拉柄BLM365剎車器BRM36</td>
                                </tr>
                                <tr>
                                    <td>變速Groupset</td>
                                    <td>11速SHIMANO 105</td>
                                </tr>
                                <tr>
                                    <td>齒盤Crankset</td>
                                    <td>1/2*11/128吋*36T</td>
                                </tr>
                                <tr>
                                    <td>鍊條Chain</td>
                                    <td>KMC X9e</td>
                                </tr>
                                <tr>
                                    <td>坐墊柱</td>
                                    <td>ETENi L400mm27.2 線控式可調座管</td>
                                </tr>
                                <tr>
                                    <td>飛輪Cassette</td>
                                    <td>11/34T</td>
                                </tr>
                                <tr>
                                    <td>車手Handlebar</td>
                                    <td>鋁合金 31.8 700W ISO4210-M</td>
                                </tr>
                                <tr>
                                    <td>座管Seatpost</td>
                                    <td>鋁合金31.8 ISO4210-M</td>
                                </tr>
                                <tr>
                                    <td>輪胎Tires</td>
                                    <td>26*4吋</td>
                                </tr>
                                <tr>
                                    <td>輪圈Wheelset</td>
                                    <td>26*32H 單層圈 方形減重孔 BK</td>
                                </tr>
                                <tr>
                                    <td>鋼絲</td>
                                    <td>白鐵 BK</td>
                                </tr>
                                <tr>
                                    <td>腳踏</td>
                                    <td>∮916 CRMO軸心 鋁 BK ISO4210</td>
                                </tr>
                                <tr>
                                    <td>停車架</td>
                                    <td>側立 可調式 鋁 B</td>
                                </tr>
                                <tr>
                                    <td>前燈</td>
                                    <td>SSL127WH</td>
                                </tr>
                                <tr>
                                    <td>長寬高</td>
                                    <td>1850*650*1100 MM</td>
                                </tr>
                                <tr>
                                    <td>重量Weight</td>
                                    <td>23.3KG</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clearfix "> </div>
        </section>
    </div>
</div>
@endsection