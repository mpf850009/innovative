@extends('innovative.main')
@section('content')
@include('innovative.script.veerscript')
<link rel="stylesheet" href="{{asset('css/innovative/flexslider.css')}}" type="text/css">
<style>
    .gallery-w3lsrow ol{
        list-style-image:url('images/innovative/veerlogo3.png');
        line-height: 30px;
        font-size: 20px;
    }
</style>
<div id="production" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>Veer</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-12 hideme">
                <iframe width="100%" height="500" src="https://www.youtube.com/embed/JrIVwVhh7Nw?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="clearfix "> </div>
        </div>
    </div>
</div>
<div id="v" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>{{trans('in_veer.p')}}</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-6 col-xs-12 gallery-grids">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 hideme flexslider">
                    <ul class="slides">
                        <li data-thumb="{{asset('images/innovative/veercruiser1.jpg')}}">
                            <a href="{{asset('images/innovative/veercruiser1b.jpg')}}" data-lightbox="example-set " data-title="Veer">
                                <img src="{{asset('images/innovative/veercruiser1a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 Veer">
                            </a>
                        </li>
                        <li data-thumb="{{asset('images/innovative/veercruiser2.jpg')}}">
                            <a href="{{asset('images/innovative/veercruiser2b.jpg')}}" data-lightbox="example-set " data-title="Veer">
                                <img src="{{asset('images/innovative/veercruiser2a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 Veer">
                            </a>
                        </li>
                        <li data-thumb="{{asset('images/innovative/veercruiser3.jpg')}}">
                            <a href="{{asset('images/innovative/veercruiser3b.jpg')}}" data-lightbox="example-set " data-title="Veer">
                                <img src="{{asset('images/innovative/veercruiser3a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 Veer">
                            </a>
                        </li>
                        <li data-thumb="{{asset('images/innovative/veercruiser4.jpg')}}">
                            <a href="{{asset('images/innovative/veercruiser4b.jpg')}}" data-lightbox="example-set " data-title="Veer">
                                <img src="{{asset('images/innovative/veercruiser4a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 Veer">
                            </a>
                        </li>
                        <li data-thumb="{{asset('images/innovative/veercruiser5.jpg')}}">
                            <a href="{{asset('images/innovative/veercruiser5b.jpg')}}" data-lightbox="example-set " data-title="Veer">
                                <img src="{{asset('images/innovative/veercruiser5a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 Veer">
                            </a>
                        </li>
                    </ul>
                        
                    <!--
                    <div class="w3ls-hover">
                        <a id="chghref" href="{{asset('images/innovative/veergif1.gif')}} " data-lightbox="example-set " data-title="Veer">
                            <img id="chgicon" src="{{asset('images/innovative/veergif1.gif')}} " class="img-responsive zoom-img " alt="veer 創星 innovative 樂享學 Veer"/>
                        </a>
                    </div>-->
                </div>
                <div class="col-sm-2"></div>
            </div>
            <div class=" col-sm-6 col-xs-12 gallery-grids hideme">
                <div style="padding:0cm 1cm;">
                    <h3>{{trans('in_veer.intro')}}</h3><br>
                    <p>{!!trans('in_veer.p1')!!}</p>
                    <br>
                </div>
            </div>
            <div class="clearfix "> </div>
        </div>
    </div>
</div>
<div id="accessories" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>Cruiser {{trans('in_nk.ac')}}</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-3 col-xs-4 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.veeracc',[$locale,$type="1"])}}">
                        <img src="{{asset('images/innovative/goveer1.jpg')}} " class="img-responsive zoom-img " alt="veer 創星 innovative 樂享學 foldable storage basket" style="padding:30px 30px;"/>
                        <div class="view-caption" style="padding:5em 20px;">
                            <h5>{{trans('in_veer.acc1')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.veeracc',[$locale,$type="2"])}}">
                        <img src="{{asset('images/innovative/goveer2.jpg')}}" class="img-responsive zoom-img" alt="veer 創星 innovative 樂享學 retractable canopy" style="padding:30px 30px;"/>
                        <div class="view-caption" style="padding:5em 20px;">
                            <h5>{{trans('in_veer.acc2')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.veeracc',[$locale,$type="3"])}}">
                        <img src="{{asset('images/innovative/goveer3.jpg')}} " class="img-responsive zoom-img " alt="veer 創星 innovative 樂享學infant car seat adapter" style="padding:30px 30px;"/>
                        <div class="view-caption" style="padding:5em 20px;">
                            <h5>{{trans('in_veer.acc3')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.veeracc',[$locale,$type="4"])}}">
                        <img src="{{asset('images/innovative/goveer4.jpg')}} " class="img-responsive zoom-img " alt="veer 創星 innovative 樂享學 travel bag" style="padding:30px 30px;"/>
                        <div class="view-caption" style="padding:5em 20px;">
                            <h5>{{trans('in_veer.acc4')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.veeracc',[$locale,$type="5"])}}">
                        <img src="{{asset('images/innovative/goveer5.jpg')}} " class="img-responsive zoom-img " alt="veer 創星 innovative 樂享學 cup holders" style="padding:30px 30px;"/>
                        <div class="view-caption" style="padding:5em 20px;">
                            <h5>{{trans('in_veer.acc5')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.veeracc',[$locale,$type="7"])}}">
                        <img src="{{asset('images/innovative/goveer7.jpg')}} " class="img-responsive zoom-img " alt="veer 創星 innovative 樂享學comfort seat" style="padding:30px 30px;"/>
                        <div class="view-caption" style="padding:5em 20px;">
                            <h5>{{trans('in_veer.acc7')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.veeracc',[$locale,$type="8"])}}">
                        <img src="{{asset('images/innovative/goveer8.jpg')}} " class="img-responsive zoom-img " alt="veer 創星 innovative 樂享學 drink Snack" style="padding:30px 30px;"/>
                        <div class="view-caption" style="padding:5em 20px;">
                            <h5>{{trans('in_veer.acc8')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <!--Edited on 2020/04/15-->
            <div class="col-sm-3 col-xs-4 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.veeracc',[$locale,$type="9"])}}">
                        <img src="{{asset('images/innovative/goveer11.jpg')}} " class="img-responsive zoom-img " alt="veer 創星 innovative 樂享學 drink Snack" style="padding:30px 30px;"/>
                        <div class="view-caption" style="padding:5em 20px;">
                            <h5>{{trans('in_veer.acc9')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>  
            <div class="col-sm-3 col-xs-4 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.veeracc',[$locale,$type="10"])}}">
                        <img src="{{asset('images/innovative/goveer12.jpg')}} " class="img-responsive zoom-img " alt="veer 創星 innovative 樂享學 drink Snack" style="padding:30px 30px;"/>
                        <div class="view-caption" style="padding:5em 20px;">
                            <h5>{{trans('in_veer.acc10')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="clearfix "> </div>
        </div>
    </div>
</div>
<script src="{{asset('js/innovative/lightbox-plus-jquery.min.js')}}"></script>

@endsection