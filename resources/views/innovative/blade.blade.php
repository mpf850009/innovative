@extends('innovative.main')
@section('content')
@include('innovative.script.nikimotionscript')
<link href="https://fonts.googleapis.com/css?family=Montserrat:300" rel="stylesheet">
<style>
.font p{
    font-family: 'Montserrat', sans-serif,Microsoft JhengHei;
    font-size: 18px;
}
.font h3{
    font-family: 'Montserrat', sans-serif,Microsoft JhengHei;
    font-size: 35px;
}
.font li{
    font-family: 'Montserrat', sans-serif,Microsoft JhengHei;
    font-size: 18px;
} 
.font strong{
    font-family: 'Montserrat', sans-serif,Microsoft JhengHei;
    font-size: 22px;
} 
.container ul{
    list-style-image:url('images/innovative/nklogo3.png');
}
</style>

<div id="production" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>blade</h3>
        </div>
        <div class="gallery-w3lsrow"><!--
            <div class="col-sm-4 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{asset('images/innovative/nkgif3.gif')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/nkgif3.gif')}} " class="img-responsive zoom-img " alt="veer 創星 innovative 樂享學" />
                        <div class="view-caption" style="padding:9em 20px;">
                            <h5>Veer
                                <br>
                                <small>查看更多</small>
                            </h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>-->
            <div class="col-sm-12 hideme">
                <iframe width="100%" height="500" src="https://www.youtube.com/embed/9WkIdRQx_0w?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="clearfix "> </div>
        </div>
    </div>
</div>

<div id="blade" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>Blade {{trans('in_nk.fc')}}</h3>
        </div>
        <div class="col-sm-6 col-xs-12">
            <div class="col-sm-2 col-xs-2 hideme">
                <a class="bladec1" title="color-Capri">
                    <img src="{{asset('images/innovative/color-Capri.jpg')}}" class="img-circle" width="40px" height="40px">
                </a>
            </div>
            <div class="col-sm-2 col-xs-2 hideme">
                <a class="bladec2" title="color-Berry">
                    <img src="{{asset('images/innovative/color-Berry.jpg')}}" class="img-circle" width="40px" height="40px">
                </a>
            </div>
            <div class="col-sm-2 col-xs-2 hideme">
                <a class="bladec3" title="color-Ebony">
                    <img src="{{asset('images/innovative/color-Ebony.png')}}" class="img-circle" width="40px" height="40px">
                </a>
            </div>
            <div class="col-sm-2 col-xs-2 hideme">
                <a class="bladec4" title="color-Emerald">
                    <img src="{{asset('images/innovative/color-Emerald.jpg')}}" class="img-circle" width="40px" height="40px">
                </a>
            </div>
            <div class="col-sm-2 col-xs-2 hideme">
                <a class="bladec5" title="color-Mink">
                    <img src="{{asset('images/innovative/color-sand.jpg')}}" class="img-circle" width="40px" height="40px">
                </a>
            </div>
            <div class="col-sm-2 col-xs-2 hideme">
                <a class="bladec6" title="color-Oyster">
                    <img src="{{asset('images/innovative/color-Oyster.jpg')}}" class="img-circle" width="40px" height="40px">
                </a>
            </div>
            <div class="clearfix"></div><br>
            <div class="col-sm-2 col-xs-2 hideme">
                <a class="bladec7" title="color-Scarlett">
                    <img src="{{asset('images/innovative/color-Scarlett.png')}}" class="img-circle" width="40px" height="40px">
                </a>
            </div>
            <div class="col-sm-2 col-xs-2 hideme">
                <a class="bladec8" title="color-Tangerine">
                    <img src="{{asset('images/innovative/color-Tangerine.jpg')}}" class="img-circle" width="40px" height="40px">
                </a>
            </div>
            <div class="clearfix"></div><br><br>
            <div class="col-sm-1"></div>
            <div class="col-sm-10 hideme">
                <div class="w3ls-hover ">
                    <a href="{{asset('images/innovative/nkgif3.gif')}}" id="bladehref" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/nkgif3.gif')}}" class="img-responsive zoom-img" id="bladeicon" alt="創星 樂享學 innovative blade Nikimotion" style="padding:2em 2em;"/>
                        <div class="view-caption" style="padding:10em 20px;">
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1"></div>
            <div class="clearfix"></div><br><br>
        </div>
        <div class="col-sm-6 col-xs-12 font" style="padding:0px 50px;">
            <h3 class="hideme">BLADE</h3><br>
            {!!trans('in_nk.dc3')!!}
            <!--
            <strong class="hideme">Description 詳細介紹</strong><br>
            <ul class="hideme">
                <li class="hideme">Reversible seat / 雙向使用座椅</li>
                <li class="hideme">Stroller can be folded with seat in both directions / 雙向座椅皆可直接收展車</li>
                <li class="hideme">Stands when folded / 收車後可自行站立</li>
                <li class="hideme">One-handed back rest adjustment / 可調節式背靠</li>
                <li class="hideme">Front bar detachable and can be opened from both sides / 可拆卸式手把</li>
                <li class="hideme">Viewing window on the roof-back / 天窗式遮陽篷</li>
                <li class="hideme">5-point belt system / 五點式安全扣具</li>
                <li class="hideme">Pram optional available / 搭配配件睡箱</li>
            </ul><br>
            <strong class="hideme">Product spec 規格</strong><br>
            <ul class="hideme">
                <li class="hideme">Weight 重量	14.5 kg</li>
                <li class="hideme">Dimention 尺寸	90 x 54 x 29 cm</li>
            </ul><br>
            <strong class="hideme">Accessory 配件</strong><br>
            <ul class="hideme">
                <li class="hideme">MAXI-COSI Car Seat Adapter / MAXI-COSI氣座轉接座</li>
                <li class="hideme">Cup Holder / 水杯架</li>
                <li class="hideme">Bug Net / 蚊帳</li>
                <li class="hideme">Rain Cover / 雨罩</li>
            </ul><br>-->
            <!--
            <br>
            <h2 class="hideme" style="float:left">
                <a href="#production">
                    <span class="label label-primary">back</span>
                </a>　
            </h2>-->
        </div>
    </div>
</div>
<div id="blac" class="gallery">
    <div class="container">
        <div class="w3-headings-all">
            <h3>Blade {{trans('in_nk.ac')}}</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-3"></div>
            <div class="col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.nkac',$locale)}}#nkac1">
                        <img src="{{asset('images/innovative/nkac1.jpg')}}" class="img-responsive zoom-img" alt="創星 樂享學 innovative Accessories Nikimotion" />
                        <div class="view-caption" style="font-size:12px; padding:7em 20px;">
                            <h5>{{trans('in_nk.acc1t')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.nkac',$locale)}}#blac1">
                        <img src="{{asset('images/innovative/blac1.jpg')}}" class="img-responsive zoom-img" alt="創星 樂享學 innovative Accessories Nikimotion blade" />
                        <div class="view-caption" style="font-size:12px; padding:7em 20px;">
                            <h5>{!!trans('in_nk.acc13t')!!}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3"></div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<div class="gallery">
    <div class="container">
        <h3>
            <a href="{{route('innovative.nikimotion',$locale)}}#production">
                <span class="label label-primary">&nbsp;{{trans('in_nk.back')}}&nbsp;</span>
            </a>
        </h3>
    </div>
</div>
<script src="{{asset('js/innovative/lightbox-plus-jquery.min.js')}}"></script>
<script src="{{asset('js//innovative/bars.js')}}"></script>
@endsection

