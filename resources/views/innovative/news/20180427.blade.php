@extends('innovative.main')
@section('content')
<style>
	.img{
		padding: 15px 0px 15px 0px;
	}
</style>
<div class="inner_main_agile_section">
	<div class="container">
		<div class="w3-headings-all">
			<h3>{{trans('in_index.open6')}}</h3>
		</div>
		<div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
				<h3>20180427 {{trans('in_index.open6')}}</h3>
				<p></p><br>
				<div class="gallery-w3lsrow">
					<div class="gallery-grids">
						<div class="w3ls-hover">
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_fl1.jpg')}}" alt="創星 樂享學 innovative 台北 福隆驛站 fulong Taipei 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_fl2.jpg')}}" alt="創星 樂享學 innovative 台北 福隆驛站 fulong Taipei 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_fl4.jpg')}}" alt="創星 樂享學 innovative 台北 福隆驛站 fulong Taipei 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_fl5.jpg')}}" alt="創星 樂享學 innovative 台北 福隆驛站 fulong Taipei 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_fl3.jpg')}}" alt="創星 樂享學 innovative 台北 福隆驛站 fulong Taipei 電動輔助自行車 租車 腳踏車" class="img">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
@endsection
