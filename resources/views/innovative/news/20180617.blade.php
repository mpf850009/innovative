@extends('innovative.main')
@section('content')
<style>
	.img{
		padding: 15px 0px 15px 0px;
	}
</style>
<div class="inner_main_agile_section">
	<div class="container">
		<div class="w3-headings-all">
			<h3>{{trans('in_index.open3')}}</h3>
		</div>
		<div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
				<h3>20180617 {{trans('in_index.open3')}}</h3>
				<p></p><br>
				<div class="gallery-w3lsrow">
					<div class="gallery-grids">
						<div class="w3ls-hover">
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_in1.jpg')}}" alt="創星 樂享學 innovative 台南 永康 Tainan Yongkang 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_in7.jpg')}}" alt="創星 樂享學 innovative 台南 永康 Tainan Yongkang 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_in3.jpg')}}" alt="創星 樂享學 innovative 台南 永康 Tainan Yongkang 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_in4.jpg')}}" alt="創星 樂享學 innovative 台南 永康 Tainan Yongkang 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_in5.jpg')}}" alt="創星 樂享學 innovative 台南 永康 Tainan Yongkang 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_in6.jpg')}}" alt="創星 樂享學 innovative 台南 永康 Tainan Yongkang 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_in2.jpg')}}" alt="創星 樂享學 innovative 台南 永康 Tainan Yongkang 電動輔助自行車 租車 腳踏車" class="img">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
@endsection