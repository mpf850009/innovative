@extends('innovative.main')
@section('content')
<style>
	.img{
		padding: 15px 0px 15px 0px;
	}
</style>
<div class="inner_main_agile_section">
	<div class="container">
		<div class="w3-headings-all">
			<h3>{{trans('in_index.open8')}}</h3>
		</div>
		<div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
                <h3>20180911 {{trans('in_index.open8')}}</h3>
                <p></p><br>
				<div class="gallery-w3lsrow">
					<div class="gallery-grids">
						<div class="w3ls-hover">
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_yuli1.jpg')}}" alt="創星 樂享學 innovative 花蓮 玉里 Hualien Yuli 電動輔助自行車 租車 腳踏車" class="img">
                            </div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_yuli2.jpg')}}" alt="創星 樂享學 innovative 花蓮 玉里 Hualien Yuli 電動輔助自行車 租車 腳踏車" class="img">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
@endsection