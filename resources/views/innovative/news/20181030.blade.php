@extends('innovative.main')
@section('content')
<style>
	.img{
		padding: 15px 0px 15px 0px;
	}
	.link{
		font-weight: bold;
		padding: 10px 10px;
		font-size: 20px;
		background-color: lightgray;
	}
</style>
<div class="inner_main_agile_section">
	<div class="container">
		<div class="w3-headings-all">
			<h3>{{trans('in_index.open7')}}</h3>
		</div>
		<div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
				<h3>
					<a class="hvr-icon-wobble-horizontal" href="https://www.taipeicycle.com.tw/zh_TW/index.html"> <i class="fa fa-arrow-right hvr-icon"></i> 10/31 - 11/3 , 2018 {{trans('in_index.open7')}} <br>　{{trans('in_index.ngeh')}}
					</a>
				</h3>
				<p></p><br>
				<div class="gallery-w3lsrow">
					<div class="gallery-grids">
						<div class="w3ls-hover">
							<div class="col-sm-12 col-xs-12">
								<img src="{{asset('images/innovative/tcarea2.png')}}" alt="創星 樂享學 innovative 南港展覽館 台北國際自行車展覽會 Taipei Cycle 電動輔助自行車 腳踏車" class="img">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div><br>
		<a class="link hvr-back-pulse" target="_blank" href="http://booth.e-taitra.com.tw/zh-TW/m/2018CC/5#&ui-state=dialog">{{trans('in_index.imglink')}}</a>
	</div>
</div>
@endsection
