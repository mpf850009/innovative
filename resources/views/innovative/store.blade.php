@extends('innovative.main')
@section('content')
<style>
	.img{
		padding: 15px 0px 15px 0px;
    }
</style>
<div id="gallery" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>{{trans('in_index.store')}}</h3>
        </div>
        <div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
				<h3 class="hideme">
					<a target="_blank" href="https://www.o-fami.com.tw" class="hvr-icon-wobble-horizontal"><i class="fa fa-arrow-right hvr-icon"></i> 樂享學官方網站 O-Fami Official Website </a>
				</h3>
			</div>
			<div class="clearfix"></div>
		</div>
        <div class="gallery-w3lsrow">
            <div class="gallery-grids">
                <div class="w3ls-hover">
                    <div class="col-sm-6 col-xs-12 hideme">
                        <img src="{{asset('images/innovative/store1.jpg')}}" class="img" alt="創星 innovative 樂享學"/>
                    </div>
                    <!--
                    <div class="col-sm-6 col-xs-12 hideme">
                        <img src="{{asset('images/innovative/store3.jpg')}}" class="img" alt="創星 innovative 樂享學"/>
                    </div>
                    <div class="col-sm-6 col-xs-12 hideme">
                        <img src="{{asset('images/innovative/store2.jpg')}}" class="img" alt="創星 innovative 樂享學"/>
                    </div>-->
                    <div class="col-sm-6 col-xs-12 hideme">
                        <img src="{{asset('images/innovative/store4.jpg')}}" class="img" alt="創星 innovative 樂享學"/>
                    </div>
                    <!--
                    <div class="col-sm-6 col-xs-12 hideme">
                        <img src="{{asset('images/innovative/news_in6.jpg')}}" class="img" alt="創星 innovative 樂享學"/>
                    </div>-->
                    <div class="col-sm-6 col-xs-12 hideme">
                        <img src="{{asset('images/innovative/news_in7.jpg')}}" class="img" alt="創星 innovative 樂享學"/>
                    </div>
                    <div class="col-sm-6 col-xs-12 hideme">
                        <img src="{{asset('images/innovative/news_in1.jpg')}}" class="img" alt="創星 innovative 樂享學"/>
                    </div>
                    <div class="col-sm-6 col-xs-12 hideme">
                        <img src="{{asset('images/innovative/news_in3.jpg')}}" class="img" alt="創星 innovative 樂享學"/>
                    </div>
                    <div class="col-sm-6 col-xs-12 hideme">
                        <img src="{{asset('images/innovative/news_in4.jpg')}}" class="img" alt="創星 innovative 樂享學"/>
                    </div>
                    <div class="col-sm-6 col-xs-12 hideme">
                        <img src="{{asset('images/innovative/news_in5.jpg')}}" class="img" alt="創星 innovative 樂享學"/>
                    </div>
                    <div class="col-sm-6 col-xs-12 hideme">
                        <img src="{{asset('images/innovative/news_in2.jpg')}}" class="img" alt="創星 innovative 樂享學"/>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!-- /contact map -->
	<div class="map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3671.7488843908427!2d120.25322841496808!3d23.032990684947865!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x346e71d9dec9de13%3A0x9452d692262750ab!2z5qiC5Lqr5a24!5e0!3m2!1szh-TW!2stw!4v1528685518319"></iframe>
	</div>
<!-- contact map -->
<script src="{{asset('js/innovative/lightbox-plus-jquery.min.js')}}"></script>
@endsection