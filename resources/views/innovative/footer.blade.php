<!-- footer-contact -->
@if($active == 'mpf drive' || $active == "e-bike" || $active == "brand" || $active == "rentplace")
<div class="w3-footer-contact">
	<div class="container">
		<!--
		<div class="col-md-4 w3-footer-contact-left">
			<h3>Trucking</h3>
			<p>Mauris nec consequat nisi. Donec tempus porttitor mollis. Nam tincidunt nulla vel malesuada vestibulum. Nullam tempus,
				mauris et malesuada sodales, arcu erat vestibulum nulla.</p>
		</div>
		<div class="col-md-4 w3-footer-contact-middle">
			<img src="images/innovative/v1.jpg" alt="img">
		</div>
		<div class="col-md-4 w3-footer-contact-right">
			<div class="col-md-2 w3-footer-contact-icon1">
				<i class="fa fa-map-marker" aria-hidden="true"></i>
			</div>
			<div class="col-md-10 w3-footer-contact-info">
				<p>Location:</p>
				<h4>4200 Newyork,Usa</h4>
			</div>
			<div class="clearfix"></div>
			<div class="col-md-2 w3-footer-contact-icon1">
				<i class="fa fa-volume-control-phone" aria-hidden="true"></i>
			</div>
			<div class="col-md-10 w3-footer-contact-info">
				<p>call:</p>
				<h4>+435-25-486</h4>
			</div>
			<div class="clearfix"></div>
			<div class="col-md-2 w3-footer-contact-icon1">
				<i class="fa fa-envelope" aria-hidden="true"></i>
			</div>
			<div class="col-md-10 w3-footer-contact-info">
				<p>Email:</p>
				<h4>
					<a href="mailto:example@mail.com"> mail@example.com</a>
				</h4>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	-->


		<!-- partners -->
		<div class="team gallery-ban" id="team">

			<ul id="flexiselDemo1">
				<li>
					<div class="wthree_testimonials_grid_main">
						<img src="{{asset('images/innovative/co_logo1.png')}}" alt=" " class="img-responsive" style="padding:0em 1em;" />　
					</div>
				</li>
				<li>
					<div class="wthree_testimonials_grid_main">
						<img src="{{asset('images/innovative/co_logo2.png')}}" alt=" " class="img-responsive" style="padding:0em 1em;" />　
					</div>　
				</li>
				<li>
					<div class="wthree_testimonials_grid_main">
						<img src="{{asset('images/innovative/co_logo3.png')}}" alt=" " class="img-responsive" style="padding:0em 1em;" />　
					</div>　
				</li>
				<li>
					<div class="wthree_testimonials_grid_main">
						<img src="{{asset('images/innovative/co_logo4.png')}}" alt=" " class="img-responsive" style="padding:0em 1em;" />　
					</div>　
				</li>
				<li>
					<div class="wthree_testimonials_grid_main">
						<img src="{{asset('images/innovative/co_logo5.png')}}" alt=" " class="img-responsive" style="padding:0em 1em;" />
					</div>　
				</li>
				<li>
					<div class="wthree_testimonials_grid_main">
						<img src="{{asset('images/innovative/co_logo6.png')}}" alt=" " class="img-responsive" style="padding:0em 1em;" />
					</div>
				</li>
				<li>
					<div class="wthree_testimonials_grid_main">
						<img src="{{asset('images/innovative/co_logo7.png')}}" alt=" " class="img-responsive" style="padding:0em 1em;" />
					</div>
				</li>
				<li>
					<div class="wthree_testimonials_grid_main">
						<img src="{{asset('images/innovative/co_logo8.png')}}" alt=" " class="img-responsive" style="padding:0em 1em;" />
					</div>
				</li>
				<li>
					<div class="wthree_testimonials_grid_main">
						<img src="{{asset('images/innovative/co_logo9.png')}}" alt=" " class="img-responsive" style="padding:0em 1em;" />
					</div>
				</li>
				<li>
					<div class="wthree_testimonials_grid_main">
						<img src="{{asset('images/innovative/co_logo10.png')}}" alt=" " class="img-responsive" style="padding:0em 1em;" />
					</div>
				</li>
				<li>
					<div class="wthree_testimonials_grid_main">
						<img src="{{asset('images/innovative/co_logo11.png')}}" alt=" " class="img-responsive" style="padding:0em 1em;" />
					</div>
				</li>
				<li>
					<div class="wthree_testimonials_grid_main">
						<img src="{{asset('images/innovative/co_logo12.png')}}" alt=" " class="img-responsive" style="padding:0em 1em;"/>
					</div>
				</li>
				<li>
					<div class="wthree_testimonials_grid_main">
						<img src="{{asset('images/innovative/co_logo13.png')}}" alt=" " class="img-responsive" style="padding:0em 1em;"/>
					</div>
				</li>
				<li>
					<div class="wthree_testimonials_grid_main">
						<img src="{{asset('images/innovative/co_logo14.png')}}" alt=" " class="img-responsive" style="padding:0em 1em;"/>
					</div>
				</li>
				<li>
					<div class="wthree_testimonials_grid_main">
						<img src="{{asset('images/innovative/co_logo15.png')}}" alt=" " class="img-responsive" style="padding:0em 1em;"/>
					</div>
				</li>
				<li>
					<div class="wthree_testimonials_grid_main">
						<img src="{{asset('images/innovative/co_logo16.png')}}" alt=" " class="img-responsive" style="padding:0em 1em;"/>
					</div>
				</li><!--
				<li>
					<div class="wthree_testimonials_grid_main">
						<img src="{{asset('images/innovative/co_logo17.png')}}" alt=" " class="img-responsive" style="padding:0em 1em;"/>
					</div>
				</li>-->
				<li>
					<div class="wthree_testimonials_grid_main">
						<img src="{{asset('images/innovative/co_logo18.png')}}" alt=" " class="img-responsive" style="padding:0em 1em;"/>
					</div>
				</li>
				<li>
					<div class="wthree_testimonials_grid_main">
						<img src="{{asset('images/innovative/co_logo19.png')}}" alt=" " class="img-responsive" style="padding:0em 1em;"/>
					</div>
				</li>
				<li>
					<div class="wthree_testimonials_grid_main">
						<img src="{{asset('images/innovative/co_logo20.png')}}" alt=" " class="img-responsive" style="padding:0em 1em;"/>
					</div>
				</li>					
			</ul>
		</div>
	</div>
	<!-- partners -->
</div>
@endif
<!-- footer-contact -->
<div class="footer-final">
	<div class="copyw3-agile">
		<div class="container">
			<p align="center">
				<a href="{{route('innovative.index',$locale)}}">{{trans('in_index.home')}}</a>　
				<a href="{{route('innovative.about',$locale)}}" >{{trans('in_index.aboutus')}}</a>　
				<a href="{{route('innovative.news',$locale)}}">{{trans('in_index.news')}}</a>　
				<a href="{{route('innovative.nikimotion',$locale)}}">Nikimotion</a>　
				<a href="{{route('innovative.veer',$locale)}}">Veer</a>　
				<a href="{{route('innovative.ebike',$locale)}}">E-Bike</a>　
				<a href="{{route('innovative.rentmap',$locale)}}">{{trans('in_index.rentplace')}}</a>　
				<a href="{{route('innovative.brand',$locale)}}">{{trans('in_index.brand')}}</a>　
				<a href="{{route('innovative.flagship',$locale)}}">{{trans('in_index.store')}}</a>　
			</p>
			<div class="col-sm-12">
				<hr>
				<p>&copy; COPYRIGHT (C) 2018 Innovative Ind. Co., Ltd. All rights reserved | <a target="_blank" href="http://www.mpfinside.com/MPF_Rent_Server/manager_index.php">BIKE</a> | Design by
					<a target="_blank" href="http://w3layouts.com/">W3layouts</a>
				</p>
			</div>
		</div>
	</div>
</div>