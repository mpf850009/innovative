@extends('innovative.main')
@section('content')
<style>
    ol li{
        font-size: 19px;
    }
    .w3ls-hover{
        padding:2em 2em;
    }
</style>
<div id="production" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>e-bike電動輔助自行車</h3>
        </div>
        <div class="gallery-w3lsrow hideme">
            <div class="col-sm-12">
                <iframe width="100%" height="500" src="https://www.youtube.com/embed/YXKk31SCX0Q?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="clearfix "> </div>
        </div>
    </div>
</div>
<div id="p" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>{{trans('in_nk.p')}}</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.ebikeintro',[$locale,$bike="1"])}}">
                        <img id="p1" src="{{asset('images/innovative/ebike13.jpg')}} " class="img-responsive zoom-img" alt="ebike 創星 innovative 樂享學 胖胎電動輔助自行車"/>
                        <div class="view-caption" style="padding:4.5em 20px;">
                            <h5>{!!trans('in_ebike.bike1h')!!}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class=" col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.ebikeintro',[$locale,$bike="2"])}}">
                        <img id="p2" src=" {{asset('images/innovative/ebike10.jpg')}} " class="img-responsive zoom-img" alt="ebike 創星 innovative 樂享學 平把電動輔助自行車"/>
                        <div class="view-caption" style="padding:4.5em 20px;">
                            <h5>{!!trans('in_ebike.bike2h')!!}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class=" col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.ebikeintro',[$locale,$bike="3"])}}">
                        <img id="p3" src=" {{asset('images/innovative/ebike12.jpg')}} " class="img-responsive zoom-img" alt="ebike 創星 innovative 樂享學 彎把電動輔助自行車"/>
                        <div class="view-caption" style="padding:4.5em 20px;">
                            <h5>{!!trans('in_ebike.bike3h')!!}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class=" col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.ebikeintro',[$locale,$bike="6"])}}">
                        <img id="p6" src=" {{asset('images/innovative/ikin ez i-bike/ez16.jpg')}} " class="img-responsive zoom-img" alt="ebike 創星 innovative 樂享學 20吋 E-Bike" />
                        <div class="view-caption" style="padding:4.5em 20px;">
                            <h5>{!!trans('in_ebike.bike6h')!!}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="clearfix "> </div>
        </div>
    </div>
</div>

<script src="{{asset('js/innovative/lightbox-plus-jquery.min.js')}}"></script>
@endsection