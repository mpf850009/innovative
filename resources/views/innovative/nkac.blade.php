@extends('innovative.main')
@section('content')
<div id="accessories" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            {{-- <h3>{{trans('in_nk.acintro')}}</h3> --}}
            <h3>{{trans('in_nk.afac')}}</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div id="nkac1" class="col-sm-6 col-xs-12 gallery-grids hideme">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 col-xs-8">
                    <div class="w3ls-hover">
                        <a id="chghref" href="{{asset('images/innovative/nkac1.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_nk.acc1t')}}">
                            <img id="chgicon" src="{{asset('images/innovative/nkac1.jpg')}} " class="img-responsive zoom-img " alt="nikimotion 創星 innovative 樂享學 accessories autofold lite blade"/>
                            <div class="view-caption" style="padding:9em 20px;">
                                <span class="glyphicon glyphicon-search"></span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="clearfix"></div>
            </div>
            <div class="col-sm-6 col-xs-12 gallery-grids hideme">
                <div style="padding:1cm 1cm 1cm 1cm;">
                    {!!trans('in_nk.acc1')!!}
                    <!--
					<p style="color:red;">NTD. 699</p><br>-->
                    <h3 style="float:left">
                        <a href="#" onClick="history.back()">
                            <span class="label label-primary">&nbsp;{{trans('in_nk.back')}}&nbsp;</span>
                        </a>
                    </h3>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <!--Edited on 2020/04/15-->
        <div class="gallery-w3lsrow">
            <div id="blac4" class="col-sm-6 col-xs-12 gallery-grids hideme">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 col-xs-8">
                    <div class="w3ls-hover">
                        <a id="chghref" href="{{asset('images/innovative/blac4.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_nk.acc1t')}}">
                            <img id="chgicon" src="{{asset('images/innovative/blac4.jpg')}} " class="img-responsive zoom-img " alt="nikimotion 創星 innovative 樂享學 accessories autofold lite blade"/>
                            <div class="view-caption" style="padding:9em 20px;">
                                <span class="glyphicon glyphicon-search"></span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="clearfix"></div>
            </div>
            <div class="col-sm-6 col-xs-12 gallery-grids hideme">
                <div style="padding:1cm 1cm 1cm 1cm;">
                    {!!trans('in_nk.acc16')!!}
                    <!--
					<p style="color:red;">NTD. 699</p><br>-->
                    <h3 style="float:left">
                        <a href="#" onClick="history.back()">
                            <span class="label label-primary">&nbsp;{{trans('in_nk.back')}}&nbsp;</span>
                        </a>
                    </h3>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="gallery-w3lsrow">
            <div id="blac5" class="col-sm-6 col-xs-12 gallery-grids hideme">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 col-xs-8">
                    <div class="w3ls-hover">
                        <a id="chghref" href="{{asset('images/innovative/blac5.png')}}" data-lightbox="example-set " data-title="{{trans('in_nk.acc1t')}}">
                            <img id="chgicon" src="{{asset('images/innovative/blac5.png')}} " class="img-responsive zoom-img " alt="nikimotion 創星 innovative 樂享學 accessories autofold lite blade"/>
                            <div class="view-caption" style="padding:9em 20px;">
                                <span class="glyphicon glyphicon-search"></span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="clearfix"></div>
            </div>
            <div class="col-sm-6 col-xs-12 gallery-grids hideme">
                <div style="padding:1cm 1cm 1cm 1cm;">
                    {!!trans('in_nk.acc17')!!}
                    <!--
					<p style="color:red;">NTD. 699</p><br>-->
                    <h3 style="float:left">
                        <a href="#" onClick="history.back()">
                            <span class="label label-primary">&nbsp;{{trans('in_nk.back')}}&nbsp;</span>
                        </a>
                    </h3>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="gallery-w3lsrow">
            <div id="afac1" class="col-sm-6 col-xs-12 gallery-grids hideme">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 col-xs-8">
                    <div class="w3ls-hover ">
                        <a id="chghref" href="{{asset('images/innovative/afac1.jpg')}} " data-lightbox="example-set " data-title="{{trans('in_nk.acc8t')}}">
                            <img id="chgicon" src="{{asset('images/innovative/afac1.jpg')}} " class="img-responsive zoom-img " alt="nikimotion 創星 innovative 樂享學 accessories autofold lite blade autofold"/>
                            <div class="view-caption" style="padding:9em 20px;">
                                <span class="glyphicon glyphicon-search"></span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="clearfix"></div>
            </div>
            <div class=" col-sm-6 col-xs-12 gallery-grids hideme">
                <div style="padding:1cm 1cm 1cm 1cm;">
                    {!!trans('in_nk.acc8')!!}
                    <!--
					<p style="color:red;">NTD. 1,399</p><br>-->
                    <h3 style="float:left">
                        <a href="#" onClick="history.back()">
                            <span class="label label-primary">&nbsp;{{trans('in_nk.back')}}&nbsp;</span>
                        </a>
                    </h3>
                </div>
            </div>
            <div class="clearfix"></div>
            <div id="afac2" class="col-sm-6 col-xs-12 gallery-grids hideme">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 col-xs-8">
                    <div class="w3ls-hover ">
                        <a id="chghref" href="{{asset('images/innovative/afac2.jpg')}} " data-lightbox="example-set " data-title="{{trans('in_nk.acc9t')}}">
                            <img id="chgicon" src="{{asset('images/innovative/afac2.jpg')}} " class="img-responsive zoom-img " alt="nikimotion 創星 innovative 樂享學 accessories autofold lite blade autofold"/>
                            <div class="view-caption" style="padding:9em 20px;">
                                <span class="glyphicon glyphicon-search"></span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="clearfix"></div>
            </div>
            <div class=" col-sm-6 col-xs-12 gallery-grids hideme">
                <div style="padding:1cm 1cm 1cm 1cm;">
                    {!!trans('in_nk.acc9')!!}
                    <!--
					<p style="color:red;">NTD. 999</p><br>-->
                    <h3 style="float:left">
                        <a href="#" onClick="history.back()">
                            <span class="label label-primary">&nbsp;{{trans('in_nk.back')}}&nbsp;</span>
                        </a>
                    </h3>
                </div>
            </div>
            <div class="clearfix"></div>
            <div id="afac3" class="col-sm-6 col-xs-12 gallery-grids hideme">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 col-xs-8">
                    <div class="w3ls-hover ">
                        <a id="chghref" href="{{asset('images/innovative/afac3.jpg')}} " data-lightbox="example-set " data-title="{{trans('in_nk.acc10t')}}">
                            <img id="chgicon" src="{{asset('images/innovative/afac3.jpg')}} " class="img-responsive zoom-img " alt="nikimotion 創星 innovative 樂享學 accessories autofold lite blade autofold"/>
                            <div class="view-caption" style="padding:9em 20px;">
                                <span class="glyphicon glyphicon-search"></span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="clearfix"></div>
            </div>
            <div class="col-sm-6 col-xs-12 gallery-grids hideme">
                <div style="padding:1cm 1cm 1cm 1cm;">
                    {!!trans('in_nk.acc10')!!}
                    <!--
					<p style="color:red;">NTD. 1,299</p><br>-->
                    <h3 style="float:left">
                        <a href="#" onClick="history.back()">
                            <span class="label label-primary">&nbsp;{{trans('in_nk.back')}}&nbsp;</span>
                        </a>
                    </h3>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

{{-- <div id="afac" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>{{trans('in_nk.afac')}}</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div id="afac1" class="col-sm-6 col-xs-12 gallery-grids hideme">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 col-xs-8">
                    <div class="w3ls-hover ">
                        <a id="chghref" href="{{asset('images/innovative/afac1.jpg')}} " data-lightbox="example-set " data-title="{{trans('in_nk.acc8t')}}">
                            <img id="chgicon" src="{{asset('images/innovative/afac1.jpg')}} " class="img-responsive zoom-img " alt="nikimotion 創星 innovative 樂享學 accessories autofold lite blade autofold"/>
                            <div class="view-caption" style="padding:9em 20px;">
                                <span class="glyphicon glyphicon-search"></span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="clearfix"></div>
            </div>
            <div class=" col-sm-6 col-xs-12 gallery-grids hideme">
                <div style="padding:1cm 1cm 1cm 1cm;">
                    {!!trans('in_nk.acc8')!!}
                    <!--
					<p style="color:red;">NTD. 1,399</p><br>-->
                    <h3 style="float:left">
                        <a href="#" onClick="history.back()">
                            <span class="label label-primary">&nbsp;{{trans('in_nk.back')}}&nbsp;</span>
                        </a>
                    </h3>
                </div>
            </div>
            <div class="clearfix"></div>
            <div id="afac2" class="col-sm-6 col-xs-12 gallery-grids hideme">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 col-xs-8">
                    <div class="w3ls-hover ">
                        <a id="chghref" href="{{asset('images/innovative/afac2.jpg')}} " data-lightbox="example-set " data-title="{{trans('in_nk.acc9t')}}">
                            <img id="chgicon" src="{{asset('images/innovative/afac2.jpg')}} " class="img-responsive zoom-img " alt="nikimotion 創星 innovative 樂享學 accessories autofold lite blade autofold"/>
                            <div class="view-caption" style="padding:9em 20px;">
                                <span class="glyphicon glyphicon-search"></span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="clearfix"></div>
            </div>
            <div class=" col-sm-6 col-xs-12 gallery-grids hideme">
                <div style="padding:1cm 1cm 1cm 1cm;">
                    {!!trans('in_nk.acc9')!!}
                    <!--
					<p style="color:red;">NTD. 999</p><br>-->
                    <h3 style="float:left">
                        <a href="#" onClick="history.back()">
                            <span class="label label-primary">&nbsp;{{trans('in_nk.back')}}&nbsp;</span>
                        </a>
                    </h3>
                </div>
            </div>
            <div class="clearfix"></div>
            <div id="afac3" class="col-sm-6 col-xs-12 gallery-grids hideme">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 col-xs-8">
                    <div class="w3ls-hover ">
                        <a id="chghref" href="{{asset('images/innovative/afac3.jpg')}} " data-lightbox="example-set " data-title="{{trans('in_nk.acc10t')}}">
                            <img id="chgicon" src="{{asset('images/innovative/afac3.jpg')}} " class="img-responsive zoom-img " alt="nikimotion 創星 innovative 樂享學 accessories autofold lite blade autofold"/>
                            <div class="view-caption" style="padding:9em 20px;">
                                <span class="glyphicon glyphicon-search"></span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="clearfix"></div>
            </div>
            <div class="col-sm-6 col-xs-12 gallery-grids hideme">
                <div style="padding:1cm 1cm 1cm 1cm;">
                    {!!trans('in_nk.acc10')!!}
                    <!--
					<p style="color:red;">NTD. 1,299</p><br>-->
                    <h3 style="float:left">
                        <a href="#" onClick="history.back()">
                            <span class="label label-primary">&nbsp;{{trans('in_nk.back')}}&nbsp;</span>
                        </a>
                    </h3>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div> --}}

<div id="blac" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>{{trans('in_nk.blac')}}</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div id="blac1" class="col-sm-6 col-xs-12 gallery-grids hideme">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 col-xs-8">
                    <div class="w3ls-hover">
                        <a id="chghref" href="{{asset('images/innovative/blac1.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_nk.acc13t')}}">
                            <img id="chgicon" src="{{asset('images/innovative/blac1.jpg')}}" class="img-responsive zoom-img " alt="nikimotion 創星 innovative 樂享學 accessories autofold lite blade blade"/>
                            <div class="view-caption" style="padding:9em 20px;">
                                <span class="glyphicon glyphicon-search"></span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="clearfix"></div>
            </div>
            <div class=" col-sm-6 col-xs-12 gallery-grids hideme">
                <div style="padding:1cm 1cm 1cm 1cm;">
                    {!!trans('in_nk.acc13')!!}
                    <!--
					<p style="color:red;">NTD. 1,399</p><br>-->
                    <h3 style="float:left">
                        <a href="#" onClick="history.back()">
                            <span class="label label-primary">&nbsp;{{trans('in_nk.back')}}&nbsp;</span>
                        </a>
                    </h3>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<script src="{{asset('js/innovative/lightbox-plus-jquery.min.js')}}"></script>
@endsection
