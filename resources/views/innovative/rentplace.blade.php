@extends('innovative.main')
@section('content')
<style>
	.img{
		padding: 15px 0px 15px 0px;
    }
    .link{
		font-weight: bold;
		padding: 10px 10px;
		font-size: 20px;
		background-color: lightgray;
	}
</style>

@if($place=="fulong")
<div class="inner_main_agile_section">
	<div class="container">
		<div class="w3-headings-all">
			<h3>新北福隆驛站</h3>
		</div>
		<div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
				<div class="gallery-w3lsrow">
					<div class="gallery-grids">
						<div class="w3ls-hover">
							<div class="col-xs-12">
								<img src="{{asset('images/innovative/news_fl2.jpg')}}" alt="創星 樂享學 innovative 台北 福隆驛站 fulong Taipei 電動輔助自行車 租車 腳踏車" class="img">
							</div>
						</div>
						<br>
						<h3>福隆驛站Fulong Station</h3><br>
						<h4>營業時間</h4>
						<p>
							週一至週五 09:00-18:00<br>
							週六至週日 08:30-18:00<br>
						</p><br>
						<h4>聯絡電話</h4>
						<p>0905-367-700 顏敏如</p><br>
						<h4>地址</h4>
						<p>新北市貢寮區福隆街2巷1-1號</p><br>
						<h4>Facebook</h4>
						<p><a target="_blank" href="https://zh-tw.facebook.com/hpbike/">福隆驛站</a></p><br>
						<h4>官方網站</h4>
						<p><a target="_blank" href="http://www.ifulong.com.tw/">福隆驛站</a></p><br>
						<h4>Google Map</h4><br>
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3615.535661373456!2d121.94298461441527!3d25.01588938398086!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x345d5c5484b55c5b%3A0x50fe76487401c49f!2zTVBGIERyaXZlIOemj-mahuermQ!5e0!3m2!1szh-TW!2stw!4v1543820591262" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe><br>

					</div>
				</div>
			</div>
			<div class="clearfix"></div>
        </div>
        <br>
        <h2 style="float:left" class="hideme">
            <a href="#" onClick="history.back()">
                <span class="label label-primary">{{trans('in_nk.back')}}</span>
            </a>
        </h2>
	</div>
</div>
@endif

@if($place=='chishang')
<div class="inner_main_agile_section">
	<div class="container">
		<div class="w3-headings-all">
			<h3>台東池上站</h3>
		</div>
		<div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
				<div class="gallery-w3lsrow">
					<div class="gallery-grids">
						<div class="w3ls-hover">
							<div class="col-xs-12">
								<img src="{{asset('images/innovative/bg24.jpg')}}" alt="創星 樂享學 innovative 台東 池上 Taitung Chishang 電動輔助自行車 租車 腳踏車" class="img">
							</div>
						</div>
						<br>
						<h3>台東池上站Chishang Station</h3><br>
						<h4>營業時間</h4>
						<p>
							週一至週四 06:00-17:00<br>
							　　　週五 05:00-17:00<br>
							週六至週日 06:00-17:00<br>
						</p><br>
						<h4>聯絡電話</h4>
						<p>0919-139020 邱佩馨<br>0919-139720 林涂仲</p><br>
						<h4>地址</h4>
						<p>958台東縣池上鄉中正路48號</p><br>
						<h4>Google Map</h4><br>
						<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14677.006217253087!2d121.2209947!3d23.1244787!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc56a24131d6739f3!2z5rGg5LiK55Kw6Y6u6Ieq6KGM6LuK5bqX!5e0!3m2!1szh-TW!2stw!4v1543824232686" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe><br>

					</div>
				</div>
			</div>
			<div class="clearfix"></div>
        </div>
        <br>
        <h2 style="float:left" class="hideme">
            <a href="#" onClick="history.back()">
                <span class="label label-primary">{{trans('in_nk.back')}}</span>
            </a>
        </h2>
	</div>
</div>
@endif

@if($place=='yongkang')
<div class="inner_main_agile_section">
	<div class="container">
		<div class="w3-headings-all">
			<h3>台南永康樂享學</h3>
		</div>
		<div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
				<div class="gallery-w3lsrow">
					<div class="gallery-grids">
						<div class="w3ls-hover">
							<div class="col-xs-12">
								<img src="{{asset('images/innovative/store1.jpg')}}" alt="創星 樂享學 innovative 台南 永康 Tainan Yongkang 電動輔助自行車 租車 腳踏車" class="img">
							</div>
						</div>
						<br>
						<h3>樂享學O-Fami</h3><br>
						<h4>營業時間</h4>
						<p>
							一 10:00-18:00<br>
							二 10:00-18:00<br>
							三 10:00-18:00<br>
							四 公休<br>
							五 10:00-18:00<br>
							六 09:00-17:00<br>
							日 09:00-17:00<br>
						</p><br>
						<h4>聯絡電話</h4>
						<p>06-2028028</p><br>
						<h4>地址</h4>
						<p>台南市永康區中山路298號</p><br>
						<h4>Facebook</h4>
						<p><a target="_blank" href="https://www.facebook.com/o.fami.tw/">樂享學O-Fami</a></p><br>
						<h4>Instagram</h4>
						<p><a target="_blank" href="https://www.instagram.com/o.fami.tw/">樂享學O-Fami</a></p><br>
						<h4>官方網站</h4>
						<p><a target="_blank" href="https://www.o-fami.com.tw/">樂享學O-Fami</a></p><br>
						<h4>Google Map</h4><br>
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3671.7488843908445!2d120.2532284143682!3d23.03299068494779!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x346e71d9dec9de13%3A0x9452d692262750ab!2z5qiC5Lqr5a24!5e0!3m2!1szh-TW!2stw!4v1543818683010" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe><br>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
        <br>
        <h2 style="float:left" class="hideme">
            <a href="#" onClick="history.back()">
                <span class="label label-primary">{{trans('in_nk.back')}}</span>
            </a>
        </h2>
	</div>
</div>
@endif

@if($place=='yuli')
<div class="inner_main_agile_section">
	<div class="container">
		<div class="w3-headings-all">
			<h3>花蓮玉里站</h3>
		</div>
		<div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
				<div class="gallery-w3lsrow">
					<div class="gallery-grids">
						<div class="w3ls-hover">
							<div class="col-xs-12">
								<img src="{{asset('images/innovative/news_yuli1.jpg')}}" alt="創星 樂享學 innovative 花蓮 玉里 Hualien Yuli 電動輔助自行車 租車 腳踏車" class="img">
							</div>
						</div>
						<br>
						<h3>花蓮玉里站Yuli Station</h3><br>
						<h4>營業時間</h4>
						<p>09:00-17:00 每日</p><br>
						<h4>聯絡電話</h4>
						<p>0963667167</p><br>
						<h4>地址</h4>
						<p>花蓮縣玉里鎮康樂街39號</p><br>
						<h4>Google Map</h4><br>
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3663.5655101578673!2d121.30952621437494!3d23.33151808479743!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x346f6a7ce441a1dd%3A0x7545c3b51030d4e5!2zOTgx6Iqx6JOu57ij546J6YeM6Y6u5bq35qiC6KGXMznomZ8!5e0!3m2!1szh-TW!2stw!4v1543825372505" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe><br>

					</div>
				</div>
			</div>
			<div class="clearfix"></div>
        </div>
        <br>
        <h2 style="float:left" class="hideme">
            <a href="#" onClick="history.back()">
                <span class="label label-primary">{{trans('in_nk.back')}}</span>
            </a>
        </h2>
	</div>
</div>
@endif

@if($place=="asheng")
<div class="inner_main_agile_section">
	<div class="container">
		<div class="w3-headings-all">
			<h3>台東阿勝租車</h3>
		</div>
		<div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
				<div class="gallery-w3lsrow">
					<div class="gallery-grids">
						<div class="w3ls-hover">
							<div class="col-xs-12">
								<img src="{{asset('images/innovative/news_as2.jpg')}}" alt="創星 樂享學 innovative 台東 阿勝單車 Taitung A-Sheng 電動輔助自行車 租車 腳踏車" class="img">
							</div>
						</div>
						<br>
						<h3>台東阿勝租車A-Sheng Station</h3><br>
						<h4>營業時間</h4>
						<p>09:00-18:00 每日</p><br>
						<h4>聯絡電話</h4>
						<p>0982-158153</p><br>
						<h4>地址</h4>
						<p>台東市新站路223號</p><br>
						<h4>Google Map</h4><br>
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5201.825622362743!2d121.12384078743067!3d22.793340483517955!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x346fba2d2dfccf93%3A0x8f4a7a4181d3c52e!2z6Zi_5Yud5Zau6LuK5Y-w5p2x5bqX!5e0!3m2!1szh-TW!2stw!4v1543826324832" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe><br>

					</div>
				</div>
			</div>
			<div class="clearfix"></div>
        </div>
        <br>
        <h2 style="float:left" class="hideme">
            <a href="#" onClick="history.back()">
                <span class="label label-primary">{{trans('in_nk.back')}}</span>
            </a>
        </h2>
	</div>
</div>
@endif

@if($place=="fengyuan")
<div class="inner_main_agile_section">
	<div class="container">
		<div class="w3-headings-all">
			<h3>台中豐原起站租車</h3>
		</div>
		<div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
				<div class="gallery-w3lsrow">
					<div class="gallery-grids">
						<div class="w3ls-hover">
							<div class="col-xs-12">
								<img src="{{asset('images/innovative/bg30.jpg')}}" alt="創星 樂享學 innovative 台中 起站租車 Taichung Fengyuan 電動輔助自行車 租車 腳踏車" class="img">
							</div>
						</div>
						<br>
						<h3>起站租車Fengyuan Station</h3><br>
						<h4>營業時間</h4>
						<p>
							一 09:00-18:30<br>
							二 公休<br>
							三 公休<br>
							四 09:00-18:30<br>
							五 09:00-18:30<br>
							六 09:00-18:30<br>
							日 09:00-18:30<br>
						</p><br>
						<h4>聯絡電話</h4>
						<p>
							04-25155903<br>
							0929-686135
						</p><br>
						<h4>地址</h4>
						<p>420台中市豐原區豐勢路一段103號</p><br>
						<!--
						<h4>Facebook</h4>
						<p><a target="_blank" href="https://www.facebook.com/starting.electric.bikes/">起站租車-電動自行車城市旅遊</a></p><br>
						-->
						<h4>Google Map</h4><br>
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3148.5236106515213!2d120.72245008561092!3d24.25383567038971!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x34691b0292bd26e9%3A0x91459572e51778cc!2z6LW356uZ56ef6LuKLembu-WLleiHquihjOi7iuWfjuW4guaXhemBig!5e0!3m2!1szh-TW!2stw!4v1543821780427" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe><br>

					</div>
				</div>
			</div>
			<div class="clearfix"></div>
        </div>
        <br>
        <h2 style="float:left" class="hideme">
            <a href="#" onClick="history.back()">
                <span class="label label-primary">{{trans('in_nk.back')}}</span>
            </a>
        </h2>
	</div>
</div>
@endif

<!--
@if($place=="clap")
<div class="inner_main_agile_section">
	<div class="container">
		<div class="w3-headings-all">
			<h3>阿里山掌聲響起民宿</h3>
		</div>
		<div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
				<div class="gallery-w3lsrow">
					<div class="gallery-grids">
						<div class="w3ls-hover">
							<div class="col-md-3"></div>
							<div class="col-md-6">
								<img src="{{asset('images/innovative/clap1.jpg')}}" alt="創星 樂享學 innovative 阿里山 掌聲響起 Chiayi 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-md-3"></div>
						</div>
						<br>
						<h3>掌聲響起民宿 applause homestay</h3><br>
						<!--
						<h4>營業時間</h4>
						<p>
							一 09:00-18:30<br>
							二 公休<br>
							三 公休<br>
							四 09:00-18:30<br>
							五 09:00-18:30<br>
							六 09:00-18:30<br>
							日 09:00-18:30<br>
						</p><br>-->
						<h4>聯絡方式</h4>
						<p>
							Line ID：052562909<br>
							電話：05-2562-909<br>
							傳真：05-2562-929
						</p><br>
						<h4>地址</h4>
						<p>嘉義縣阿里山鄉樂野村8鄰樂野209-6號</p><br>
						<h4>官方網站</h4>
						<p><a target="_blank" href="http://www.applause.tw/index.html">阿里山掌聲響起民宿</a></p><br>
						<h4>Facebook</h4>
						<p><a target="_blank" href="https://www.facebook.com/%E9%98%BF%E9%87%8C%E5%B1%B1%E6%B0%91%E5%AE%BF-%E6%8E%8C%E8%81%B2%E9%9F%BF%E8%B5%B7-172855626235122/?fref=ts">阿里山掌聲響起民宿</a></p><br>
						<h4>Google Map</h4><br>
						<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14640.356139084699!2d120.7187032!3d23.4572527!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb839c95c9839f0e!2z5o6M6IGy6Z-_6LW35rCR5a6_!5e0!3m2!1szh-TW!2stw!4v1549867470343" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe><br>

					</div>
				</div>
			</div>
			<div class="clearfix"></div>
        </div>
        <br>
        <h2 style="float:left" class="hideme">
            <a href="#" onClick="history.back()">
                <span class="label label-primary">{{trans('in_nk.back')}}</span>
            </a>
        </h2>
	</div>
</div>
@endif
-->

@if($place=="houli")
<div class="inner_main_agile_section">
	<div class="container">
		<div class="w3-headings-all">
			<h3>后里奇典租車</h3>
		</div>
		<div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
				<div class="gallery-w3lsrow">
					<div class="gallery-grids">
						<div class="w3ls-hover">
							<div class="col-md-3"></div>
							<div class="col-md-6">
								<img src="{{asset('images/innovative/houli1.jpg')}}" alt="創星 樂享學 innovative 后里 奇典租車 Taichung Houli 電動輔助自行車 租車 腳踏車">
							</div>
							<div class="col-md-3"></div>
						</div>
						<br>
						<h3>奇典租車</h3><br>
						<h4>營業時間</h4>
						<p>07:00-20:00</p><br>
						<h4>聯絡方式</h4>
						<p>電話：0930-681103</p><br>
						<h4>地址</h4>
						<p>421台中市后里區甲后路9號</p><br>
						<h4>官方網站</h4>
						<p><a target="_blank" href="https://chidian.mmweb.tw/?fbclid=IwAR0_dpIgE3RZJmGwJ2u-MgLoGC8kO8KAAsIC8IlHl_Ush9qzz9HpXan-0Ps">后豐鐵馬道.奇典租車中心</a></p><br>
						<h4>Facebook</h4>
						<p><a target="_blank" href="https://www.facebook.com/chidian2/">后豐鐵馬道.奇典租車中心</a></p><br>
						<h4>Google Map</h4><br>
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d909.0162423694995!2d120.73179572915456!3d24.309368799019797!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x34691b4b7d8e6477%3A0xbb6d1a6cd69d530c!2z5aWH5YW456ef6LuK5Lit5b-DICjlkI7osZDpkLXppqzpgZPlj4rmnbHosZDntqDoibLotbDlu4rnp5_ou4op!5e0!3m2!1szh-TW!2stw!4v1549868886529" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe><br>

					</div>
				</div>
			</div>
			<div class="clearfix"></div>
        </div>
        <br>
        <h2 style="float:left" class="hideme">
            <a href="#" onClick="history.back()">
                <span class="label label-primary">{{trans('in_nk.back')}}</span>
            </a>
        </h2>
	</div>
</div>
@endif

@if($place=="zuojhen")
<div class="inner_main_agile_section">
	<div class="container">
		<div class="w3-headings-all">
			<h3>左鎮月世界旅遊服務中心</h3>
		</div>
		<div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
				<div class="gallery-w3lsrow">
					<div class="gallery-grids">
						<div class="w3ls-hover">
							<div class="col-xs-12">
								<img src="{{asset('images/innovative/zuojhen1.jpg')}}" alt="創星 樂享學 innovative 左鎮 月世界旅遊服務中心 zuojhen 電動輔助自行車 租車 腳踏車" class="img">
							</div>
						</div>
						<br>
						<h3>左鎮月世界旅遊服務中心</h3><br>
						<h4>營業時間</h4>
						<p>08:30-17:00</p><br>
						<h4>聯絡方式</h4>
						<p>電話：06-5730061</p><br>
						<h4>地址</h4>
						<p>地址台南市左鎮區岡林里31號</p><br>
						<h4>官方網站</h4>
						<p><a target="_blank" href="https://www.twtainan.net/zh-tw/Attractions/Detail/4358/">左鎮月世界生態學園(岡林國小)</a></p><br>
						<h4>Facebook</h4>
						<p><a target="_blank" href="https://www.facebook.com/pages/category/Hotel---Lodging/%E5%B7%A6%E9%8E%AE%E6%9C%88%E4%B8%96%E7%95%8C%E6%97%85%E9%81%8A%E6%9C%8D%E5%8B%99%E4%B8%AD%E5%BF%83-993076310750344/">左鎮月世界旅遊服務中心</a></p><br>
						<h4>Google Map</h4><br>
						<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14689.812732544462!2d120.41478!3d23.007127!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6578936349d2e2ef!2z5bem6Y6u5pyI5LiW55WM5peF6YGK5pyN5YuZ5Lit5b-D!5e0!3m2!1szh-TW!2stw!4v1549869797197" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe><br>

					</div>
				</div>
			</div>
			<div class="clearfix"></div>
        </div>
        <br>
        <h2 style="float:left" class="hideme">
            <a href="#" onClick="history.back()">
                <span class="label label-primary">{{trans('in_nk.back')}}</span>
            </a>
        </h2>
	</div>
</div>
@endif

@endsection
