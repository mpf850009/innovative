@extends('innovative.main')
@section('content')
@include('innovative.script.nikimotionscript')
<link href="https://fonts.googleapis.com/css?family=Montserrat:300" rel="stylesheet">
<style>
.font p{
    font-family: 'Montserrat', sans-serif,Microsoft JhengHei;
    font-size: 18px;
}
.font h3{
    font-family: 'Montserrat', sans-serif,Microsoft JhengHei;
    font-size: 35px;
}
.font li{
    font-family: 'Montserrat', sans-serif,Microsoft JhengHei;
    font-size: 18px;
} 
.font strong{
    font-family: 'Montserrat', sans-serif,Microsoft JhengHei;
    font-size: 22px;
} 
.container ul{
    list-style-position: inside;
    list-style-image:url('images/innovative/nklogo3.png');
}
.margin_top_10{
    margin-top: 10px;
}
.padding_bottom_20{
    padding-bottom:20px;
}
.span_color{
    color: black;
    font-size: 10px;
}
.pad_0{
    padding: 0 0;
}
</style>

<div id="production" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>Autofold</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-12 hideme">
                <iframe width="100%" height="500" src="https://www.youtube.com/embed/p1vpCyCxLbs?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="clearfix "> </div>
        </div>
    </div>
</div>
<!-- <div id="autofold" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>AutoFold {{trans('in_nk.fc')}}</h3>
        </div>
        <div class="col-sm-6 col-xs-12">
            <div class="col-sm-2 col-xs-2 hideme">
                <a class="btnFollow13" title="color-Scarlett">
                    <img src="{{asset('images/innovative/color-Scarlett.png')}}" class="img-circle" width="40px" height="40px">
                </a>
            </div>
            <div class="col-sm-2 col-xs-2 hideme">
                <a class="btnFollow14" title="color-Berry">
                    <img src="{{asset('images/innovative/color-Berry.jpg')}}" class="img-circle" width="40px" height="40px">
                </a>
            </div>
            <div class="col-sm-2 col-xs-2 hideme">
                <a class="btnFollow15" title="color-Capri">
                    <img src="{{asset('images/innovative/color-Capri.jpg')}}" class="img-circle" width="40px" height="40px">
                </a>
            </div>
            <div class="col-sm-2 col-xs-2 hideme">
                <a class="btnFollow16" title="color-Ebony">
                    <img src="{{asset('images/innovative/color-Ebony.png')}}" class="img-circle" width="40px" height="40px">
                </a>
            </div>
            <div class="col-sm-2 col-xs-2 hideme">
                <a class="btnFollow17" title="color-Emerald">
                    <img src="{{asset('images/innovative/color-Emerald.jpg')}}" class="img-circle" width="40px" height="40px">
                </a>
            </div>
            <div class="col-sm-2 col-xs-2 hideme">
                <a class="btnFollow18" title="color-Mink">
                    <img src="{{asset('images/innovative/color-Mink.jpg')}}" class="img-circle" width="40px" height="40px">
                </a>
            </div>
            <div class="clearfix"></div><br>
            <div class="col-sm-2 col-xs-2 hideme">
                <a class="btnFollow19" title="color-Oyster">
                    <img src="{{asset('images/innovative/color-Oyster.jpg')}}" class="img-circle" width="40px" height="40px">
                </a>
            </div>
            <div class="col-sm-2 col-xs-2 hideme">
                <a class="btnFollow20" title="color-Tangerine">
                    <img src="{{asset('images/innovative/color-Tangerine.jpg')}}" class="img-circle" width="40px" height="40px">
                </a>
            </div>
            <div class="col-sm-2 col-xs-2 hideme">
                <a class="btnFollow21" title="color-Forest">
                    <img src="{{asset('images/innovative/color-Forest.jpg')}}" class="img-circle" width="40px" height="40px">
                </a>
            </div>
            <div class="col-sm-2 col-xs-2 hideme">
                <a class="btnFollow22" title="color-Graphite">
                    <img src="{{asset('images/innovative/color-Graphite.jpg')}}" class="img-circle" width="40px" height="40px">
                </a>
            </div>
            <div class="col-sm-2 col-xs-2 hideme">
                <a class="btnFollow23" title="color-GreyBlue">
                    <img src="{{asset('images/innovative/color-GreyBlue.jpg')}}" class="img-circle" width="40px" height="40px">
                </a>
            </div>
            <div class="col-sm-2 col-xs-2 hideme">
                <a class="btnFollow24" title="color-Camel">
                    <img src="{{asset('images/innovative/color-Camel.jpg')}}" class="img-circle" width="40px" height="40px">
                </a>
            </div>
            <div class="clearfix"></div><br><br>
            <div class="col-sm-1"></div>
            <div class="col-sm-10 hideme">
                <div class="w3ls-hover">
                    <a href="{{asset('images/innovative/afgif1.gif')}}" id="chghref" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/afgif1.gif')}}" class="img-responsive zoom-img" id="chgicon" alt="創星 樂享學 innovative autofold Nikimotion" />
                        <div class="view-caption" style="padding:10em 20px;">
                            <span class="glyphicon glyphicon-search "></span>
                        </div>
                    </a>
                </div><br><br>
            </div>
            <div class="col-sm-1"></div>
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-6 col-xs-12 font" style="padding:0px 50px;">
            <h3 class="hideme">AUTOFOLD</h3><br>
            {!!trans('in_nk.dc1')!!}
        </div>
    </div>
</div> -->
<!--Edited on 2020/04/23 -->
<div id="autofold" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>AutoFold {{trans('in_nk.fc')}}</h3>
        </div>
        <div class="col-12">
            <div class="col-md-6" style="margin-bottom:50px !important; ">
                <div class="w3ls-hover" style="padding:0 20%;">
                    <a href="{{asset('images/innovative/afgif15.gif ')}}" id="chghref" data-lightbox="example-set" data-title="" >
                        <img src="{{asset('images/innovative/afgif15.gif ')}}" class="img-responsive zoom-img" id="chgicon" alt="創星 樂享學 innovative autofold Nikimotion" />
                        <div class="view-caption" style="padding:8em 20px;">
                            <span class="glyphicon glyphicon-search "></span>
                        </div>
                    </a>   
                </div><br><br>
                <div class="clearfix"></div>
                <div class="col-sm-12 col-xs-12 hideme">
                    <div class="col-sm-3 col-xs-2 margin_top_10"> 
                        <h4>運動系列</h4>
                    </div>
                    <div class="col-sm-9 col-xs-10 col-md-9 padding_bottom_20">
                        <div class="col-sm-4 col-xs-4 hideme pad_0 text-center">
                            <a class="btnFollow13" title="color-Scarlett">
                                <img src="{{asset('images/innovative/color-Scarlett.png')}}" class="img-circle" width="40px" height="40px"><br>
                                <span class="span_color">珊瑚紅</span>
                            </a>
                        </div>
                        <div class="col-sm-4 col-xs-4 hideme pad_0 text-center">
                            <a class="btnFollow14" title="color-Berry">
                                <img src="{{asset('images/innovative/color-Berry.jpg')}}" class="img-circle" width="40px" height="40px"><br>
                                <span class="span_color">紅莓紫</span>
                            </a>
                        </div>
                        <div class="col-sm-4 col-xs-4 hideme pad_0 text-center">
                            <a class="btnFollow15" title="color-Capri">
                                <img src="{{asset('images/innovative/color-Capri.jpg')}}" class="img-circle" width="40px" height="40px"><br>
                                <span class="span_color">湖水藍</span>
                            </a>
                        </div>
                        <div class="clearfix"></div><br>
                        <div class="col-sm-4 col-xs-4 hideme pad_0 text-center">
                            <a class="btnFollow16" title="color-Tangerine">
                                <img src="{{asset('images/innovative/color-Tangerine.jpg')}}" class="img-circle" width="40px" height="40px"><br>
                                <span class="span_color">橙陽橘</span>
                            </a>
                        </div>
                        <div class="col-sm-4 col-xs-4 hideme pad_0 text-center">
                            <a class="btnFollow17" title="color-Emerald">
                                <img src="{{asset('images/innovative/color-Emerald.jpg')}}" class="img-circle" width="40px" height="40px"><br>
                                <span class="span_color">大地綠</span>
                            </a>
                        </div>
                        <div class="col-sm-4 col-xs-4 hideme pad_0 text-center">
                            <a class="btnFollow18" title="color-black">
                                <img src="{{asset('images/innovative/color-black.png')}}" class="img-circle" width="40px" height="40px"><br>
                                <span class="span_color">經典黑</span>
                            </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div> 
                <div class="clearfix"></div>
                <div class="col-sm-12 col-xs-12 hideme">
                    <div class="col-sm-3 col-xs-2 margin_top_10"> 
                        <h4 >亞麻系列</h4>
                    </div>
                    <div class="col-sm-9 col-xs-10 col-md-9 padding_bottom_20">
                        <div class="col-sm-4 col-xs-4 hideme pad_0 text-center">
                            <a class="btnFollow22" title="color-Forest">
                                <img src="{{asset('images/innovative/color-Forest.jpg')}}" class="img-circle" width="40px" height="40px"><br>
                                <span class="span_color">森林綠</span>
                            </a>
                        </div>
                        <div class="col-sm-4 col-xs-4 hideme pad_0 text-center">
                            <a class="btnFollow23" title="color-Graphite">
                                <img src="{{asset('images/innovative/color-Graphite.jpg')}}" class="img-circle" width="40px" height="40px"><br>
                                <span class="span_color">石墨黑</span>
                            </a>
                        </div>
                        <div class="col-sm-4 col-xs-4 hideme pad_0 text-center">
                            <a class="btnFollow24" title="color-GreyBlue">
                                <img src="{{asset('images/innovative/color-GreyBlue.jpg')}}" class="img-circle" width="40px" height="40px"><br>
                                <span class="span_color">天藍灰</span>
                            </a>
                        </div>
                        <div class="clearfix"></div><br>
                        <div class="col-sm-4 col-xs-4 hideme pad_0 text-center">
                            <a class="btnFollow25" title="color-Camel">
                                <img src="{{asset('images/innovative/color-Camel.jpg')}}" class="img-circle" width="40px" height="40px"><br>
                                <span class="span_color">卡其駝</span>
                            </a>
                        </div>
                        <div class="col-sm-4 col-xs-4 hideme pad_0 text-center">
                            <a class="btnFollow26" title="color-Cherry">
                                <img src="{{asset('images/innovative/color-Cherry.jpg')}}" class="img-circle" width="40px" height="40px"><br>
                                <span class="span_color">櫻桃紅</span>
                            </a>
                        </div>
                        <div class="col-sm-4 col-xs-4 hideme pad_0 text-center">
                            <a class="btnFollow27" title="color-Aqua">
                                <img src="{{asset('images/innovative/color-Aqua.jpg')}}" class="img-circle" width="40px" height="40px"><br>
                                <span class="span_color">天空藍</span>
                            </a>
                        </div>
                    </div>
                </div> 
                <div class="clearfix"></div>
                <div class="col-sm-12 col-xs-12 hideme">
                    <div class="col-sm-3 col-xs-2 margin_top_10"> 
                        <h4 >經典系列</h4>
                    </div>
                    <div class="col-sm-9 col-xs-10 col-md-9 padding_bottom_20">
                        <div class="col-sm-4 col-xs-4 hideme pad_0 text-center">
                            <a class="btnFollow19" title="color-milk">
                                <img src="{{asset('images/innovative/color-Mink.jpg')}}" class="img-circle" width="40px" height="40px"><br>
                                <span class="span_color">米白貂</span>
                            </a>
                        </div>
                        <div class="col-sm-4 col-xs-4 hideme pad_0 text-center">
                            <a class="btnFollow20" title="color-Oyster">
                                <img src="{{asset('images/innovative/color-Oyster.jpg')}}" class="img-circle" width="40px" height="40px"><br>
                                <span class="span_color">牡蠣白</span>
                            </a>
                        </div>
                        <div class="col-sm-4 col-xs-4 hideme pad_0 text-center">
                            <a class="btnFollow21" title="color-Ebony">
                                <img src="{{asset('images/innovative/color-Ebony.png')}}" class="img-circle" width="40px" height="40px"><br>
                                <span class="span_color">檀木藍</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-6">
                <div class="col-12" style="margin-top:50px;">
                    <h3>AUTOFOLD</h3><br>
                    {!!trans('in_nk.dc1')!!}
                </div>
                <div class="clearfix"></div>
            </div>

        </div>
    </div>
</div>   

<div class="gallery-w3lsrow">
    <div class="container">
        <div class="gallery-grids">
            <div class="col-xs-12 hideme ">
                <img src="images/innovative/au0.png">
            </div>
            <div class="col-xs-12 hideme">
                <img src="images/innovative/au2.png">
            </div>
            <div class="col-xs-12 hideme">
                <img src="images/innovative/au3.png">
            </div>
            <div class="col-xs-12 hideme">
                <img src="images/innovative/au4.png">
            </div>
            <div class="col-xs-12 hideme">
                <img src="images/innovative/au5.png">
            </div>
            <div class="col-xs-12 hideme">
                <img src="images/innovative/au6.png">
            </div>
            <div class="col-xs-12 hideme">
                <img src="images/innovative/au8.png">
            </div>
        </div> 
    </div> 
</div>


<div id="nkac" class="gallery">
    <div class="container">
        <div class="w3-headings-all">
            <h3>Autofold {{trans('in_nk.ac')}}</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.nkac',$locale)}}#nkac1">
                        <img src="{{asset('images/innovative/nkac1.jpg')}}" class="img-responsive zoom-img" alt="創星 樂享學 innovative Accessories Nikimotion" />
                        <div class="view-caption" style="font-size:12px; padding:7em 20px;">
                            <h5>{{trans('in_nk.acc1t')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.nkac',$locale)}}#afac1">
                        <img src="{{asset('images/innovative/afac1.jpg')}}" class="img-responsive zoom-img" alt="創星 樂享學 innovative Accessories Nikimotion AUTOFOLD" />
                        <div class="view-caption" style="font-size:12px; padding:7em 20px;">
                            <h5>{!!trans('in_nk.acc8t')!!}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.nkac',$locale)}}#afac2">
                        <img src="{{asset('images/innovative/afac2.jpg')}}" class="img-responsive zoom-img" alt="創星 樂享學 innovative Accessories Nikimotion AUTOFOLD" />
                        <div class="view-caption" style="font-size:12px; padding:7em 20px;">
                            <h5>{{trans('in_nk.acc9t')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.nkac',$locale)}}#afac3">
                        <img src="{{asset('images/innovative/afac3.jpg')}}" class="img-responsive zoom-img" alt="創星 樂享學 innovative Accessories Nikimotion AUTOFOLD" />
                        <div class="view-caption" style="font-size:12px; padding:7em 20px;">
                            <h5>{{trans('in_nk.acc10t')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            {{-- <div class="col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.nkac',$locale)}}#blac4">
                        <img src="{{asset('images/innovative/blac4.jpg')}}" class="img-responsive zoom-img" alt="創星 樂享學 innovative Accessories Nikimotion AUTOFOLD" />
                        <div class="view-caption" style="font-size:12px; padding:7em 20px;">
                            <h5>{{trans('in_nk.acc16t')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div> --}}
            <div class="col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.nkac',$locale)}}#blac5">
                        <img src="{{asset('images/innovative/blac5.png')}}" class="img-responsive zoom-img" alt="創星 樂享學 innovative Accessories Nikimotion AUTOFOLD" />
                        <div class="view-caption" style="font-size:12px; padding:7em 20px;">
                            <h5>{{trans('in_nk.acc17t')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <!-- Edited on 2020/04/15-->
            {{-- <div class="col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.nkac',$locale)}}#blac4">
                        <img src="{{asset('images/innovative/blac4.jpg')}}" class="img-responsive zoom-img" alt="創星 樂享學 innovative Accessories Nikimotion blade" />
                        <div class="view-caption" style="font-size:12px; padding:6em 20px;">
                            <h5>{{trans('in_nk.acc16t')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.nkac',$locale)}}#blac5">
                        <img src="{{asset('images/innovative/blac5.png')}}" class="img-responsive zoom-img" alt="創星 樂享學 innovative Accessories Nikimotion blade" />
                        <div class="view-caption" style="font-size:12px; padding:6em 20px;">
                            <h5>{{trans('in_nk.acc17t')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div> --}}
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="gallery">
    <div class="container">
        <h3>
            <a href="{{route('innovative.nikimotion',$locale)}}#production">
                <span class="label label-primary">&nbsp;{{trans('in_nk.back')}}&nbsp;</span>
            </a>
        </h3>
    </div>
</div> 

<script src="{{asset('js/innovative/lightbox-plus-jquery.min.js')}}"></script>
<script src="{{asset('js//innovative/bars.js')}}"></script>
@endsection

