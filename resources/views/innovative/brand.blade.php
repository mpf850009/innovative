@extends('innovative.main')
@section('content')
<!-- BRAND -->
<div id="brand" class="gallery">
    <div class="container">
        <div class="w3-headings-all">
            <h3>{{trans('in_index.brand')}}</h3>
        </div>
        <div class="gallery-w3lsrow">

            <div class="col-sm-3 col-xs-4 gallery-grids">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/co_logo1.png')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/co_logo1.png')}}" class="img-responsive zoom-img" alt="合作夥伴 創星 innovative 樂享學"/>
                    </a>
                </div>
                <div class="col-sm-2"></div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/co_logo2.png')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/co_logo2.png')}}" class="img-responsive zoom-img" alt="合作夥伴 創星 innovative 樂享學" />
                    </a>
                </div>
                <div class="col-sm-2"></div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/co_logo3.png')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/co_logo3.png')}}" class="img-responsive zoom-img" alt="合作夥伴 創星 innovative 樂享學" />
                    </a>
                </div>
                <div class="col-sm-2"></div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/co_logo4.png')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/co_logo4.png')}}" class="img-responsive zoom-img" alt="合作夥伴 創星 innovative 樂享學" />
                    </a>
                </div>
                <div class="col-sm-2"></div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/co_logo5.png')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/co_logo5.png')}}" class="img-responsive zoom-img" alt="合作夥伴 創星 innovative 樂享學" />
                    </a>
                </div>
                <div class="col-sm-2"></div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/co_logo6.png')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/co_logo6.png')}}" class="img-responsive zoom-img" alt="合作夥伴 創星 innovative 樂享學"/>
                    </a>
                </div>
                <div class="col-sm-2"></div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/co_logo7.png')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/co_logo7.png')}}" class="img-responsive zoom-img" alt="合作夥伴 創星 innovative 樂享學"/>
                    </a>
                </div>
                <div class="col-sm-2"></div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/co_logo8.png')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/co_logo8.png')}}" class="img-responsive zoom-img" alt="合作夥伴 創星 innovative 樂享學"/>
                    </a>
                </div>
                <div class="col-sm-2"></div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/co_logo9.png')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/co_logo9.png')}}" class="img-responsive zoom-img" alt="合作夥伴 創星 innovative 樂享學"/>
                    </a>
                </div>
                <div class="col-sm-2"></div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/co_logo10.png')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/co_logo10.png')}}" class="img-responsive zoom-img" alt="合作夥伴 創星 innovative 樂享學"/>
                    </a>
                </div>
                <div class="col-sm-2"></div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/co_logo11.png')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/co_logo11.png')}}" class="img-responsive zoom-img" alt="合作夥伴 創星 innovative 樂享學"/>
                    </a>
                </div>
                <div class="col-sm-2"></div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/co_logo12.png')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/co_logo12.png')}}" class="img-responsive zoom-img" alt="合作夥伴 創星 innovative 樂享學"/>
                    </a>
                </div>
                <div class="col-sm-2"></div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/co_logo13.png')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/co_logo13.png')}}" class="img-responsive zoom-img" alt="合作夥伴 創星 innovative 樂享學"/>
                    </a>
                </div>
                <div class="col-sm-2"></div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/co_logo14.png')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/co_logo14.png')}}" class="img-responsive zoom-img" alt="合作夥伴 創星 innovative 樂享學"/>
                    </a>
                </div>
                <div class="col-sm-2"></div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/co_logo15.png')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/co_logo15.png')}}" class="img-responsive zoom-img" alt="合作夥伴 創星 innovative 樂享學"/>
                    </a>
                </div>
                <div class="col-sm-2"></div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/co_logo16.png')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/co_logo16.png')}}" class="img-responsive zoom-img" alt="合作夥伴 創星 innovative 樂享學"/>
                    </a>
                </div>
                <div class="col-sm-2"></div>
            </div><!--
            <div class="col-sm-3 col-xs-4 gallery-grids">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/co_logo17.png')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/co_logo17.png')}}" class="img-responsive zoom-img" alt="合作夥伴 創星 innovative 樂享學"/>
                    </a>
                </div>
                <div class="col-sm-2"></div>
            </div>-->
            <div class="col-sm-3 col-xs-4 gallery-grids">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/co_logo18.png')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/co_logo18.png')}}" class="img-responsive zoom-img" alt="合作夥伴 創星 innovative 樂享學"/>
                    </a>
                </div>
                <div class="col-sm-2"></div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/co_logo19.png')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/co_logo19.png')}}" class="img-responsive zoom-img" alt="合作夥伴 創星 innovative 樂享學"/>
                    </a>
                </div>
                <div class="col-sm-2"></div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids">
                <div class="col-sm-2"></div>
                <div class="col-sm-8 w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/co_logo20.png')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/co_logo20.png')}}" class="img-responsive zoom-img" alt="合作夥伴 創星 innovative 樂享學"/>
                    </a>
                </div>
                <div class="col-sm-2"></div>
            </div>
            
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!-- gallery -->
<script src="{{asset('js/innovative/lightbox-plus-jquery.min.js')}}"></script>
@endsection
