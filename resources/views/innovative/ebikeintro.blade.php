@extends('innovative.main')
@section('content')
@include('innovative.script.ebikescript')
<style>
    .pad0{
        padding: 0px 0px;
    }
    .pad15{
        padding: 15px 15px;
    }
    ol li{
        font-size: 19px;
    }
    ul,li{list-style:none;}
    @font-face {
        font-family: 'Glyphicons Halflings';
        src: url('../fonts/glyphicons-halflings-regular.eot');
        src: url('../fonts/glyphicons-halflings-regular.eot?#iefix') format('embedded-opentype'), url('../fonts/glyphicons-halflings-regular.woff2') format('woff2'), url('../fonts/glyphicons-halflings-regular.woff') format('woff'), url('../fonts/glyphicons-halflings-regular.ttf') format('truetype'), url('../fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular') format('svg');
    }
	.container h2,h3,h4,h5{
		font-weight: bold;
	}
    .vv{
        width: 100%;
        padding:5px 5px;
        border-color:#0099ff;
        border-width:3px;
    }
	.myImg{
		opacity: 0.6;
		-webkit-transform: scale(1);
		transform: scale(1);
		-webkit-transition: .5s ease-in-out;
		-moz-transition: .5s ease-in-out;
		-o-transition: .5s ease-in-out;
		transition: .5s ease-in-out;
	}
	.myImg:hover {
		opacity: 1;
		-webkit-transform: scale(1.2);
		transform: scale(1.2);
	}
	figure.snip1104 {
		padding: 0 0;
		margin: 0 0;
		font-family: 'Raleway', Arial, sans-serif;
		position: relative;
		overflow: hidden;
		width: 100%;
		background: #000000;
		color: #ffffff;
		text-align: center;
		box-shadow: 0 0 5px rgba(0, 0, 0, 0.15);
        border-radius:10px;
	}

	figure.snip1104 * {
		-webkit-box-sizing: border-box;
		box-sizing: border-box;
		-webkit-transition: all 0.4s ease-in-out;
		transition: all 0.4s ease-in-out;
	}

	figure.snip1104 img {
		max-width: 100%;
		position: relative;
		opacity: 0.4;
	}

	figure.snip1104 figcaption {
		padding: 0 0;
		position: absolute;
		top: 0;
		left: 0;
		bottom: 0;
		right: 0;
	}

	figure.snip1104 h2 {
		position: absolute;
		left: 40px;
		right: 40px;
		display: inline-block;
		background: #000000;
		-webkit-transform: skew(-10deg) rotate(-10deg) translate(0, -50%);
		transform: skew(-10deg) rotate(-10deg) translate(0, -50%);
		padding: 5px 5px;
		margin: 0 0;
		top: 50%;
		text-transform: uppercase;
		font-weight: 400;
        font-size: 21px;
        padding: 10px 5px;
	}

	figure.snip1104 h2 span {
		font-weight: 800;
	}

	figure.snip1104:before {
		height: 93%;
		width: 100%;
		top: 0;
		left: 0;
		content: '';
		background: #ffffff;
		position: absolute;
		-webkit-transition: all 0.3s ease-in-out;
		transition: all 0.3s ease-in-out;
		-webkit-transform: rotate(110deg) translateY(-50%);
		transform: rotate(110deg) translateY(-50%);
	}

	figure.snip1104 a {
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		position: absolute;
		z-index: 1;
	}
	figure.snip1104:hover img,
	figure.snip1104.hover img {
		opacity: 1;
		-webkit-transform: scale(1.1);
		transform: scale(1.1);
	}

	figure.snip1104:hover h2,
	figure.snip1104.hover h2 {
		-webkit-transform: skew(-10deg) rotate(-10deg) translate(-150%, -50%);
		transform: skew(-10deg) rotate(-10deg) translate(-150%, -50%);
	}

	figure.snip1104:hover:before,
	figure.snip1104.hover:before {
		-webkit-transform: rotate(110deg) translateY(-150%);
		transform: rotate(110deg) translateY(-150%);
	}

    .imageborder{
        border: 2px solid grey;
        border-radius: 10px;
        padding: 10px 10px;
    }
</style>
@if($bike=="1")
<div id="bike4" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>{{trans('in_ebike.bike1')}}</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-6 col-xs-12 gallery-grids">
                <div class="w3ls-hover">
                    <a href="{{asset('images/innovative/ebike13.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_ebike.bike1')}}">
                        <img src="{{asset('images/innovative/ebike13.jpg')}}" class="img-responsive zoom-img" alt="ebike 創星 innovative 樂享學 胖胎電動輔助自行車"/>
                        <div class="view-caption" style="padding:12em 20px;">
                            <h5>{{trans('in_ebike.bike1')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div><br>
                {!!trans('in_ebike.bikerule')!!}
            </div>
        </div>
        <div class="gallery-w3lsrow" valign="middle">
            <div class="col-sm-6 col-xs-12 gallery-grids bs-docs-example ">
                <table class="table table-striped">
                    {!!trans('in_ebike.bike1d')!!}
                </table>
                <h2 style="float:left" class="hideme">
                    <a href="#" onClick="history.back()">
                        <span class="label label-primary">{{trans('in_nk.back')}}</span>
                    </a>
                </h2>
            </div>
        </div>
        <div class="clearfix "> </div>
    </div>
</div>
@endif
@if($bike=="2")
<div id="bike5" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>{!!trans('in_ebike.bike2')!!}</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-6 col-xs-12 gallery-grids">
                <div class="w3ls-hover ">
                    <a href="{{asset('images/innovative/ebike10.jpg')}} " data-lightbox="example-set " data-title="{{trans('in_ebike.bike2')}}">
                        <img src="{{asset('images/innovative/ebike10.jpg')}} " class="img-responsive zoom-img"  alt="ebike 創星 innovative 樂享學 平把電動輔助自行車"/>
                        <div class="view-caption" style="padding:12em 20px;">
                            <h5>{{trans('in_ebike.bike2')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div><br>
                {!!trans('in_ebike.bikerule')!!}
            </div>
        </div>
        <div class="gallery-w3lsrow" valign="middle">
            <div class="col-sm-6 col-xs-12 gallery-grids bs-docs-example ">
                <table class="table table-striped ">
                    {!!trans('in_ebike.bike2d')!!}
                </table>
                <h2 style="float:left" class="hideme">
                    <a href="#" onClick="history.back()">
                        <span class="label label-primary">{{trans('in_nk.back')}}</span>
                    </a>
                </h2>
            </div>
        </div>
        <div class="clearfix "> </div>
    </div>
</div>
@endif
@if($bike=="3")
<div id="bike6" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>{{trans('in_ebike.bike3')}}</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-6 col-xs-12 gallery-grids">
                <div class="w3ls-hover ">
                    <a href="{{asset('images/innovative/ebike12.jpg')}} " data-lightbox="example-set " data-title="{{trans('in_ebike.bike3')}}">
                        <img src="{{asset('images/innovative/ebike12.jpg')}} " class="img-responsive zoom-img" alt="ebike 創星 innovative 樂享學 彎把電動輔助自行車"/>
                        <div class="view-caption" style="padding:12em 20px;">
                            <h5>{{trans('in_ebike.bike3')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div><br>
                {!!trans('in_ebike.bikerule')!!}
            </div>
        </div>
        <div class="gallery-w3lsrow" valign="middle">
            <div class="col-sm-6 col-xs-12 gallery-grids bs-docs-example ">
                <table class="table table-striped ">
                    {!!trans('in_ebike.bike3d')!!}
                </table>
                <h2 style="float:left" class="hideme">
                    <a href="#" onClick="history.back()">
                        <span class="label label-primary">{{trans('in_nk.back')}}</span>
                    </a>
                </h2>
            </div>
        </div>
        <div class="clearfix "> </div>
    </div>
</div>
@endif
@if($bike=="4")
<div id="bike7" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>{!!trans('in_ebike.bike4')!!}</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-6 col-xs-12 gallery-grids">
                <div class="w3ls-hover ">
                    <a href="{{asset('images/innovative/ebike29.jpg')}} " data-lightbox="example-set " data-title="{{trans('in_ebike.bike4')}}">
                        <img src="{{asset('images/innovative/ebike29.jpg')}} " class="img-responsive zoom-img" alt="ebike 創星 innovative 樂享學 K&K胖胎車"/>
                        <div class="view-caption" style="padding:12em 20px;">
                            <h5>{{trans('in_ebike.bike4')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div><br>
                {!!trans('in_ebike.bikerule')!!}
            </div>
        </div>
        <div class="gallery-w3lsrow" valign="middle">
            <div class="col-sm-6 col-xs-12 gallery-grids bs-docs-example ">
                <table class="table table-striped ">
                    {!!trans('in_ebike.bike4d')!!}
                </table>
                <h2 style="float:left" class="hideme">
                    <a href="#" onClick="history.back()">
                        <span class="label label-primary">{{trans('in_nk.back')}}</span>
                    </a>
                </h2>
            </div>
        </div>
        <div class="clearfix "> </div>
    </div>
</div>
@endif
@if($bike=="5")
<div id="bike1" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>{!!trans('in_ebike.bike5')!!}</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-1"></div>
            <div class="col-sm-4 col-xs-12 gallery-grids">
                <div class="w3ls-hover">
                    <a href="{{asset('images/innovative/ebike26.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_ebike.bike5')}}">
                        <img src="{{asset('images/innovative/ebike26.jpg')}}" class="img-responsive zoom-img" alt="ebike 創星 innovative 樂享學 20吋 E-Bike"/>
                        <div class="view-caption" style="padding:7em 20px;">
                            <h5>{{trans('in_ebike.bike5')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div><br>
            </div>
            <div class="col-sm-1"></div>
            <div class=" col-sm-6 col-xs-12 gallery-grids " style="padding:0cm 1cm;">
                {!!trans('in_ebike.bike5d')!!}
                <h2 style="float:left" class="hideme">
                    <a href="#" onClick="history.back()">
                        <span class="label label-primary">{{trans('in_nk.back')}}</span>
                    </a>
                </h2>
                <div class="clearfix "> </div>
            </div>
        </div>
    </div>
</div>
@endif
@if($bike=="6")
<div id="bike2" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3 style="text-transform:lowercase;">{{trans('in_ebike.bike6')}}</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-md-1"></div>
            <div class="col-md-10  gallery-grids row">
                {{-- 切換車輛 --}}
                <div class="col-xs-12">
                    <ul class="nav nav-pills" style="float:right">
                        <li class="active"><a data-toggle="pill" href="#menu1" style="background:none;width:60px;height:60px;"><img src="{{asset('images/innovative/ikin ez i-bike/grey.png')}}" style="width:45px;height:45px;" class="img-circle" title="星辰銀"></a></li>
                        <li><a data-toggle="pill" href="#home" style="background:none;width:60px;height:60px;"><img src="{{asset('images/innovative/ikin ez i-bike/orange.png')}}" style="width:45px;height:45px;" class="img-circle" title="活力橘"></a></li>
                        <li><a data-toggle="pill" href="#menu2" style="background:none;width:60px;height:60px;"><img src="{{asset('images/innovative/ikin ez i-bike/darkblue.png')}}" style="width:45px;height:45px;" class="img-circle" title="閃靛藍"></a></li>
                    </ul><br>
                        
                    <div class="tab-content">
                        <div id="home" class="tab-pane fade row pad15">
                            <div class="col-xs-12 w3ls-hover pad0">
                                <a id="changebikehref1" href="{{asset('images/innovative/ikin ez i-bike/ez01.jpg')}} " data-lightbox="example-set " data-title="{{trans('in_ebike.bike6')}}">
                                    <img id="changebikesrc1" src="{{asset('images/innovative/ikin ez i-bike/ez01.jpg')}} " class="img-responsive zoom-img" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike"/>
                                    <div class="view-caption" style="padding:8em 5em;">
                                        <h5>{{trans('in_ebike.bike6')}}</h5>
                                        <span class="glyphicon glyphicon-search"></span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xs-12 row w3ls-hover gallery pad0">
                                <div class="col-xs-3 hideme pad0">
                                    <a class="btnchangebike01 pad0">
                                        <img src="{{asset('images/innovative/ikin ez i-bike/ez01.jpg')}}" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike">
                                    </a>
                                </div>
                                <div class="col-xs-3 hideme pad0">
                                    <a class="btnchangebike02 pad0">
                                        <img src="{{asset('images/innovative/ikin ez i-bike/ez02.jpg')}}" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div id="menu1" class="tab-pane fade in active row pad15">
                            <div class="col-xs-12 w3ls-hover pad0">
                                <a id="changebikehref2" href="{{asset('images/innovative/ikin ez i-bike/ez11.jpg')}} " data-lightbox="example-set " data-title="{{trans('in_ebike.bike6')}}">
                                    <img id="changebikesrc2" src="{{asset('images/innovative/ikin ez i-bike/ez11.jpg')}} " class="img-responsive zoom-img" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike"/>
                                    <div class="view-caption" style="padding:8em 5em;">
                                        <h5>{{trans('in_ebike.bike6')}}</h5>
                                        <span class="glyphicon glyphicon-search"></span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xs-12 row w3ls-hover gallery pad0">
                                <div class="col-xs-3 hideme pad0">
                                    <a class="btnchangebike11">
                                        <img src="{{asset('images/innovative/ikin ez i-bike/ez11.jpg')}}" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike">
                                    </a>
                                </div>
                                <div class="col-xs-3 hideme pad0">
                                    <a class="btnchangebike12">
                                        <img src="{{asset('images/innovative/ikin ez i-bike/ez12.jpg')}}" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike">
                                    </a>
                                </div>
                                <div class="col-xs-3 hideme pad0">
                                    <a class="btnchangebike13">
                                        <img src="{{asset('images/innovative/ikin ez i-bike/ez13.jpg')}}" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike">
                                    </a>
                                </div>
                                <div class="col-xs-3 hideme pad0">
                                    <a class="btnchangebike14">
                                        <img src="{{asset('images/innovative/ikin ez i-bike/ez14.jpg')}}" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div id="menu2" class="tab-pane fade row pad15">
                            <div class="col-xs-12 w3ls-hover pad0">
                                <a id="changebikehref3" href="{{asset('images/innovative/ikin ez i-bike/ez21.jpg')}} " data-lightbox="example-set " data-title="{{trans('in_ebike.bike6')}}">
                                    <img id="changebikesrc3" src="{{asset('images/innovative/ikin ez i-bike/ez21.jpg')}} " class="img-responsive zoom-img" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike"/>
                                    <div class="view-caption" style="padding:8em 5em;">
                                        <h5>{{trans('in_ebike.bike6')}}</h5>
                                        <span class="glyphicon glyphicon-search"></span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xs-12 row w3ls-hover gallery pad0">
                                <div class="col-xs-3 hideme pad0">
                                    <a class="btnchangebike21">
                                        <img src="{{asset('images/innovative/ikin ez i-bike/ez21.jpg')}}" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike">
                                    </a>
                                </div>
                                <div class="col-xs-3 hideme pad0">
                                    <a class="btnchangebike22">
                                        <img src="{{asset('images/innovative/ikin ez i-bike/ez22.jpg')}}" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div><br>

                {{-- 產品描述 --}}
                <div class="col-sm-12">
                    <h3>ez i-bike產品描述</h3><br>
                    <p>● 6061 鋁合金車架<br>● MPF Drive 中置馬達系統<br>● 馬達配置於車架中間，使騎乘重心平穩、輕鬆靈活。<br>● 利用扭力、速度感應及踩踏頻率，提供不同的動力支援;讓你在平路騎乘順暢、爬坡輕鬆省力。<br>
                        ● 車架可摺疊設計、LED 前燈、前置物籃、後貨架<br>● 輪徑:20”451 輪組<br>● 顏色:星辰銀、活力橘、閃靛藍<br>● 高效鋰電池 400Wh(36V-11.6Ah)<br>● 充電時間:4-6 小時(首次使用建議充滿 6 小時)<br>● 續航力:35-80KM<br>
                        <span style="font-size:14px">*說明:測量狀態為載重 65kg，使用容量 36V/11.6Ah 的全新電池，在車速 18km/h、 風速 2-3 級、常溫 25 度的情況下，且路面平坦胎壓正常。</span>
                    </p><br><br>
                    <h3>MPF 中置馬達系統特點</h3><br>
                    <p>● 最大功率: 385 W<br>● 額定功率: 250 W<br>● 最大扭力: 60 Nm<br>● 時速最高:25 km/h<br>● 電動助力:10 段</p><br>
                    <h3>ez i-bike 車款簡介</h3><br>
                    <div class="container-fluid">
                        <ul class="demo row pb-4">
                            <li class="col-lg-4 col-sm-6 pad15">
                                <div class="gallery-grid1 imageborder wow fadeInUp" data-wow-duration="2s">
                                    <a href="{{asset('images/innovative/ikin ez i-bike/ez32.jpg')}}" data-lightbox="example-set" data-title="中置馬達">
                                        <figure class="snip1104">
                                            <img src="{{asset('images/innovative/ikin ez i-bike/ez32.jpg')}}" title="電動輔助自行車" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike 中置馬達" style="padding:0 0;" />
                                            <figcaption style="padding:0 0;">
                                                <h2>中置馬達<span></span></h2>
                                            </figcaption>
                                        </figure>
                                    </a>		
                                </div>
                            </li>
                            <li class="col-lg-4 col-sm-6 pad15">
                                <div class="gallery-grid1 imageborder wow fadeInUp" data-wow-duration="2s">
                                    <a href="{{asset('images/innovative/ikin ez i-bike/I4US-1.jpg')}}" data-lightbox="example-set" data-title="I4US">
                                        <figure class="snip1104">
                                            <img src="{{asset('images/innovative/ikin ez i-bike/I4US-1.jpg')}}" title="電動輔助自行車" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike 儀表控制系統" style="padding:0 0;" />
                                            <figcaption style="padding:0 0;">
                                                <h2>儀表控制系統<span></span></h2>
                                            </figcaption>
                                        </figure>
                                    </a>		
                                </div>
                            </li>
                            <li class="col-lg-4 col-sm-6 pad15">
                                <div class="gallery-grid1 imageborder wow fadeInUp" data-wow-duration="2s">
                                    <a href="{{asset('images/innovative/ikin ez i-bike/ez33.jpg')}}" data-lightbox="example-set" data-title="大容量鋰電池">
                                        <figure class="snip1104">
                                            <img src="{{asset('images/innovative/ikin ez i-bike/ez33.jpg')}}" title="大容量鋰電池" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike 大容量鋰電池 36V,11.6Ah" style="padding:0 0;" />
                                            <figcaption style="padding:0 0;">
                                                <h2>大容量鋰電池<span>36V,11.6Ah</span></h2>
                                            </figcaption>
                                        </figure>
                                    </a>		
                                </div>
                            </li>
                            <li class="col-lg-4 col-sm-6 pad15">
                                <div class="gallery-grid1 imageborder wow fadeInUp" data-wow-duration="2s">
                                    <a href="{{asset('images/innovative/ikin ez i-bike/ez31.jpg')}}" data-lightbox="example-set" data-title="SHIMANO 內變3速">
                                        <figure class="snip1104">
                                            <img src="{{asset('images/innovative/ikin ez i-bike/ez31.jpg')}}" title="電動輔助自行車" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike SHIMANO 內變3速" style="padding:0 0;"/>
                                            <figcaption style="padding:0 0;">
                                                <h2>SHIMANO<span>內變3速</span></h2>
                                            </figcaption>
                                        </figure>                                
                                    </a>	
                                </div>
                            </li>
                            <li class="col-lg-4 col-sm-6 pad15">
                                <div class="gallery-grid1 imageborder wow fadeInUp" data-wow-duration="2s">
                                    <a href="{{asset('images/innovative/ikin ez i-bike/ez34.jpg')}}" data-lightbox="example-set" data-title="前置物籃">
                                        <figure class="snip1104">
                                            <img src="{{asset('images/innovative/ikin ez i-bike/ez34.jpg')}}" title="電動輔助自行車" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike 前置物籃" />
                                            <figcaption>
                                                <h2>前置物籃<span></span></h2>
                                            </figcaption>
                                        </figure>	
                                    </a>	
                                </div>
                            </li>
                            <li class="col-lg-4 col-sm-6 pad15">
                                <div class="gallery-grid1 imageborder wow fadeInUp" data-wow-duration="2s">
                                    <a href="{{asset('images/innovative/ikin ez i-bike/ez35.jpg')}}" data-lightbox="example-set" data-title="雙眼型LED前燈">
                                        <figure class="snip1104">
                                            <img src="{{asset('images/innovative/ikin ez i-bike/ez35.jpg')}}" title="電動輔助自行車" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike 雙眼型LED前燈" />
                                            <figcaption>
                                                <h2>雙眼型LED前燈<span></span></h2>
                                            </figcaption>
                                        </figure>
                                    </a>		
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div><br>

                {{-- 產品規格 --}}
                <div class="col-sm-12">
                    <h3>產品規格</h3><br>
                    <table class="table table-striped">
                        {!!trans('in_ebike.bike6d')!!}
                    </table>
                    <h2 style="float:left" class="hideme">
                        <a href="#" onClick="history.back()">
                            <span class="label label-primary">{{trans('in_nk.back')}}</span>
                        </a>
                    </h2>
                </div>
                <div class="clearfix"> </div>

            </div>
            <div class="col-md-1"></div>
            <div class="clearfix"></div><br>

        </div>
    </div>
</div>
@endif

<script src="{{asset('js/innovative/lightbox-plus-jquery.min.js')}}"></script>
@endsection
