@extends('innovative.main')
@section('content')
<script src="{{asset('js/innovative/lightbox-plus-jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset("js/innovative/mapdata.js")}}"></script>		
<script type="text/javascript" src="{{asset("js/innovative/countrymap.js")}}"></script>

<!-- Gallery -->
<div id="gallery" class="gallery">
    <div class="container">
        <div class="w3-headings-all">
            <h3>E-Bike Rental Location</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-md-1"></div>
            <div class="col-md-8" id="map"></div>
            <div class="col-md-3"></div>
            <div class="clearfix"></div>

            <!--<div class="col-sm-3 col-xs-4 gallery-grids" >
                <div class="w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/ofami1.jpg')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/ofami1.jpg')}}" class="img-responsive zoom-img img-thumbnail" alt="全省租車 創星 innovative 樂享學 台南永康 tainan yongkang"/>
                        <div class="view-caption">
                            <h5>台南永康</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids" >
                <div class="w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/tainan1.jpg')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/tainan1.jpg')}}" class="img-responsive zoom-img img-thumbnail" alt="全省租車 創星 innovative 樂享學 安平 小遊龍 anping little dragon"/>
                        <div class="view-caption">
                            <h5>台南安平</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids">
                <div class="w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/taipei1.jpg')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/taipei1.jpg')}}" class="img-responsive zoom-img img-thumbnail" alt="全省租車 創星 innovative 樂享學 新北福隆 fulong New Taipei City"/>
                        <div class="view-caption">
                            <h5>新北福隆</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids" >
                <div class="w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/dongshan1.jpg')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/dongshan1.jpg')}}" class="img-responsive zoom-img img-thumbnail" alt="全省租車 創星 innovative 樂享學 冬山車站 ilan dongshan station"/>
                        <div class="view-caption">
                            <h5>宜蘭冬山</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-9 col-xs-12 gallery-grids">
                <div class="w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/taiwan.jpg')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/taiwan.jpg')}}" class="img-responsive zoom-img img-thumbnail" alt="全省租車 創星 innovative 樂享學 單車環島 taiwan bike trail"/>
                        <div class="view-caption">
                            <h5>單車路線</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids">
                <div class="w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/chishang1.jpg')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/chishang1.jpg')}}" class="img-responsive zoom-img img-thumbnail" alt="全省租車 創星 innovative 樂享學 台東 池上 日光租車 taitiung Chishang"/>
                        <div class="view-caption">
                            <h5>台東池上</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids">
                <div class="w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/guanshan2.jpg')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/guanshan2.jpg')}}" class="img-responsive zoom-img img-thumbnail" alt="全省租車 創星 innovative 樂享學 台東 關山 taitung guanshan"/>
                        <div class="view-caption">
                            <h5>台東關山</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids" >
                <div class="w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/guanshan1.jpg')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/guanshan1.jpg')}}" class="img-responsive zoom-img img-thumbnail" alt="全省租車 創星 innovative 樂享學 關山火車站 guanshan station"/>
                        <div class="view-caption">
                            <h5>關山輕旅九號TR9</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids" >
                <div class="w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/dongshan3.jpg')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/dongshan3.jpg')}}" class="img-responsive zoom-img img-thumbnail" alt="全省租車 創星 innovative 樂享學 冬山車站 ilan dongshan station"/>
                        <div class="view-caption">
                            <h5>宜蘭冬山</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids" >
                <div class="w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/ofami2.jpg')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/ofami2.jpg')}}" class="img-responsive zoom-img img-thumbnail" alt="全省租車 創星 innovative 樂享學 台南永康 tainan yongkang"/>
                        <div class="view-caption">
                            <h5>台南永康</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids" >
                <div class="w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/tainan2.jpg')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/tainan2.jpg')}}" class="img-responsive zoom-img img-thumbnail" alt="全省租車 創星 innovative 樂享學 安平 小遊龍 anping little dragon"/>
                        <div class="view-caption">
                            <h5>台南安平</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids">
                <div class="w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/fulong1.jpg')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/fulong1.jpg')}}" class="img-responsive zoom-img img-thumbnail" alt="全省租車 創星 innovative 樂享學 新北福隆 fulong New Taipei City"/>
                        <div class="view-caption">
                            <h5>新北福隆</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids" >
                <div class="w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/dongshan2.jpg')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/dongshan2.jpg')}}" class="img-responsive zoom-img img-thumbnail" alt="全省租車 創星 innovative 樂享學 冬山車站 ilan dongshan station"/>
                        <div class="view-caption">
                            <h5>宜蘭冬山</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids">
                <div class="w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/chishang2.jpg')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/chishang2.jpg')}}" class="img-responsive zoom-img img-thumbnail" alt="全省租車 創星 innovative 樂享學 台東 池上 日光租車 taitiung Chishang"/>
                        <div class="view-caption">
                            <h5>台東池上</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-4 gallery-grids">
                <div class="w3ls-hover" style="padding:15px 15px;">
                    <a href="{{asset('images/innovative/guanshan3.jpg')}}" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/guanshan3.jpg')}}" class="img-responsive zoom-img img-thumbnail" alt="全省租車 創星 innovative 樂享學 台東 關山 taitung guanshan"/>
                        <div class="view-caption">
                            <h5>台東關山</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="clearfix"> </div>
            <div class="container"><br>
                <p style="float:right">圖片來源: 「<a target="_blank" href="http://www.chinatimes.com/newspapers/20150118000297-260114">中時電子報</a>」</p>
            </div>-->
            <!--點選圖片作介紹
            <h4>
                <a href="#" data-toggle="modal" data-target="#myModal11"><img src="{{asset('images/innovative/taitung4.jpg')}}" class="img-responsive zoom-img img-thumbnail"></a>
            </h4>
        -->
        </div>
    </div>
</div>
@endsection