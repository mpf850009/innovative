@extends('innovative.main')
@section('content')
@include('innovative.script.nikimotionscript')
@include('innovative.nikimotionproduct')

<div id="ac" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>{{trans('in_nk.nkac')}}</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.nkac',$locale)}}#nkac1">
                        <img src="{{asset('images/innovative/nkac1.jpg')}}" class="img-responsive zoom-img" alt="創星 樂享學 innovative Accessories Nikimotion" />
                        <div class="view-caption" style="font-size:12px; padding:7em 20px;">
                            <h5>{!!trans('in_nk.acc1t')!!}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>    
            </div>
            <div class="col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.nkac',$locale)}}#afac1">
                        <img src="{{asset('images/innovative/afac1.jpg')}}" class="img-responsive zoom-img" alt="創星 樂享學 innovative Accessories Nikimotion AUTOFOLD" />
                        <div class="view-caption" style="font-size:12px; padding:7em 20px;">
                            <h5>{!!trans('in_nk.acc8t')!!}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.nkac',$locale)}}#afac2">
                        <img src="{{asset('images/innovative/afac2.jpg')}}" class="img-responsive zoom-img" alt="創星 樂享學 innovative Accessories Nikimotion AUTOFOLD" />
                        <div class="view-caption" style="font-size:12px; padding:7em 20px;">
                            <h5>{!!trans('in_nk.acc9t')!!}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.nkac',$locale)}}#afac3">
                        <img src="{{asset('images/innovative/afac3.jpg')}}" class="img-responsive zoom-img" alt="創星 樂享學 innovative Accessories Nikimotion AUTOFOLD" />
                        <div class="view-caption" style="font-size:12px; padding:7em 20px;">
                            <h5>{!!trans('in_nk.acc10t')!!}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.nkac',$locale)}}#blac1">
                        <img src="{{asset('images/innovative/blac1.jpg')}}" class="img-responsive zoom-img" alt="創星 樂享學 innovative Accessories Nikimotion blade" />
                        <div class="view-caption" style="font-size:12px; padding:7em 20px;">
                            <h5>{!!trans('in_nk.acc13t')!!}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <!-- Edited on 2020/04/15-->
            <div class="col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.nkac',$locale)}}#blac4">
                        <img src="{{asset('images/innovative/blac4.jpg')}}" class="img-responsive zoom-img" alt="創星 樂享學 innovative Accessories Nikimotion blade" />
                        <div class="view-caption" style="font-size:12px; padding:7em 20px;">
                            <h5>{!!trans('in_nk.acc16t')!!}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.nkac',$locale)}}#blac5">
                        <img src="{{asset('images/innovative/blac5.png')}}" class="img-responsive zoom-img" alt="創星 樂享學 innovative Accessories Nikimotion blade" />
                        <div class="view-caption" style="font-size:12px; padding:7em 20px;">
                            <h5>{!!trans('in_nk.acc17t')!!}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<script src="{{asset('js/innovative/lightbox-plus-jquery.min.js')}}"></script>
<script src="{{asset('js/innovative/bars.js')}}"></script>
@endsection