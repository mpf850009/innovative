﻿<!DOCTYPE html>
<html>
<head>
	<title>{{$title}}</title>
	<!-- custom-theme -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="創星,嬰兒車,拖車,樂享學,腳踏車,電助腳踏車,咖啡輕食,嬰幼兒,樂享學,ebike,bike,taiwan,innovative,veer,nikimotion,autofold,autofoldlite,easyfolding," />
	<meta property="og:image" content="{{asset('images/innovativelogo1.png')}}" />
	<!--Hover-->
	@include('innovative.css.hover')
	<!-- custom-theme -->
	@include('innovative.css.bootstrap')
	@include('innovative.css.style')
	@include('innovative.css.mainstyle')
	<!-- font-awesome-icons -->
	@include('innovative.css.fontawesome')
	<!--
	<link href="//fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	-->
	<!--網站圖標-->
	<link rel="shortcut icon" href="{{asset('logo.ico')}}" type="image/x-icon" />
	<!--自動回到頂端-->
	<!--
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>-->
	<!--LightBox-->
	@include('innovative.css.lightbox')
	

	<style>
		.hideme{
			opacity:0;
		}
	</style>
</head>
<body>
	@include('innovative.nav')
	@yield('content')
	@include('innovative.footer')

	

	<script src="{{asset('js/innovative/jq-1.7.1.js')}}" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function() {
		/* Every time the window is scrolled ... */
			$(window).scroll( function(){
				/* Check the location of each desired element */
				$('.hideme').each( function(i){
					var bottom_of_object = $(this).offset().top + $(this).outerHeight();
					var bottom_of_window = $(window).scrollTop() + $(window).height();
					/* If the object is completely visible in the window, fade it it */
					if( bottom_of_window > bottom_of_object ){
						$(this).animate({'opacity':'1'},700);
					}
				}); 
			});
		});
	</script>
	

	
	
	
	<!-- js -->
	<script type="text/javascript" src="{{asset('js/innovative/jquery-2.1.4.min.js')}}"></script>
	<!-- start-smooth-scrolling -->
	<script async type="text/javascript">
		jQuery(document).ready(function ($) {
			$(".scroll").click(function (event) {
				event.preventDefault();
				$('html,body').animate({
					scrollTop: $(this.hash).offset().top
				}, 1000);
			});
		});
	</script>

	
	<script async src="{{asset('js/innovative/mainScript.js')}}"></script>
	<script sync src="{{asset('js/innovative/rgbSlide.min.js')}}"></script>
	
	<!--team-->
	<script async type="text/javascript" src="{{asset('js/innovative/jquery.flexisel.js')}}"></script>
	<script type="text/javascript">
		$(window).load(function () {
			$("#flexiselDemo1").flexisel({
				visibleItems: 5,
				itemsToScroll: 4,
				animationSpeed: 800,
				autoPlay: true,
				autoPlaySpeed: 3000,
				pauseOnHover: true,
				enableResponsiveBreakpoints: true,
				responsiveBreakpoints: {
					portrait: {
						changePoint: 480,
						visibleItems: 1,
						itemsToScroll: 1
					},
					landscape: {
						changePoint: 640,
						visibleItems: 2,
						itemsToScroll: 2
					},
					tablet: {
						changePoint: 768,
						visibleItems: 3,
						itemsToScroll: 3
					}
				}
			});
		});
	</script>
	<!-- here ends scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function () {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
			$().UItoTop({
				easingType: 'easeOutQuart'
			});
		});
	</script>
	<a href="#" id="toTop" style="display: block;">
		<span id="toTopHover" style="opacity: 1;"> </span>
	</a>
	<script async src="{{asset('js/innovative/SmoothScroll.min.js')}}"></script>
	<!-- start-smoth-scrolling -->
	<script sync type="text/javascript" src="{{asset('js/innovative/move-top.js')}}"></script>
	<script async type="text/javascript" src="{{asset('js/innovative/easing.js')}}"></script>
	<!-- start-smoth-scrolling -->
	<!-- for bootstrap working -->
	<script async src="{{asset('js/innovative/bootstrap.js')}}"></script>
	<!-- //for bootstrap working -->
	<!-- ResponsiveTabs -->
	<script sync src="{{asset('js/innovative/easyResponsiveTabs.js')}}" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			$('#horizontalTab').easyResponsiveTabs({
				type: 'default', //Types: default, vertical, accordion           
				width: 'auto', //auto or any width like 600px
				fit: true // 100% fit in a container
			});
		});
	</script>

	<!--flexslider-->
	<script src="{{asset('js/innovative/jquery.flexslider.js')}}"></script>
	<script type="text/javascript" charset="utf-8">
	$(window).load(function() {
		$('.flexslider').flexslider({
			animation: "slide",
			controlNav: "thumbnails"
		});
	});
	</script>
	<!--flexslider-->

		
</body>
</html>