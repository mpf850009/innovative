@extends('innovative.main')
@section('content')
<!-- banner -->
	<div class="modal fade" id="myModal2" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<div class="signin-form profile">
						<h3 class="agileinfo_sign">Sign In</h3>
						<div class="login-form">
							<form action="#" method="post">
								<input type="email" name="email" placeholder="E-mail" required="">
								<input type="password" name="password" placeholder="Password" required="">
								<div class="tp">
									<input type="submit" value="Sign In">
								</div>
							</form>
						</div>
						<div class="login-social-grids">
							<ul>
								<li>
									<a href="#">
										<i class="fa fa-facebook"></i>
									</a>
								</li>
								<li>
									<a href="#">
										<i class="fa fa-twitter"></i>
									</a>
								</li>
								<li>
									<a href="#">
										<i class="fa fa-rss"></i>
									</a>
								</li>
							</ul>
						</div>
						<p>
							<a href="#" data-toggle="modal" data-target="#myModal3"> Dont have an account?</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal1 -->
	<!-- about page -->
	<div class="inner_main_agile_section">
		<div class="container">
			<div class="w3-headings-all hideme">
				<h3>{{trans('in_index.aboutus')}}</h3>
			</div>
			<div class="agile_inner_grids">
				<div class="col-md-6 w3_agileits_about_grid_left"><br><br><br>
					{!!trans('in_index.about1')!!}
					<!--
					<ul>
						<li>
							<i class="fa fa-long-arrow-right" aria-hidden="true"></i>Speed deliver</li>
						<li>
							<i class="fa fa-long-arrow-right" aria-hidden="true"></i>Interational clients</li>
						<li>
							<i class="fa fa-long-arrow-right" aria-hidden="true"></i>Fast Response</li>
						<li>
							<i class="fa fa-long-arrow-right" aria-hidden="true"></i>Effective Solutions</li>
					</ul>-->
				</div>
				<div class="col-md-6 w3_agileits_about_grid_right hideme">
					<div class="w3ls-hover">
						<img src="{{asset('images/innovative/logopic1.png')}}" class="img-responsive zoom-img" alt="innovative 創星 樂享學" title="innovative 創星" />
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- about page -->
	<!-- contact map -->
	<div class="map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3671.7499813876384!2d120.25324161433454!3d23.032950421721452!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x346e715cc4bb730b%3A0x4bbcd933c427b0d4!2z5Ym15pifIElubm92YXRpdmU!5e0!3m2!1szh-TW!2stw!4v1528681058904"></iframe>
	</div>
	<!-- contact map -->
	<script src="{{asset('js/innovative/lightbox-plus-jquery.min.js')}}"></script>
@endsection