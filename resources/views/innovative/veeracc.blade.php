@extends('innovative.main')
@section('content')
@include('innovative.script.veerscript')
<link rel="stylesheet" href="{{asset('css/innovative/flexslider.css')}}" type="text/css">
<style>
    .gallery-w3lsrow ol{
        list-style-image:url('images/innovative/veerlogo3.png');
        line-height: 30px;
        font-size: 20px;
    }
</style>

@if($type=="1")
<div id="v1" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>{{trans('in_veer.acc1')}}</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-12 hideme">
                <iframe width="100%" height="500" src="https://www.youtube.com/embed/ntr320rlD48?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="clearfix"></div><br>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-6 col-xs-12 gallery-grids">
                <div class="col-sm-2 col-xs-2"></div>
                <div class="col-sm-8 col-xs-8 hideme flexslider">
                    <ul class="slides">
                        <li data-thumb="{{asset('images/innovative/veeracc11.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc11b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc1')}}">
                                <img src="{{asset('images/innovative/veeracc11a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 foldable storage basket 折疊式置物籃">
                            </a>
                        </li>
                        <li data-thumb="{{asset('images/innovative/veeracc12.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc12b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc1')}}">
                                <img src="{{asset('images/innovative/veeracc12a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 foldable storage basket 折疊式置物籃">
                            </a>
                        </li>
                        <li data-thumb="{{asset('images/innovative/veeracc13.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc13b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc1')}}">
                                <img src="{{asset('images/innovative/veeracc13a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 foldable storage basket 折疊式置物籃">
                            </a>
                        </li>
                        <li data-thumb="{{asset('images/innovative/veeracc14.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc14b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc1')}}">
                                <img src="{{asset('images/innovative/veeracc14a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 foldable storage basket 折疊式置物籃">
                            </a>
                        </li>
                        <li data-thumb="{{asset('images/innovative/veeracc15.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc15b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc1')}}">
                                <img src="{{asset('images/innovative/veeracc15a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 foldable storage basket 折疊式置物籃">
                            </a>
                        </li>
                        <li data-thumb="{{asset('images/innovative/veeracc16.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc16b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc1')}}">
                                <img src="{{asset('images/innovative/veeracc16a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 foldable storage basket 折疊式置物籃">
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-2 col-xs-2"></div>
            </div>
            <div class="col-sm-6 col-xs-12 gallery-grids hideme">
                <div style="padding:1cm 1cm 1cm 1cm;">
                    <h3>{{trans('in_veer.intro')}}</h3><br>
                    <p>{{trans('in_veer.acc1d')}}</p>
                    <br>
                    <h2 style="float:left">
                        <a href="{{route('innovative.veer',$locale)}}#accessories">
                            <span class="label label-primary">&nbsp;{{trans('in_nk.back')}}&nbsp;</span>
                        </a>
                    </h2>
                </div>
            </div>
            <div class="clearfix "> </div>
        </div>
    </div>
</div>
@endif
<!--Edited on 2020/04/15-->
@if($type=="2")
<div id="v2" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>{{trans('in_veer.acc2')}}</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-12 hideme">
                <iframe width="100%" height="500" src="https://www.youtube.com/embed/KqkyJRC78Dk?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="clearfix"></div><br>
        </div>
        <div class="gallery-w3lsrow">
            <div class=" col-sm-6 col-xs-12 gallery-grids row">
                <div class=" col-xs-12 gallery-grids row">
                    <div class="col-sm-3 col-xs-3"></div>
                    <ul class="nav nav-pills" style="float:left">
                        <li class="active" ><a data-toggle="pill" href="#menu3" style="background:none;height:60px;"> <img src="{{asset('images/innovative/color-IceCamoSwatch.png')}}" style="width:45px;height:45px;" class="img-circle" ></a></li>
                        <li><a data-toggle="pill" href="#home1" style="background:none;height:60px;"><img src="{{asset('images/innovative/color-Camo.png')}}" style="width:45px;height:45px;" class="img-circle" ></a></li>
                        <li><a data-toggle="pill" href="#menu4" style="background:none; height:60px;"><img src="{{asset('images/innovative/color-Savanna-White-Icon.png')}}" style="width:45px;height:45px;" class="img-circle" ></a></li>
                        <li><a data-toggle="pill" href="#menu5" style="background:none;height:60px;"><img src="{{asset('images/innovative/color-black.png')}}" style="width:45px;height:45px;" class="img-circle" ></a></li>
                    </ul>
                    <div class="col-sm-3 col-xs-3"></div>
                </div>
                <div class="clearfix"></div><br>
                <div class="col-sm-1"></div>
                <div class=" tab-content">
                    <div id="menu3" class="tab-pane fade in active row pad15">
                        <div class="col-xs-12 w3ls-hover pad0">
                            <a id="vhref4" href="{{asset('images/innovative/veericecamo1b.jpg')}} " data-lightbox="example-set " >
                                <img id="vicon4" src="{{asset('images/innovative/veericecamo1b.jpg')}} " class="img-responsive zoom-img" />
                                <div class="view-caption" style="padding:15em 5em;">
                                    <h5>ICE CAMO</h5>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-12 row w3ls-hover gallery pad0">
                            <div class="col-xs-3 hideme pad0">
                                <a class="v17">
                                    <img src="{{asset('images/innovative/veericecamo1b.jpg')}}" >
                                </a>
                            </div>
                            <div class="col-xs-3 hideme pad0">
                                <a class="v17a">
                                    <img src="{{asset('images/innovative/veericecamo2b.jpg')}}" >
                                </a>
                            </div>
                            <div class="col-xs-3 hideme pad0">
                                <a class="v17b">
                                    <img src="{{asset('images/innovative/veericecamo3b.jpg')}}" >
                                </a>
                            </div>
                        </div>
                    </div>
                    <div id="home1" class="tab-pane fade row pad15">
                        <div class="col-xs-12 w3ls-hover  pad0">
                            <a id="vhref7" href="{{asset('images/innovative/veercamo1.jpg')}} " data-lightbox="example-set " >
                                <img id="vicon7" src="{{asset('images/innovative/veercamo1.jpg')}} " class="img-responsive zoom-img"/>
                                <div class="view-caption" style="padding:15em 5em;">
                                    <h5>CAMO</h5>
                                </div> 
                            </a> 
                        </div>
                        <div class="col-xs-12 row w3ls-hover gallery pad0" >
                            <div class="col-xs-3"></div>
                            <div class="col-xs-3 hideme pad0">
                                <a class="v20 pad0">
                                    <img src="{{asset('images/innovative/veercamo1.jpg')}}">
                                </a>
                            </div>
                            <div class="col-xs-3 hideme pad0">
                                <a class="v20a pad0">
                                    <img src="{{asset('images/innovative/veercamo2.jpg')}}">
                                </a>
                            </div>
                            <div class="col-xs-3"></div>
                        </div>
                    </div>
                    <div id="menu4" class="tab-pane fade row pad15">
                        <div class="col-xs-12 w3ls-hover pad0">
                            <a id="vhref5" href="{{asset('images/innovative/veersavannawhite0b.jpg')}} " data-lightbox="example-set " >
                                <img id="vicon5" src="{{asset('images/innovative/veersavannawhite0b.jpg')}} " class="img-responsive zoom-img" />
                                <div class="view-caption" style="padding:15em 5em;">
                                    <h5>WHITE</h5>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-12 row w3ls-hover gallery pad0">
                            <div class="col-xs-3 hideme pad0">
                                <a class="v18  pad0">
                                    <img src="{{asset('images/innovative/veersavannawhite0b.jpg')}}" >
                                </a>
                            </div>
                            <div class="col-xs-3 hideme pad0">
                                <a class="v18a  pad0">
                                    <img src="{{asset('images/innovative/veersavannawhite1b.jpg')}}" >
                                </a>
                            </div>
                            <div class="col-xs-3 hideme pad0">
                                <a class="v18b  pad0">
                                    <img src="{{asset('images/innovative/veersavannawhite2b.jpg')}}" >
                                </a>
                            </div>
                            <div class="col-xs-3 hideme pad0">
                                <a class="v18c  pad0">
                                    <img src="{{asset('images/innovative/veersavannawhite3b.jpg')}}" >
                                </a>
                            </div>
                        </div>
                    </div>
                    <div id="menu5" class="tab-pane fade row pad15">
                        <div class="col-xs-12 w3ls-hover pad0">
                            <a id="vhref6" href="{{asset('images/innovative/veeracc21b.jpg')}} " data-lightbox="example-set " >
                                <img id="vicon6" src="{{asset('images/innovative/veeracc21b.jpg')}} " class="img-responsive zoom-img" />
                                <div class="view-caption" style="padding:10em 5em;">
                                    <h5>BLACK </h5>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-12 row w3ls-hover gallery pad0">
                            <div class="col-xs-3 hideme pad0">
                                <a class="v19  pad0">
                                    <img src="{{asset('images/innovative/veeracc21b.jpg')}}" >
                                </a>
                            </div>
                            <div class="col-xs-3 hideme pad0">
                                <a class="v19a  pad0">
                                    <img src="{{asset('images/innovative/veeracc22b.jpg')}}" >
                                </a>
                            </div>
                            <div class="col-xs-3 hideme pad0">
                                <a class="v19b  pad0">
                                    <img src="{{asset('images/innovative/veeracc23b.jpg')}}" >
                                </a>
                            </div>
                            <div class="col-xs-3 hideme pad0">
                                <a class="v19c  pad0">
                                    <img src="{{asset('images/innovative/veeracc24b.jpg')}}" >
                                </a>
                            </div>
                            <div class="col-xs-3 hideme pad0">
                                <a class="v19b  pad0">
                                    <img src="{{asset('images/innovative/veeracc25b.jpg')}}" >
                                </a>
                            </div>
                        </div>
                    </div>
                </div>      
            </div>
        </div>
        <div class=" col-sm-6 col-xs-12 gallery-grids row">
            <div style="padding:1cm 1cm 1cm 3cm;">
                <h3>{{trans('in_veer.intro')}}</h3><br>
                <p>{{trans('in_veer.acc2d')}}</p>
                <br>
                <h2 style="float:left">
                    <a href="{{route('innovative.veer',$locale)}}#accessories">
                        <span class="label label-primary">&nbsp;{{trans('in_nk.back')}}&nbsp;</span>
                    </a>
                </h2>
            </div>
        </div>
    </div>
    <div class="col-sm-1"></div>
    <div class="clearfix"></div><br>
</div>         
@endif
@if($type=="3")
<div id="v3" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>{{trans('in_veer.acc3')}}</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-12 hideme">
                <iframe width="100%" height="500" src="https://www.youtube.com/embed/uZ9StNhpGLI?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="clearfix"></div><br>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-6 col-xs-12 gallery-grids">
                <div class="col-sm-2 col-xs-2"></div>
                <div class="col-sm-8 col-xs-8 hideme flexslider">
                    <ul class="slides">
                        <li data-thumb="{{asset('images/innovative/veeracc31.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc31b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc3')}}">
                                <img src="{{asset('images/innovative/veeracc31a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 infant car seat adapter 汽座轉接座">
                            </a>
                        </li>
                        <li data-thumb="{{asset('images/innovative/veeracc32.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc32b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc3')}}">
                                <img src="{{asset('images/innovative/veeracc32a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 infant car seat adapter 汽座轉接座">
                            </a>
                        </li>
                        <li data-thumb="{{asset('images/innovative/veeracc33.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc33b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc3')}}">
                                <img src="{{asset('images/innovative/veeracc33a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 infant car seat adapter 汽座轉接座">
                            </a>
                        </li>
                        <li data-thumb="{{asset('images/innovative/veeracc34.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc34b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc3')}}">
                                <img src="{{asset('images/innovative/veeracc34a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 infant car seat adapter 汽座轉接座">
                            </a>
                        </li>
                        <li data-thumb="{{asset('images/innovative/veeracc35.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc35b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc3')}}">
                                <img src="{{asset('images/innovative/veeracc35a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 infant car seat adapter 汽座轉接座">
                            </a>
                        </li>
                        <li data-thumb="{{asset('images/innovative/veeracc36.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc36b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc3')}}">
                                <img src="{{asset('images/innovative/veeracc36a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 infant car seat adapter 汽座轉接座">
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-2 col-xs-2"></div>
            </div>
            <div class=" col-sm-6 col-xs-12 gallery-grids hideme">
                <div style="padding:1cm 1cm 1cm 1cm;">
                    <h3>{{trans('in_veer.intro')}}</h3><br>
                    <p>{{trans('in_veer.acc3d')}}</p>
                    <br>
                    <h2 style="float:left">
                        <a href="{{route('innovative.veer',$locale)}}#accessories">
                            <span class="label label-primary">&nbsp;{{trans('in_nk.back')}}&nbsp;</span>
                        </a>
                    </h2>
                </div>
            </div>
            <div class="clearfix "> </div>
        </div>
    </div>
</div>
@endif

@if($type=="4")
<div id="v4" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>{{trans('in_veer.acc4')}}</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-12 hideme">
                <iframe width="100%" height="500" src="https://www.youtube.com/embed/cTY_LfBjjkI?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="clearfix"></div><br>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-6 col-xs-12 gallery-grids">
                <div class="col-sm-2 col-xs-2"></div>
                <div class="col-sm-8 col-xs-8 hideme flexslider">
                    <ul class="slides">
                        <li data-thumb="{{asset('images/innovative/veeracc41.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc41b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc4')}}">
                                <img src="{{asset('images/innovative/veeracc41a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 travel bag 旅行袋">
                            </a>
                        </li>
                        <li data-thumb="{{asset('images/innovative/veeracc42.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc42b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc4')}}">
                                <img src="{{asset('images/innovative/veeracc42a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 travel bag 旅行袋">
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-2 col-xs-2"></div>
            </div>
            <div class="col-sm-6 col-xs-12 gallery-grids hideme">
                <div style="padding:1cm 1cm 1cm 1cm;">
                    <h3>{{trans('in_veer.intro')}}</h3><br>
                    <p>{{trans('in_veer.acc4d')}}</p>
                    <br>
                    <h2 style="float:left">
                        <a href="{{route('innovative.veer',$locale)}}#accessories">
                            <span class="label label-primary">&nbsp;{{trans('in_nk.back')}}&nbsp;</span>
                        </a>
                    </h2>
                </div>
            </div>
            <div class="clearfix "> </div>
        </div>
    </div>
</div>
@endif

@if($type=="5")
<div id="v5" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>{{trans('in_veer.acc5')}}</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-6 col-xs-12 gallery-grids">
                <div class="col-sm-2 col-xs-2"></div>
                <div class="col-sm-8 col-xs-8 hideme flexslider">
                    <ul class="slides">
                        <li data-thumb="{{asset('images/innovative/veeracc51.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc51b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc5')}}">
                                <img src="{{asset('images/innovative/veeracc51a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 cup holders 水杯架">
                            </a>
                        </li>
                        <li data-thumb="{{asset('images/innovative/veeracc52.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc52b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc5')}}">
                                <img src="{{asset('images/innovative/veeracc52a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 cup holders 水杯架">
                            </a>
                        </li>
                        <li data-thumb="{{asset('images/innovative/veeracc53.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc53b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc5')}}">
                                <img src="{{asset('images/innovative/veeracc53a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 cup holders 水杯架">
                            </a>
                        </li>
                        <li data-thumb="{{asset('images/innovative/veeracc54.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc54b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc5')}}">
                                <img src="{{asset('images/innovative/veeracc54a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 cup holders 水杯架">
                            </a>
                        </li>
                        <li data-thumb="{{asset('images/innovative/veeracc55.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc55b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc5')}}">
                                <img src="{{asset('images/innovative/veeracc55a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 cup holders 水杯架">
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-2 col-xs-2"></div>
            </div>
            <div class="col-sm-6 col-xs-12 gallery-grids hideme">
                <div style="padding:1cm 1cm 1cm 1cm;">
                    <h3>{{trans('in_veer.intro')}}</h3><br>
                    <p>{{trans('in_veer.acc5d')}}</p>
                    <br>
                    <h2 style="float:left">
                        <a href="{{route('innovative.veer',$locale)}}#accessories">
                            <span class="label label-primary">&nbsp;{{trans('in_nk.back')}}&nbsp;</span>
                        </a>
                    </h2>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
@endif

@if($type=='6')
<div id="v6" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>{{trans('in_veer.acc6')}}</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-6 col-xs-12 gallery-grids">
                <div class="col-sm-2 col-xs-2"></div>
                <div class="col-sm-8 col-xs-8 hideme flexslider">
                    <div class="w3ls-hover ">
                        <a id="nphref" href="{{asset('images/innovative/goveer10.jpg')}}" data-lightbox="example-set" data-title="{{trans('in_veer.acc6')}}">
                            <img id="npicon" src="{{asset('images/innovative/goveer10.jpg')}} " class="img-responsive zoom-img" alt="veer 創星 innovative 樂享學 nap system"/>
                        </a>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-2"></div>
            </div>
            <div class=" col-sm-6 col-xs-12 gallery-grids hideme">
                <div style="padding:0.5cm 0.5cm 0.5cm 0.5cm;">
                    <h3>{{trans('in_veer.intro')}}</h3><br>
                    <p>{{trans('in_veer.acc6d')}}</p>
                    <br>
                    <h2 style="float:left">
                        <a href="{{route('innovative.veer',$locale)}}#accessories">
                            <span class="label label-primary">&nbsp;{{trans('in_nk.back')}}&nbsp;</span>
                        </a>
                    </h2>
                </div>
            </div>
            <div class="clearfix "> </div>
        </div>
    </div>
</div>
@endif

@if($type=="7")
<div id="v7" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>{{trans('in_veer.acc7')}}</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-12 hideme">
                <iframe width="100%" height="500" src="https://www.youtube.com/embed/IrehdLbc0mY?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="clearfix"></div><br>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-6 col-xs-12 gallery-grids">
                <div class="col-sm-2 col-xs-2"></div>
                <div class="col-sm-8 col-xs-8 hideme flexslider">
                    <ul class="slides">
                        <li data-thumb="{{asset('images/innovative/veeracc61.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc61b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc7')}}">
                                <img src="{{asset('images/innovative/veeracc61a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 comfort seat 軟坐墊">
                            </a>
                        </li>
                        <li data-thumb="{{asset('images/innovative/veeracc62.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc62b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc7')}}">
                                <img src="{{asset('images/innovative/veeracc62a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 comfort seat 軟坐墊">
                            </a>
                        </li>
                        <li data-thumb="{{asset('images/innovative/veeracc63.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc63b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc7')}}">
                                <img src="{{asset('images/innovative/veeracc63a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 comfort seat 軟坐墊">
                            </a>
                        </li>
                        <li data-thumb="{{asset('images/innovative/veeracc64.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc64b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc7')}}">
                                <img src="{{asset('images/innovative/veeracc64a.jpg')}}" class="img-responsive" alt="veer 創星 innovative 樂享學 comfort seat 軟坐墊">
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-2 col-xs-2"></div>
            </div>
            <div class=" col-sm-6 col-xs-12 gallery-grids hideme">
                <div style="padding:1cm 1cm 1cm 1cm;">
                    <h3>{{trans('in_veer.intro')}}</h3><br>
                    <p>{{trans('in_veer.acc7d')}}</p>
                    <br>
                    <h2 style="float:left">
                        <a href="{{route('innovative.veer',$locale)}}#accessories">
                            <span class="label label-primary">&nbsp;{{trans('in_nk.back')}}&nbsp;</span>
                        </a>
                    </h2>
                </div>
            </div>
            <div class="clearfix "> </div>
        </div>
    </div>
</div>
@endif

@if($type=='8')
<div id="v8" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>{{trans('in_veer.acc8')}}</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-6 col-xs-12 gallery-grids">
                <div class="col-sm-2 col-xs-2"></div>
                <div class="col-sm-8 col-xs-8 hideme flexslider">
                    <div class="w3ls-hover ">
                        <a href="{{asset('images/innovative/goveer9.jpg')}} " data-lightbox="example-set " data-title="{{trans('in_veer.acc8')}}">
                            <img src="{{asset('images/innovative/goveer9.jpg')}} " class="img-responsive zoom-img" alt="veer 創星 innovative 樂享學 drink Snack 餐盤"/>
                        </a>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-2"></div>
            </div>
            <div class=" col-sm-6 col-xs-12 gallery-grids hideme">
                <div style="padding:1cm 1cm 1cm 1cm;">
                    <h3>{{trans('in_veer.intro')}}</h3><br>
                    <p>{{trans('in_veer.acc8d')}}</p>
                    <br>
                    <h2 style="float:left">
                        <a href="{{route('innovative.veer',$locale)}}#accessories">
                            <span class="label label-primary">&nbsp;{{trans('in_nk.back')}}&nbsp;</span>
                        </a>
                    </h2>
                </div>
            </div>
            <div class="clearfix "> </div>
        </div>
    </div>
</div>
@endif
@if($type=='9')
<div id="v9" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>{{trans('in_veer.acc9')}}</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-12 hideme">
                <iframe width="100%" height="500" src="https://www.youtube.com/embed/rAjRwXwWRFA?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="clearfix"></div><br>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-6 col-xs-12 gallery-grids">
                <div class="col-sm-2 col-xs-2"></div>
                <div class="col-sm-8 col-xs-8 hideme flexslider">
                    <ul class="slides">
                        <li data-thumb="{{asset('images/innovative/veeracc71.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc71b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc9')}}">
                                <img src="{{asset('images/innovative/veeracc71a.jpg')}}" class="img-responsive" >
                            </a>
                        </li>
                        <li data-thumb="{{asset('images/innovative/veeracc72.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc72b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc9')}}">
                                <img src="{{asset('images/innovative/veeracc72a.jpg')}}" class="img-responsive" >
                            </a>
                        </li>
                        <li data-thumb="{{asset('images/innovative/veeracc73.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc73b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc9')}}">
                                <img src="{{asset('images/innovative/veeracc73a.jpg')}}" class="img-responsive" >
                            </a>
                        </li>
                        <li data-thumb="{{asset('images/innovative/veeracc74.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc74b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc9')}}">
                                <img src="{{asset('images/innovative/veeracc74a.jpg')}}" class="img-responsive" >
                            </a>
                        </li>
                        <li data-thumb="{{asset('images/innovative/veeracc75.jpg')}}">
                            <a href="{{asset('images/innovative/veeracc75b.jpg')}}" data-lightbox="example-set " data-title="{{trans('in_veer.acc9')}}">
                                <img src="{{asset('images/innovative/veeracc75a.jpg')}}" class="img-responsive" >
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-2 col-xs-2"></div>
            </div>
            <div class=" col-sm-6 col-xs-12 gallery-grids hideme">
                <div style="padding:1cm 1cm 1cm 1cm;">
                    <h3>{{trans('in_veer.intro')}}</h3><br>
                    <p>{{trans('in_veer.acc9d')}}</p>
                    <br>
                    <h2 style="float:left">
                        <a href="{{route('innovative.veer',$locale)}}#accessories">
                            <span class="label label-primary">&nbsp;{{trans('in_nk.back')}}&nbsp;</span>
                        </a>
                    </h2>
                </div>
            </div>
            <div class="clearfix "> </div>
        </div>
    </div>
</div>
@endif
@if($type=='10')
<div id="camo" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>側護欄組</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-12 hideme">
                <iframe width="100%" height="500" src="https://www.youtube.com/embed/ATDwHTY9bcY?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="clearfix"></div><br>
        </div>
        <div class="gallery-w3lsrow">
            <div class=" col-sm-6 col-xs-12 gallery-grids row">
                <div class=" col-xs-12 gallery-grids row">
                    <div class="col-sm-4 col-xs-4"></div>
                    <ul class="nav nav-pills" style="float:left">
                        <li class="active" ><a data-toggle="pill" href="#menu1" style="background:none;height:60px;"> <img src="{{asset('images/innovative/color-Camo.png')}}" style="width:45px;height:45px;" class="img-circle" ></a></li>
                        <li><a data-toggle="pill" href="#home" style="background:none;height:60px;"><img src="{{asset('images/innovative/color-IceCamoSwatch.png')}}" style="width:45px;height:45px;" class="img-circle" ></a></li>
                        <li><a data-toggle="pill" href="#menu2" style="background:none; height:60px;"><img src="{{asset('images/innovative/color-Savanna-White-Icon.png')}}" style="width:45px;height:45px;" class="img-circle" ></a></li>
                    </ul>
                    <div class="col-sm-4 col-xs-4"></div>
                </div>
                <div class="clearfix"></div><br>
                <div class="col-sm-1"></div>
                <div class=" tab-content">
                    <div id="menu1" class="tab-pane fade in active row pad15">
                        <div class="col-xs-12 w3ls-hover pad0">
                            <a id="vhref2" href="{{asset('images/innovative/veeracc81b.jpg')}} " data-lightbox="example-set " >
                                <img id="vicon2" src="{{asset('images/innovative/veeracc81b.jpg')}} " class="img-responsive zoom-img" />
                                <div class="view-caption" style="padding:15em 5em;">
                                    <h5>CAMO</h5>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-12 row w3ls-hover gallery pad0">
                            <div class="col-xs-3"></div>
                            <div class="col-xs-3 hideme pad0">
                                <a class="v15">
                                    <img src="{{asset('images/innovative/veeracc81b.jpg')}}" >
                                </a>
                            </div>
                            <div class="col-xs-3 hideme pad0">
                                <a class="v15b">
                                    <img src="{{asset('images/innovative/veeracc82b.jpg')}}" >
                                </a>
                            </div>
                            <div class="col-xs-3"></div>
                        </div>
                    </div>
                    <div id="home" class="tab-pane fade row pad15">
                        <div class="col-xs-12 w3ls-hover  pad0">
                            <a id="vhref" href="{{asset('images/innovative/veeracc91b.jpg')}} " data-lightbox="example-set " >
                                <img id="vicon" src="{{asset('images/innovative/veeracc91b.jpg')}} " class="img-responsive zoom-img"/>
                                <div class="view-caption" style="padding:15em 5em;">
                                    <h5>ICE-CAMO</h5>
                                </div> 
                            </a> 
                        </div>
                        <div class="col-xs-12 row w3ls-hover gallery pad0" >
                            <div class="col-xs-3"></div>
                            <div class="col-xs-3 hideme pad0">
                                <a class="v13 pad0">
                                    <img src="{{asset('images/innovative/veeracc91b.jpg')}}">
                                </a>
                            </div>
                            <div class="col-xs-3 hideme pad0">
                                <a class="v13a pad0">
                                    <img src="{{asset('images/innovative/veeracc92b.jpg')}}">
                                </a>
                            </div>
                            <div class="col-xs-3"></div>
                        </div>
                    </div>
                    <div id="menu2" class="tab-pane fade row pad15">
                        <div class="col-xs-12 w3ls-hover pad0">
                            <a id="vhref1" href="{{asset('images/innovative/veeracc100b.jpg')}} " data-lightbox="example-set " >
                                <img id="vicon1" src="{{asset('images/innovative/veeracc100b.jpg')}} " class="img-responsive zoom-img" />
                                <div class="view-caption" style="padding:15em 5em;">
                                    <h5>WHITE</h5>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-12 row w3ls-hover gallery pad0">
                            <div class="col-xs-3 hideme pad0">
                                <a class="v14">
                                    <img src="{{asset('images/innovative/veeracc100b.jpg')}}" >
                                </a>
                            </div>
                            <div class="col-xs-3 hideme pad0">
                                <a class="v14a">
                                    <img src="{{asset('images/innovative/veeracc101b.jpg')}}" >
                                </a>
                            </div>
                            <div class="col-xs-3 hideme pad0">
                                <a class="v14b">
                                    <img src="{{asset('images/innovative/veeracc102b.jpg')}}" >
                                </a>
                            </div>
                            <div class="col-xs-3 hideme pad0">
                                <a class="v14c">
                                    <img src="{{asset('images/innovative/veeracc104b.png')}}" >
                                </a>
                            </div>
                        </div>
                    </div>
                </div>      
            </div>
        </div>
        <div class=" col-sm-6 col-xs-12 gallery-grids row">
            <div style="padding:1cm 1cm 1cm 3cm;">
                <h3>{{trans('in_veer.intro')}}</h3><br>
                <p>{{trans('in_veer.acc10d')}}</p>
                <br>
                <h2 style="float:left">
                    <a href="{{route('innovative.veer',$locale)}}#accessories">
                        <span class="label label-primary">&nbsp;{{trans('in_nk.back')}}&nbsp;</span>
                    </a>
                </h2>
            </div>
        </div>
    </div>
    <div class="col-sm-1"></div>
    <div class="clearfix"></div></br>
</div>  
@endif
<script src="{{asset('js/innovative/lightbox-plus-jquery.min.js')}}"></script>
@endsection