@extends('innovative.main')
@section('content')
@include('innovative.script.nikimotionscript')
<link href="https://fonts.googleapis.com/css?family=Montserrat:300" rel="stylesheet">
<style>
.font p{
    font-family: 'Montserrat', sans-serif,Microsoft JhengHei;
    font-size: 18px;
}
.font h3{
    font-family: 'Montserrat', sans-serif,Microsoft JhengHei;
    font-size: 35px;
}
.font li{
    font-family: 'Montserrat', sans-serif,Microsoft JhengHei;
    font-size: 18px;
} 
.font strong{
    font-family: 'Montserrat', sans-serif,Microsoft JhengHei;
    font-size: 22px;
} 
.container ul{
    list-style-image:url('images/innovative/nklogo3.png');
}
</style>

<div id="autofoldlite" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>AutoFold Lite {{trans('in_nk.fc')}}</h3>
        </div>
        <div class="col-sm-6 col-xs-12">
            <div class="col-sm-3 col-xs-3"></div>
            <div class="col-sm-2 col-xs-2 hideme">
                <a class="color1" title="color-Green">
                    <img src="{{asset('images/innovative/color-green.png')}}" class="img-circle" width="40px" height="40px">
                </a>
            </div>
            <div class="col-sm-2 col-xs-2 hideme">
                <a class="color2" title="color-Red">
                    <img src="{{asset('images/innovative/color-red.jpg')}}" class="img-circle" width="40px" height="40px">
                </a>
            </div>
            <div class="col-sm-2 col-xs-2 hideme">
                <a class="color3" title="color-Black" >
                    <img src="{{asset('images/innovative/color-black.png')}}" class="img-circle" width="40px" height="40px">
                </a>
            </div>
            <div class="col-sm-3 col-xs-3"></div>
            <div class="clearfix"></div><br><br>
            <div class="col-sm-1"></div>
            <div class="col-sm-10 hideme">
                <div class="w3ls-hover">
                    <a href="{{asset('images/innovative/aflgif1.gif')}}" id="aflhref" data-lightbox="example-set" data-title="">
                        <img src="{{asset('images/innovative/aflgif1.gif')}}" class="img-responsive zoom-img" id="aflicon" alt="創星 樂享學 innovative autofold lite Nikimotion" style="padding:2em 2em;"/>
                        <div class="view-caption" style="padding:10em 20px;">
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1"></div>
            <div class="clearfix"></div><br>
        </div>
        <div class="col-sm-6 col-xs-12 font" style="padding:0px 50px;">
            <h3 class="hideme">AUTOFOLD Lite</h3><br>
            {!!trans('in_nk.dc2')!!}
        </div>
    </div>
</div>
<div id="aflac" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>AUTOFOLD Lite {{trans('in_nk.ac')}}</h3>
        </div>
        <div class="gallery-w3lsrow">
            <div class="col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.nkac',$locale)}}#nkac1">
                        <img src="{{asset('images/innovative/nkac1.jpg')}}" class="img-responsive zoom-img" alt="創星 樂享學 innovative Accessories Nikimotion" />
                        <div class="view-caption" style="font-size:12px; padding:7em 20px;">
                            <h5>{{trans('in_nk.acc1t')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.nkac',$locale)}}#afac1">
                        <img src="{{asset('images/innovative/afac1.jpg')}}" class="img-responsive zoom-img" alt="創星 樂享學 innovative Accessories Nikimotion AUTOFOLD" />
                        <div class="view-caption" style="font-size:12px; padding:7em 20px;">
                            <h5>{!!trans('in_nk.acc8t')!!}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.nkac',$locale)}}#afac2">
                        <img src="{{asset('images/innovative/afac2.jpg')}}" class="img-responsive zoom-img" alt="創星 樂享學 innovative Accessories Nikimotion AUTOFOLD" />
                        <div class="view-caption" style="font-size:12px; padding:7em 20px;">
                            <h5>{{trans('in_nk.acc9t')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.nkac',$locale)}}#afac3">
                        <img src="{{asset('images/innovative/afac3.jpg')}}" class="img-responsive zoom-img" alt="創星 樂享學 innovative Accessories Nikimotion AUTOFOLD" />
                        <div class="view-caption" style="font-size:12px; padding:7em 20px;">
                            <h5>{{trans('in_nk.acc10t')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.nkac',$locale)}}#blac4">
                        <img src="{{asset('images/innovative/blac4.jpg')}}" class="img-responsive zoom-img" alt="創星 樂享學 innovative Accessories Nikimotion AUTOFOLD" />
                        <div class="view-caption" style="font-size:12px; padding:7em 20px;">
                            <h5>{{trans('in_nk.acc16t')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 gallery-grids hideme">
                <div class="w3ls-hover">
                    <a href="{{route('innovative.nkac',$locale)}}#blac5">
                        <img src="{{asset('images/innovative/blac5.png')}}" class="img-responsive zoom-img" alt="創星 樂享學 innovative Accessories Nikimotion AUTOFOLD" />
                        <div class="view-caption" style="font-size:12px; padding:7em 20px;">
                            <h5>{{trans('in_nk.acc17t')}}</h5>
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="gallery">
    <div class="container">
        <h3>
            <a href="{{route('innovative.nikimotion',$locale)}}#production">
                <span class="label label-primary">&nbsp;{{trans('in_nk.back')}}&nbsp;</span>
            </a>
        </h3>
    </div>
</div>

<script src="{{asset('js/innovative/lightbox-plus-jquery.min.js')}}"></script>
<script src="{{asset('js//innovative/bars.js')}}"></script>
@endsection