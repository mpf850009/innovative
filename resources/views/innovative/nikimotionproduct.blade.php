 <!--Edited on 2020/04/23-->
 <style>
    .w3ls-hover h4 {
    position: absolute;
    bottom: 0;
    color: #fff;
    background-color: rgba(0, 0, 0, 0.5);
    width:100%;
    font-size: 2em;
    padding: 0.5em;
    text-align: center;
}
</style>
<div id="production" class="gallery">
    <div class="container">
        <div class="w3-headings-all hideme">
            <h3>{{trans('in_nk.p')}}</h3>
        </div>
        <div class="gallery-w3lsrow">  
            <div class="col-sm-4 col-xs-12 gallery-grids hideme">
                <!--
                <div class="w3-headings-all">
                    <h3 style="font-size:1.5em;">autofold Lite</h3>
                </div>-->
                <div class="w3ls-hover">
                    <a href="{{route('innovative.autofoldlite',$locale)}}">
                        <!--edited on 2020/04/23-->
                        <img src="{{asset('images/innovative/nk12.jpg')}}" class="img-responsive zoom-img" alt="創星 樂享學 innovative autofold lite Nikimotion"/>
                        <h4>AUTOFOLD LITE</h4>
                        <div class="view-caption" style="background-image: url('{{asset('images/innovative/afl5.jpg')}}');background-repeat: no-repeat; background-size: contain;" >
                        </div>
                    </a>
                </div>
            </div>

            <div class=" col-sm-4 col-xs-12 gallery-grids hideme">
                <!--
                <div class="w3-headings-all">
                    <h3 style="font-size:1.5em;">autofold</h3>
                </div>-->
                <div class="w3ls-hover">
                    <a href="{{route('innovative.autofold',$locale)}}">
                        <!--edited on 2020/04/23 -->
                        <img src="{{asset('images/innovative/nk13.jpg')}}" class="img-responsive zoom-img" alt="創星 樂享學 innovative autofold Nikimotion"/>
                        <h4>AUTOFOLD</h4>
                        <div class="view-caption" style=" background-image: url('{{asset('images/innovative/afl6.jpg')}}'); background-repeat: no-repeat; background-size: contain;">
                        </div>
                    </a>
                </div>
            </div>      
            
            <div class=" col-sm-4 col-xs-12 gallery-grids hideme">
                <!--
                <div class="w3-headings-all">
                    <h3 style="font-size:1.5em;">blade</h3>
                </div>-->
                <div class="w3ls-hover">
                    <a href="{{route('innovative.blade',$locale)}}">
                        <!--edited on 2020/04/23-->
                        <img src="{{asset('images/innovative/nk14.jpg')}}" class="img-responsive zoom-img" alt="創星 樂享學 innovative autofold Nikimotion"/>
                        <h4>BLADE</h4>
                        <div class="view-caption" style="background-image: url('{{asset('images/innovative/afl7.jpg')}}');background-repeat: no-repeat; background-size: contain;">
                        </div>
                    </a>
                </div>
            </div>
            <div class="clearfix "> </div>
        </div>
    </div>
</div>