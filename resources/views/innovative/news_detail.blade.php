@extends('innovative.main')
@section('content')
<style>
	.img{
		padding: 15px 0px 15px 0px;
    }
    .link{
		font-weight: bold;
		padding: 10px 10px;
		font-size: 20px;
		background-color: lightgray;
	}
</style>

@if($date=="0427")
<div class="inner_main_agile_section">
	<div class="container">
		<div class="w3-headings-all">
			<h3>{{trans('in_index.open6')}}</h3>
		</div>
		<div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
				<h3>20180427 {{trans('in_index.open6')}}</h3>
				<p></p><br>
				<div class="gallery-w3lsrow">
					<div class="gallery-grids">
						<div class="w3ls-hover">
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_fl1.jpg')}}" alt="創星 樂享學 innovative 台北 福隆驛站 fulong Taipei 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_fl2.jpg')}}" alt="創星 樂享學 innovative 台北 福隆驛站 fulong Taipei 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_fl4.jpg')}}" alt="創星 樂享學 innovative 台北 福隆驛站 fulong Taipei 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_fl5.jpg')}}" alt="創星 樂享學 innovative 台北 福隆驛站 fulong Taipei 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_fl3.jpg')}}" alt="創星 樂享學 innovative 台北 福隆驛站 fulong Taipei 電動輔助自行車 租車 腳踏車" class="img">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
        </div>
        <br>
        <h2 style="float:left" class="hideme">
            <a href="#" onClick="history.back()">
                <span class="label label-primary">{{trans('in_nk.back')}}</span>
            </a>
        </h2>
	</div>
</div>
@endif

@if($date=='0601')
<div class="inner_main_agile_section">
	<div class="container">
		<div class="w3-headings-all">
			<h3>{{trans('in_index.open5')}}</h3>
		</div>
		<div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
				<h3>20180601 {{trans('in_index.open5')}}</h3>
				<p></p><br>
				<div class="gallery-w3lsrow">
					<div class="gallery-grids">
						<div class="w3ls-hover">
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_cs1.jpg')}}" alt="創星 樂享學 innovative 台東 池上 Taitung Chishang 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_cs2.jpg')}}" alt="創星 樂享學 innovative 台東 池上 Taitung Chishang 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_cs3.jpg')}}" alt="創星 樂享學 innovative 台東 池上 Taitung Chishang 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_cs4.jpg')}}" alt="創星 樂享學 innovative 台東 池上 Taitung Chishang 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_cs6.jpg')}}" alt="創星 樂享學 innovative 台東 池上 Taitung Chishang 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_cs5.jpg')}}" alt="創星 樂享學 innovative 台東 池上 Taitung Chishang 電動輔助自行車 租車 腳踏車" class="img">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
        <br>
        <h2 style="float:left" class="hideme">
            <a href="#" onClick="history.back()">
                <span class="label label-primary">{{trans('in_nk.back')}}</span>
            </a>
        </h2>
	</div>
</div>
@endif

@if($date=='0617')
<div class="inner_main_agile_section">
	<div class="container">
		<div class="w3-headings-all">
			<h3>{{trans('in_index.open3')}}</h3>
		</div>
		<div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
				<h3>20180617 {{trans('in_index.open3')}}</h3>
				<p></p><br>
				<div class="gallery-w3lsrow">
					<div class="gallery-grids">
						<div class="w3ls-hover">
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_in1.jpg')}}" alt="創星 樂享學 innovative 台南 永康 Tainan Yongkang 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_in7.jpg')}}" alt="創星 樂享學 innovative 台南 永康 Tainan Yongkang 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_in3.jpg')}}" alt="創星 樂享學 innovative 台南 永康 Tainan Yongkang 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_in4.jpg')}}" alt="創星 樂享學 innovative 台南 永康 Tainan Yongkang 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_in5.jpg')}}" alt="創星 樂享學 innovative 台南 永康 Tainan Yongkang 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_in6.jpg')}}" alt="創星 樂享學 innovative 台南 永康 Tainan Yongkang 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_in2.jpg')}}" alt="創星 樂享學 innovative 台南 永康 Tainan Yongkang 電動輔助自行車 租車 腳踏車" class="img">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
        <br>
        <h2 style="float:left" class="hideme">
            <a href="#" onClick="history.back()">
                <span class="label label-primary">{{trans('in_nk.back')}}</span>
            </a>
        </h2>
	</div>
</div>
@endif

@if($date=='0911')
<div class="inner_main_agile_section">
	<div class="container">
		<div class="w3-headings-all">
			<h3>{{trans('in_index.open8')}}</h3>
		</div>
		<div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
                <h3>20180911 {{trans('in_index.open8')}}</h3>
                <p></p><br>
				<div class="gallery-w3lsrow">
					<div class="gallery-grids">
						<div class="w3ls-hover">
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_yuli2.jpg')}}" alt="創星 樂享學 innovative 花蓮 玉里 Hualien Yuli 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_yuli1.jpg')}}" alt="創星 樂享學 innovative 花蓮 玉里 Hualien Yuli 電動輔助自行車 租車 腳踏車" class="img">
                            </div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
        <br>
        <h2 style="float:left" class="hideme">
            <a href="#" onClick="history.back()">
                <span class="label label-primary">{{trans('in_nk.back')}}</span>
            </a>
        </h2>
	</div>
</div>
@endif

@if($date=='1030')
<div class="inner_main_agile_section">
	<div class="container">
		<div class="w3-headings-all">
			<h3>{{trans('in_index.open7')}}</h3>
		</div>
		<div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
				<h3>
					<a class="hvr-icon-wobble-horizontal" href="https://www.taipeicycle.com.tw/zh_TW/index.html"> 10/31 - 11/3 , 2018 {{trans('in_index.open7')}} <br>{{trans('in_index.ngeh')}}
					</a>
				</h3>
				<p></p><br>
				<div class="gallery-w3lsrow">
					<div class="gallery-grids">
						<div class="w3ls-hover">
							<div class="col-sm-12 col-xs-12">
								<img src="{{asset('images/innovative/tcarea2.png')}}" alt="創星 樂享學 innovative 南港展覽館 台北國際自行車展覽會 Taipei Cycle 電動輔助自行車 腳踏車" class="img">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
        </div><br>
        <h2 style="float:left" class="hideme">
            <a href="#" onClick="history.back()">
                <span class="label label-primary">{{trans('in_nk.back')}}</span>
            </a>
        </h2>
		<a class="link hvr-back-pulse" target="_blank" href="http://booth.e-taitra.com.tw/zh-TW/m/2018CC/5#&ui-state=dialog" style="float:right">{{trans('in_index.imglink')}}</a>
	</div>
</div>
@endif

@if($date=="1120")
<div class="inner_main_agile_section">
	<div class="container">
		<div class="w3-headings-all">
			<h3>{{trans('in_index.open9')}}</h3>
		</div>
		<div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
				<h3>20181120 {{trans('in_index.open9')}}</h3>
				<p></p><br>
				<div class="gallery-w3lsrow">
					<div class="gallery-grids">
						<div class="w3ls-hover">
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/place9.jpg')}}" alt="創星 樂享學 innovative 台東 阿勝單車 Taitung A-Sheng 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_as1.jpg')}}" alt="創星 樂享學 innovative 台東 阿勝單車 Taitung A-Sheng 電動輔助自行車 租車 腳踏車" class="img">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
        </div>
        <br>
        <h2 style="float:left" class="hideme">
            <a href="#" onClick="history.back()">
                <span class="label label-primary">{{trans('in_nk.back')}}</span>
            </a>
        </h2>
	</div>
</div>
@endif

@if($date=="1127")
<div class="inner_main_agile_section">
	<div class="container">
		<div class="w3-headings-all">
			<h3>{{trans('in_index.open10')}}</h3>
		</div>
		<div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
				<h3>20181120 {{trans('in_index.open10')}}</h3>
				<p></p><br>
				<div class="gallery-w3lsrow">
					<div class="gallery-grids">
						<div class="w3ls-hover">
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_fy1.jpg')}}" alt="創星 樂享學 innovative 台中 起站租車 Taichung Fengyuan 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_fy4.jpg')}}" alt="創星 樂享學 innovative 台中 起站租車 Taichung Fengyuan 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_fy2.jpg')}}" alt="創星 樂享學 innovative 台中 起站租車 Taichung Fengyuan 電動輔助自行車 租車 腳踏車" class="img">
							</div>
							<div class="col-sm-6 col-xs-12">
								<img src="{{asset('images/innovative/news_fy3.jpg')}}" alt="創星 樂享學 innovative 台中 起站租車 Taichung Fengyuan 電動輔助自行車 租車 腳踏車" class="img">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
        </div>
        <br>
        <h2 style="float:left" class="hideme">
            <a href="#" onClick="history.back()">
                <span class="label label-primary">{{trans('in_nk.back')}}</span>
            </a>
        </h2>
	</div>
</div>
@endif

@if($date=='1214')
<div class="inner_main_agile_section">
	<div class="container">
		<div class="w3-headings-all">
			<h3>{{trans('in_index.open11')}}</h3>
		</div>
		<div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
				<h3>
					<a class="hvr-icon-wobble-horizontal" href="https://ksmombaby-fair.top-link.com.tw/"> 12/14 - 12/17 , 2018 {{trans('in_index.open11')}} <br>{{trans('in_index.kse')}}
					</a>
				</h3>
				<p></p><br>
				<div class="gallery-w3lsrow">
					<div class="gallery-grids">
						<div class="w3ls-hover">
							<div class="col-sm-12 col-xs-12">
								<img src="{{asset('images/innovative/ksstroller.jpg')}}" alt="創星 樂享學 innovative 高雄 展覽館 婦幼展 嬰兒車" class="img">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
        </div><br>
        <h2 style="float:left" class="hideme">
            <a href="#" onClick="history.back()">
                <span class="label label-primary">{{trans('in_nk.back')}}</span>
            </a>
        </h2>
		<a class="link hvr-back-pulse" target="_blank" href="https://dykt84bvm7etr.cloudfront.net/uploadfiles/732/upload/files/mombaby_plan_2.pdf" style="float:right">{{trans('in_index.imglink')}}</a>
	</div>
</div>
@endif

@if($date=='190327')
<div class="inner_main_agile_section">
	<div class="container">
		<div class="w3-headings-all">
			<h3>{{trans('in_index.open7')}}</h3>
		</div>
		<div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
				<h3>
					<a class="hvr-icon-wobble-horizontal" href="https://www.taipeicycle.com.tw/zh_TW/index.html"> 03/27 - 03/30 , 2019 {{trans('in_index.open7')}} <br>{{trans('in_index.ngeh2')}}
					</a>
				</h3>
				<p></p><br>
				<div class="gallery-w3lsrow">
					<div class="gallery-grids">
						<div class="w3ls-hover">
							<div class="col-sm-12 col-xs-12">
								<img src="{{asset('images/innovative/news1-1.jpg')}}" alt="創星 樂享學 innovative 南港展覽館 台北國際自行車展覽會 Taipei Cycle 電動輔助自行車 腳踏車" class="img">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
        </div><br>
        <h2 style="float:left" class="hideme">
            <a href="#" onClick="history.back()">
                <span class="label label-primary">{{trans('in_nk.back')}}</span>
            </a>
        </h2>
	</div>
</div>

@endif

@if($date=='190404')
<div class="inner_main_agile_section">
	<div class="container">
		<div class="w3-headings-all">
			<h3>{{trans('in_index.open12')}}</h3>
		</div>
		<div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
				<h3>
					<a class="hvr-icon-wobble-horizontal" href="https://mombaby-fair.top-link.com.tw/"> 04/04 - 04/07 , 2019 {{trans('in_index.open12')}} <br>{{trans('in_index.tbme1')}}
					</a>
				</h3>
				<p></p><br>
				<div class="gallery-w3lsrow">
					<div class="gallery-grids">
						<div class="w3ls-hover">
							<div class="col-sm-12 col-xs-12">
								<img src="{{asset('images/innovative/news7.jpg')}}" alt="創星 樂享學 innovative 台北世貿 婦幼展覽會 嬰兒 孕媽咪" class="img">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
        </div><br>
        <h2 style="float:left" class="hideme">
            <a href="#" onClick="history.back()">
                <span class="label label-primary">{{trans('in_nk.back')}}</span>
            </a>
        </h2>
	</div>
</div>
@endif

@if($date=='191016')
<div class="inner_main_agile_section">
	<div class="container">
		<div class="w3-headings-all">
			<h3>{{trans('in_index.open13')}}</h3>
		</div>
		<div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
				<h3>
					<a class="hvr-icon-wobble-horizontal" href="http://www.taichungbikeweek.com/"> 10/16 - 10/18 , 2019 {{trans('in_index.open13')}} <br>{{trans('in_index.tcbw1')}}<br>台中金典酒店 The Splendor Hotel, Taichung
					</a>
				</h3>
				<p></p><br>
				<div class="gallery-w3lsrow">
					<div class="gallery-grids">
						<div class="w3ls-hover">
							<div class="col-sm-12 col-xs-12">
								<img src="{{asset('images/innovative/news9.jpg')}}" alt="創星 樂享學 innovative 台中週 自行車 Taichung Bike Week" class="img">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
        </div><br>
        <h2 style="float:left" class="hideme">
            <a href="#" onClick="history.back()">
                <span class="label label-primary">{{trans('in_nk.back')}}</span>
            </a>
        </h2>
	</div>
</div>
@endif

@if($date=='200304')
<div class="inner_main_agile_section">
	<div class="container">
		<div class="w3-headings-all">
			<h3>{{trans('in_index.open7')}}</h3>
		</div>
		<div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
				<h3>
					<a class="hvr-icon-wobble-horizontal" target="_blank" href="https://www.taipeicycle.com.tw/zh_TW/index.html"> 03/04 - 03/07 , 2020 {{trans('in_index.open7')}} <br>{{trans('in_index.ngeh3')}}
					</a>
				</h3>
				<p></p><br>
				<div class="gallery-w3lsrow">
					<div class="gallery-grids">
						<div class="w3ls-hover">
							<div class="col-sm-12 col-xs-12">
								<img src="{{asset('images/innovative/news11.jpg')}}" alt="創星 樂享學 innovative 南港展覽館 台北國際自行車展覽會 Taipei Cycle 電動輔助自行車 腳踏車 mpf drive 穩正" class="img">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
        </div><br>
        <h2 style="float:left" class="hideme">
            <a href="#" onClick="history.back()">
                <span class="label label-primary">{{trans('in_nk.back')}}</span>
            </a>
        </h2>
		<a class="link hvr-back-pulse" target="_blank" href="https://booth.e-taitra.com.tw/zh-TW/m/2020CC/19" style="float:right">{{trans('in_index.imglink')}}</a>
	</div>
</div>

@endif

@endsection
