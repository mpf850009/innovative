@extends('innovative.main')
@section('content')
<style>
	h3 a{
		padding: 10px 0px 10px 10px; 
	}
</style>
<div class="inner_main_agile_section">
	<div class="container">
		<div class="w3-headings-all hideme">
			<h3>{{trans('in_index.news')}}</h3>
		</div>
		<div class="agile_inner_grids">
			<div class="col-md-12 w3_agileits_about_grid_left">
				<h3 class="hideme">
					<a href="{{route('innovative.news_detail',[$locale,$date='200304'])}}" class="hvr-icon-wobble-horizontal"><i class="fa fa-arrow-right hvr-icon"></i> 20200304 {{trans('in_index.open7')}} 
					</a>
				</h3>
				<h3 class="hideme">
					<a href="{{route('innovative.news_detail',[$locale,$date='191016'])}}" class="hvr-icon-wobble-horizontal"><i class="fa fa-arrow-right hvr-icon"></i> 20191016 {{trans('in_index.open13')}} 
					</a>
				</h3>
				<h3 class="hideme">
					<a href="{{route('innovative.news_detail',[$locale,$date='190404'])}}" class="hvr-icon-wobble-horizontal"><i class="fa fa-arrow-right hvr-icon"></i> 20190404 {{trans('in_index.open12')}} 
					</a>
				</h3>
				<h3 class="hideme">
					<a href="{{route('innovative.news_detail',[$locale,$date='190327'])}}" class="hvr-icon-wobble-horizontal"><i class="fa fa-arrow-right hvr-icon"></i> 20190327 {{trans('in_index.open7')}} 
					</a>
				</h3>
				<h3 class="hideme">
					<a href="{{route('innovative.news_detail',[$locale,$date='1214'])}}" class="hvr-icon-wobble-horizontal"><i class="fa fa-arrow-right hvr-icon"></i> 20181214 {{trans('in_index.open11')}} 
					</a>
				</h3>
				<h3 class="hideme">
					<a href="{{route('innovative.news_detail',[$locale,$date='1127'])}}" class="hvr-icon-wobble-horizontal"><i class="fa fa-arrow-right hvr-icon"></i> 20181127 {{trans('in_index.open10')}} 
					</a>
				</h3>
				<h3 class="hideme">
					<a href="{{route('innovative.news_detail',[$locale,$date='1120'])}}" class="hvr-icon-wobble-horizontal"><i class="fa fa-arrow-right hvr-icon"></i> 20181120 {{trans('in_index.open9')}} 
					</a>
				</h3>
				<h3 class="hideme">
					<a href="{{route('innovative.news_detail',[$locale,$date='1030'])}}" class="hvr-icon-wobble-horizontal"><i class="fa fa-arrow-right hvr-icon"></i> 20181030 {{trans('in_index.open7')}} 
					</a>
				</h3>
				<h3 class="hideme">
					<a href="{{route('innovative.news_detail',[$locale,$date='0911'])}}" class="hvr-icon-wobble-horizontal"><i class="fa fa-arrow-right hvr-icon"></i> 20180911 {{trans('in_index.open8')}} 
					</a>
				</h3>
				<h3 class="hideme">
					<a href="{{route('innovative.news_detail',[$locale,$date='0617'])}}" class="hvr-icon-wobble-horizontal"><i class="fa fa-arrow-right hvr-icon"></i> 20180617 {{trans('in_index.open3')}} 
					</a>
				</h3>
				<h3 class="hideme">
					<a href="{{route('innovative.news_detail',[$locale,$date='0601'])}}" class="hvr-icon-wobble-horizontal"><i class="fa fa-arrow-right hvr-icon"></i> 20180601 {{trans('in_index.open5')}} 
					</a>
				</h3>
				<h3 class="hideme">
					<a href="{{route('innovative.news_detail',[$locale,$date='0427'])}}" class="hvr-icon-wobble-horizontal"><i class="fa fa-arrow-right hvr-icon"></i> 20180427 {{trans('in_index.open6')}}
					</a>
				</h3>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<script src="{{asset('js/innovative/lightbox-plus-jquery.min.js')}}"></script>

@endsection