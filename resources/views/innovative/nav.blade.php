<?php $URL='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>
<!-- banner -->
	<div class="header" style="padding:0px 0px;">
		<div class="w3layouts_header_right">
			<div class="detail-w3l" >
				<ul>
					<!--
					<li>
						<i class="glyphicon glyphicon-earphone" aria-hidden="true"></i> 06-2028028
					</li>
					-->
				</ul>
			</div>
		</div>
		<div class="w3layouts_header_left">
			<ul><!--
				<li>
					<a href="#">
						<i class="fa fa-user" aria-hidden="true"></i>Login</a>
				</li>
				
				<li>
					<a href="#" data-toggle="modal" data-target="#myModal2">
						<i class="fa fa-user" aria-hidden="true"></i>Login</a>
				</li>
			-->
			</ul>
		</div>
		<!-- AddToAny BEGIN -->
			<div class="a2a_kit a2a_kit_size_20 a2a_default_style agileits-social top_content">
				<a class="a2a_button_facebook"></a>
				<a class="a2a_button_twitter"></a>
				<a class="a2a_button_google_plus"></a>
				<a class="a2a_button_line"></a>
				<a class="a2a_button_wechat"></a>
			</div>
			<script async src="https://static.addtoany.com/menu/page.js"></script>
		<!-- AddToAny END -->

		<!--<div class="agileits-social top_content">

			<ul>
				<li>
					<a href="#">
						<i class="fa fa-facebook"></i>
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-twitter"></i>
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-youtube-square"></i>
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-instagram"></i>
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-comments"></i>
					</a>
				</li>
				<li>
					<a href="http://www.facebook.com/share.php?u=<?php echo $URL; ?>"><i class="fa fa-facebook"></i></a>
				</li>
				<li>
					<a href="https://social-plugins.line.me/lineit/share?url=<?php echo $URL; ?>"><img src="{{asset('images/innovative/line.png')}}" alt="" srcset=""></a>
				</li>
				
				<li>
					<a href="#">
						<i class="fa fa-rss"></i>
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-vk"></i>
					</a>
				</li>
			</ul>
		</div>-->
		<div class="clearfix"> </div>
	</div>
	<div class="banner">
		<nav class="navbar navbar-default">
			<div class="navbar-header navbar-left">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<h1>
					<a class="navbar-brand" href="{{route('innovative.index',$locale)}}">
						<!--
						<span>
							<i class="fa fa-star" aria-hidden="true"></i>
						</span>Innovative
						-->
						<img src="{{asset('images/innovative/innovativelogo200.png')}}" alt="">
					</a>
				</h1>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
				<nav class="link-effect-2" id="link-effect-2">
					<ul class="nav navbar-nav">
						<li @if($active=='index')class="active" @endif>
							<a href="{{route('innovative.index',$locale)}}">
								<span data-hover="{{trans('in_index.home')}}">{{ trans('in_index.home') }}</span>
							</a>
						</li>
						<li @if($active=='about us')class="active" @endif>
							<a href="{{route('innovative.about',$locale)}}">
								<span data-hover="{{trans('in_index.aboutus')}}">{{trans('in_index.aboutus')}}</span>
							</a>
						</li>
						<li @if($active=='news' || $active=='news1')class="active" @endif>
							<a href="{{route('innovative.news',$locale)}}">
								<span data-hover="{{trans('in_index.news')}}">{{trans('in_index.news')}}</span>
							</a>
						</li>
						<li @if($active=='nikimotion'||$active=='autofold'||$active=='autofold lite'||$active=='blade'||$active=='accessories')class="active" @endif class="dropdown" >
							<a href="{{route('innovative.nikimotion',$locale)}}">
								<span data-hover="Nikimotion">Nikimotion</span>
							</a>
							<!--
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<span data-hover="Nikimotion">Nikimotion</span>
								<b class="caret"></b>
							</a>
							<ul class="dropdown-menu agile_short_dropdown">
								<li>
									<a href="{{route('innovative.autofold',$locale)}}">{{trans('in_index.autofold')}}</a>
								</li>
								<li>
									<a href="{{route('innovative.autofoldlite',$locale)}}">{{trans('in_index.autofoldlite')}}</a>
								</li>
								<li>
									<a href="{{route('innovative.blade',$locale)}}">Blade</a>
								</li>
								<li>
									<a href="{{route('innovative.nkac',$locale)}}">Accessories</a>
								</li>
							</ul>-->
						</li>
						<li @if($active=='veer'||$active=='veer acc')class="active" @endif>
							<a href="{{route('innovative.veer',$locale)}}">
								<span data-hover="Veer">Veer</span>
							</a>
						</li>
						<li @if($active=='e-bike'||$active=='rentplace'||$active=='brand'||$active=='e-bike intro'||$active=='rentmap')class="active" @endif class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<span data-hover="E-Bike">E-Bike</span>
								<b class="caret"></b>
							</a>
							<ul class="dropdown-menu agile_short_dropdown">
								<li>
									<a href="{{route('innovative.ebike',$locale)}}">ikin</a>
								</li>
								<li>
									<a href="{{route('innovative.rentmap',$locale)}}">{{trans('in_index.rentplace')}}</a>
								</li>
								<li>
									<a href="{{route('innovative.brand',$locale)}}">{{trans('in_index.brand')}}</a>
								</li>
							</ul>
						</li>
						<li @if($active=='store')class="active" @endif>
                            <a href="{{route('innovative.flagship',$locale)}}">
                                <span data-hover="{{trans('in_index.store')}}">{{trans('in_index.store')}}</span>
                            </a>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<span data-hover="Language">Language</span>
								<b class="caret"></b>
							</a>
							<ul class="dropdown-menu agile_short_dropdown">
								<li>
									<a href="/home_en">English</a>
								</li>
								<li>
									<a href="/home_zh-TW">繁體中文</a>
								</li>
								<!--<li>
									<a href="">簡体中文</a>
								</li>
								<li>
									<a href="">한국어</a>
								</li>
								<li>
									<a href="">日本語</a>
								</li>-->
							</ul>
						</li>
					</ul>
				</nav>
			</div>
		</nav>
	</div>
	<!-- banner -->
	@if($active!="index")
		@if($active=="news1")
		<div class="banner-multi" style="{{$background}}"></div>
		<div class="about-breadcrumb">
			<div class="container">
				<ul>
					<li>
						<a href="{{route('innovative.index',$locale)}}">{{trans('in_index.home')}}</a>
						<i>|</i>
					</li>
					<li>
						<a href="{{route('innovative.news',$locale)}}">{{trans('in_index.news')}}</a>
						<i>|</i>
					</li>
					<li>
						<p>{{$tag}}</p>
					</li>
				</ul>
			</div>
		</div>
		@elseif($active=="rentplace")
		<div class="banner-multi" style="{{$background}}"></div>
		<div class="about-breadcrumb">
			<div class="container">
				<ul>
					<li>
						<a href="{{route('innovative.index',$locale)}}">{{trans('in_index.home')}}</a>
						<i>|</i>
					</li>
					<li>
						<a href="{{route('innovative.rentmap',$locale)}}">{{trans('in_index.rentplace')}}</a>
						<i>|</i>
					</li>
					<li>
						<p>{{$place}}</p>
					</li>
				</ul>
			</div>
		</div>
		@elseif($active=="autofold"||$active=="autofold lite"||$active=="blade"||$active=="accessories")
		<div class="banner-multi" style="{{$background}}"></div>
		<div class="about-breadcrumb">
			<div class="container">
				<ul>
					<li>
						<a href="{{route('innovative.index',$locale)}}">{{trans('in_index.home')}}</a>
						<i>|</i>
					</li>
					<li>
						<p>{{$active}}</p>
					</li>
				</ul>
			</div>
		</div>
		@else
		<div class="banner-multi" style="{{$background}}">
		</div>
		<div class="about-breadcrumb">
			<div class="container">
				<ul>
					<li>
						<a href="{{route('innovative.index',$locale)}}">{{trans('in_index.home')}}</a>
						<i>|</i>
					</li>
					<li>
						<p>{{$active}}</p> 
					</li>
				</ul>
			</div>
		</div>
		@endif
	@endif