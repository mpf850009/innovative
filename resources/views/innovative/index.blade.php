@extends('innovative.main')
@section('content')
@include('innovative.script.indexscript')
<!-- banner -->
<div id="exampleSlider">
	<div>
		<h3>
			<span></span>
		</h3>
	</div>
	<div>
		<h3>
			<span></span>
		</h3>
	</div>
	<div>
		<h3>
			<span></span>
		</h3>
	</div>
	<div>
		<h3>
			<span></span>
		</h3>
	</div>
</div>
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<div class="signin-form profile">
					<h3 class="agileinfo_sign">Sign In</h3>
					<div class="login-form">
						<form action="#" method="post">
							<input type="email" name="email" placeholder="E-mail" required="">
							<input type="password" name="password" placeholder="Password" required="">
							<div class="tp">
								<input type="submit" value="Sign In">
							</div>
						</form>
					</div>
					<div class="login-social-grids">
						<ul>
							<li>
								<a href="#">
									<i class="fa fa-facebook"></i>
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa fa-twitter"></i>
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa fa-rss"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal1 -->
<!-- bootstrap-pop-up -->
<div class="w3-about-us">
	<div class="container">
		<div class="w3-headings-all hideme">
			<h3>{{trans('in_index.welcome')}}</h3>
		</div>
		<!-- edited on 2020/04/23 add font-size-->
		<div class="w3-about-text hideme">{!!trans('in_index.welcome1')!!}</div>
		
		<!-- about brief -->
		<div class="events">
			{{-- <div id="abgne_marquee" class="hideme">
				<ul>
					<li class="b1">
						<h3>
							<a href="{{route('innovative.news_detail',[$locale,$date='200304'])}}" class="hvr-icon-wobble-horizontal"><i class="fa fa-arrow-right hvr-icon"></i> 20200304 {{trans('in_index.open7')}}</a>
						</h3>
					</li>
					<li class="b1">
						<h3>
							<a href="{{route('innovative.news_detail',[$locale,$date='191016'])}}" class="hvr-icon-wobble-horizontal"><i class="fa fa-arrow-right hvr-icon"></i> 20191016 {{trans('in_index.open13')}}</a>
						</h3>
					</li>
					<li class="b1">
						<h3>
							<a href="{{route('innovative.news_detail',[$locale,$date='190404'])}}" class="hvr-icon-wobble-horizontal"><i class="fa fa-arrow-right hvr-icon"></i> 20190404 {{trans('in_index.open12')}}</a>
						</h3>
					</li>
					<li class="b1">
						<h3>
							<a href="{{route('innovative.news_detail',[$locale,$date='190327'])}}" class="hvr-icon-wobble-horizontal"><i class="fa fa-arrow-right hvr-icon"></i> 20190327 {{trans('in_index.open7')}}</a>
						</h3>
					</li>
					<li class="b1">
						<h3>
							<a href="{{route('innovative.news_detail',[$locale,$date='1214'])}}" class="hvr-icon-wobble-horizontal"><i class="fa fa-arrow-right hvr-icon"></i> 20181214 {{trans('in_index.open11')}}</a>
						</h3>
					</li>
					<li class="b1">
						<h3>
							<a href="{{route('innovative.news_detail',[$locale,$date='1127'])}}" class="hvr-icon-wobble-horizontal"><i class="fa fa-arrow-right hvr-icon"></i> 20181127 {{trans('in_index.open10')}}</a>
						</h3>
					</li>
					<li class="b1">
						<h3>
							<a href="{{route('innovative.news_detail',[$locale,$date='1120'])}}" class="hvr-icon-wobble-horizontal"><i class="fa fa-arrow-right hvr-icon"></i> 20181120 {{trans('in_index.open9')}} 
							</a>
						</h3>
					</li>
					<li class="b1">
						<h3>
							<a href="{{route('innovative.news_detail',[$locale,$date='1030'])}}" class="hvr-icon-wobble-horizontal"><i class="fa fa-arrow-right hvr-icon"></i> 20181030 {{trans('in_index.open7')}} 
							</a>
						</h3>
					</li>
					<li class="b1">
						<h3>
							<a href="{{route('innovative.news_detail',[$locale,$date='0911'])}}" class="hvr-icon-wobble-horizontal"><i class="fa fa-arrow-right hvr-icon"></i> 20180911 {{trans('in_index.open8')}} 
							</a>
						</h3>
					</li>
					<li class="b1">
						<h3>
							<a href="{{route('innovative.news_detail',[$locale,$date='0617'])}}" class="hvr-icon-wobble-horizontal"><i class="fa fa-arrow-right hvr-icon"></i> 20180617 {{trans('in_index.open3')}}</a>
						</h3>
					</li>
					<li class="b1">
						<h3>
							<a href="{{route('innovative.news_detail',[$locale,$date='0601'])}}" class="hvr-icon-wobble-horizontal"><i class="fa fa-arrow-right hvr-icon"></i> 20180601 {{trans('in_index.open5')}}</a>
						</h3>
					</li>
					<li class="b1">
						<h3>
							<a href="{{route('innovative.news_detail',[$locale,$date='0427'])}}" class="hvr-icon-wobble-horizontal"><i class="fa fa-arrow-right hvr-icon"></i> 20180427 {{trans('in_index.open6')}}</a>
						</h3>
					</li>
				</ul>
			</div> --}}
			<div class="col-md-6 w3layouts_event_grid hideme">
				<div class="w3_agile_event_grid1">
					<img src="{{asset('images/innovative/wel5.jpg')}}" alt=" " class="img-responsive" />
				</div>
				<div class="agileits_w3layouts_event_grid1">
					<h5>
						<a href="#" data-toggle="modal" data-target="#myModal">{{trans('in_index.nikimotion')}}</a>
					</h5>
					<p>{{trans('in_index.wel1')}}</p>
				</div>
			</div>
			<div class="col-md-6 w3layouts_event_grid hideme">
				<div class="w3_agile_event_grid1">
					<img src="{{asset('images/innovative/wel6.jpg')}}" alt=" " class="img-responsive" />
				</div>
				<div class="agileits_w3layouts_event_grid1">
					<h5>
						<a href="#" data-toggle="modal" data-target="#myModal">{{trans('in_index.veer')}}</a>
					</h5>
					<p>{{trans('in_index.wel2')}}</p>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-md-6 w3layouts_event_grid hideme">
				<div class="w3_agile_event_grid1">
					<img src="{{asset('images/innovative/wel4.jpg')}}" alt=" " class="img-responsive" />
				</div>
				<div class="agileits_w3layouts_event_grid1">
					<h5>
						<a href="#" data-toggle="modal" data-target="#myModal">IKIN</a>
					</h5>
					<p>{{trans('in_index.wel3')}}</p>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<!-- //about brief -->
	</div>
</div>
<!-- services -->
<div class="w3-agile-services">
	<div class="container">
		<div class="w3-headings-all hideme">
			<h3>{{trans('in_index.service')}}</h3>
		</div>
		<div class="agileits-services">
			<div class="services-right-grids">
				<div class="col-sm-4 services-right-grid hideme">
					<div class="services-icon hvr-radial-in">
						<i class="fa fa-tachometer" aria-hidden="true"></i>
					</div>
					<div class="services-icon-info">
						{!!trans('in_index.service1')!!}
						<h4>
							<a href="#" data-toggle="modal" data-target="#myModal11">more</a>
						</h4>
					</div>
				</div>
				<div class="col-sm-4 services-right-grid hideme">
					<div class="services-icon hvr-radial-in">
						<i class="fa fa-wrench" aria-hidden="true"></i>
					</div>
					<div class="services-icon-info">
						{!!trans('in_index.service2')!!}
						<h4>
							<a href="#" data-toggle="modal" data-target="#myModal12">more</a>
						</h4>
					</div>
				</div>
				<div class="col-sm-4 services-right-grid hideme">
					<div class="services-icon hvr-radial-in">
						<i class="fa fa-shield" aria-hidden="true"></i>
					</div>
					<div class="services-icon-info">
						{!!trans('in_index.service3')!!}
						<h4>
							<a href="#" data-toggle="modal" data-target="#myModal13">more</a>
						</h4>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
</div>
@include('innovative.service')
<!-- services -->
<!-- news -->
<div id="news" class="gallery">
	<div class="container">
		<div class="w3-headings-all hideme">
			<h3>{{trans('in_index.exhibitor')}}</h3>
		</div>
		<div class="gallery-w3lsrow">
			{{--  <div class="col-sm-6 col-xs-12 gallery-grids hideme">
				<div class="w3ls-hover">
					<a target="_blank" href="https://www.taipeicycle.com.tw/zh_TW/index.html" data-lightbox="example-set" data-title="">
						<img src="{{asset('images/innovative/news1.jpg')}}" class="img-responsive zoom-img img-thumbnail" alt="" />
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-xs-12 gallery-grids w3-adv-right2 hideme" style="padding:1cm 1cm 0cm 1cm;">
				{!!trans('in_index.exh1')!!}
			</div>
			<div class="clearfix"> </div>

			<div class="col-sm-6 col-xs-12 gallery-grids w3-adv-left1 hideme" style="padding:1cm 1cm 0cm 1cm;">
				{!!trans('in_index.exh2')!!}
			</div>
			<div class="col-sm-6 col-xs-12 gallery-grids hideme">
				<div class="w3ls-hover">
					<a target="_blank" href="http://www.taichungbikeweek.com/" data-lightbox="example-set" data-title="">
						<img src="{{asset('images/innovative/news3.jpg')}}" class="img-responsive zoom-img img-thumbnail" alt="" />
					</a>
				</div>
			</div>
			<div class="clearfix"> </div>

			<div class="col-sm-6 col-xs-12 gallery-grids hideme">
				<div class="w3ls-hover">
					<a target="_blank" href="https://mombaby-fair.top-link.com.tw/page/636" data-lightbox="example-set" data-title="">
						<img src="{{asset('images/innovative/news5.jpg')}}" class="img-responsive zoom-img img-thumbnail" alt="" />
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-xs-12 gallery-grids w3-adv-right2 hideme" style="padding:1cm 1cm 0cm 1cm;">
				{!!trans('in_index.exh3')!!}
			</div>
			<div class="clearfix"> </div>  --}}

			<div class="col-sm-6 col-xs-12 gallery-grids hideme">
				<div class="w3ls-hover" style="padding:2.5em 0em">
					<a target="_blank" href="https://mombaby-fair.top-link.com.tw/" data-lightbox="example-set" data-title="">
						<img src="{{asset('images/innovative/mombaby.jpg')}}" class="img-responsive zoom-img" alt="嬰兒 孕媽咪 台北世貿 nikimotion veer 高雄展覽館" />
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-xs-12 gallery-grids w3-adv-right2 hideme" style="padding:1cm 1cm 0cm 1cm;">
				{!!trans('in_index.exh5')!!}
			</div>

			<div class="clearfix"> </div><hr>

			<div class="col-sm-6 col-xs-12 gallery-grids w3-adv-left1 hideme" style="padding:1cm 1cm 0cm 1cm;">
				{!!trans('in_index.tcbw2')!!}
			</div>
			<div class="col-sm-6 col-xs-12 gallery-grids hideme">
				<div class="w3ls-hover" style="padding:2.5em 0em">
					<a target="_blank" href="http://www.taichungbikeweek.com" data-lightbox="example-set" data-title="">
						<img src="{{asset('images/innovative/news9.jpg')}}" class="img-responsive zoom-img" alt="台中週 自行車 Taichung Bike Week MPF Drive" />
					</a>
				</div>
			</div>

			<div class="clearfix"> </div>
			<div class="container hideme">
				<br>
				{!!trans('in_index.info1')!!}
			</div>
		</div>
	</div>
</div>
<!-- news -->
<!-- contact -->
<div class="contact jarallax">
	<div class="container">
		<div class="w3-headings-all hideme">
			<h3>{!!trans('in_index.contactus')!!}</h3>
		</div>
		<div class="contact-grids"  align="center">
			<div class="address hideme">
				{!!trans('in_index.contactdetail')!!}
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<!-- contact map -->
<div class="map hideme">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3671.7499813876384!2d120.25324161433454!3d23.032950421721452!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x346e715cc4bb730b%3A0x4bbcd933c427b0d4!2z5Ym15pifIElubm92YXRpdmU!5e0!3m2!1szh-TW!2stw!4v1528681058904"></iframe>
</div>
<style type="text/css">
div#abgne_marquee {
	position: relative;
	overflow: hidden;
	width: auto;
	height: 60px;
	border: 0px solid #ccc;
	margin:0px auto;
}
div#abgne_marquee ul, div#abgne_marquee li {
	margin: 0;
	padding: 0;
	list-style: none;
}
div#abgne_marquee ul {
	position: absolute;
		/* 往後推個 30px */
}
div#abgne_marquee ul li a {
	display: block;
	overflow: hidden; /* 超出範圍的部份要隱藏 */
	font-size:22px;
	height: 60px;
	line-height: 22px;	
	text-decoration: none;	
}
div#abgne_marquee ul li.b1 a {
	background-position: 5px 5px;
}
div#abgne_marquee ul li.b2 a {
	background-position: 5px -5px;
}
div#abgne_marquee ul li.b3 a {
	background-position: 5px -15px;
}
div#abgne_marquee div.marquee_btn {
	position: absolute;
	cursor: pointer;
}
</style>
<script type="text/javascript">
$(function(){
	// 先取得 div#abgne_marquee ul
	// 接著把 ul 中的 li 項目再重覆加入 ul 中(等於有兩組內容)
	// 再來取得 div#abgne_marquee 的高來決定每次跑馬燈移動的距離
	// 設定跑馬燈移動的速度及輪播的速度
	var $marqueeUl = $('div#abgne_marquee ul'),
	_marqueeUlHtml = $marqueeUl.html(),
	_height = $('div#abgne_marquee').height() * -1,
	scrollSpeed = 600,
	timer,
	speed = 3000 + scrollSpeed,
	direction = 0, // 0 表示往上, 1 表示往下
	_lock = false,
	_showItems = 1, // 一次顯示幾個
	_moveItems = 1; // 一次移動幾個
		
	$("div#abgne_marquee").css("height", _showItems * _height * -1); // 重新設定div的高度
	$("div#abgne_marquee div.marquee_btn").css("top", (_showItems - 1) * _height * -1 / 2); // 重新設定marquee_btn的margin-top

	if(_showItems >= $marqueeUl.children('li').length) return;

	var $marqueeli = $marqueeUl.append(_marqueeUlHtml+_marqueeUlHtml).children();
		
	// 先把 $marqueeli 移動到第二組
	$marqueeUl.css('top', $marqueeli.length / 3 * _height);
	
	// 幫左邊 $marqueeli 加上 hover 事件
	// 當滑鼠移入時停止計時器；反之則啟動
	$marqueeli.hover(function(){
		clearTimeout(timer);
	}, function(){
		timer = setTimeout(showad, speed);
	});
	
	// 判斷要往上還是往下
	$('div#abgne_marquee .marquee_btn').click(function(){
		if(_lock) return;
		clearTimeout(timer);
		direction = $(this).attr('id') == 'marquee_next_btn' ? 0 : 1;
		showad();
	});
	
	// 控制跑馬燈上下移動的處理函式
	function showad(){
		_lock = !_lock;
		var _now = $marqueeUl.position().top / _height;
		_now = (direction ? _now - _moveItems + $marqueeli.length : _now + _moveItems)  % $marqueeli.length;
		// $marqueeUl 移動

		$marqueeUl.animate({
			top: _now * _height
		}, scrollSpeed, function(){
		// 如果已經移動到第二組時...則馬上把 top 設回到第一組的最後一筆
		// 藉此產生不間斷的輪播

		if(_now + _moveItems >= $marqueeli.length / 3 * 2){
			$marqueeUl.css('top', $marqueeli.length / 3 * _height - _height * ($marqueeli.length / 3 * 2 - _now));
		}else if(_now < $marqueeli.length / 3 ){
			$marqueeUl.css('top', $marqueeli.length / 3 * _height + (_height* _now));
		}
		_lock = !_lock;
		});
		
		// 再啟動計時器
		timer = setTimeout(showad, speed);
		}
		// 啟動計時器
		timer = setTimeout(showad, speed);
		$('a').focus(function(){
		this.blur();
	});
});
</script>
@endsection
